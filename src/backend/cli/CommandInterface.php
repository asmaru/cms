<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli;

use asmaru\cli\StyledOutput;

interface CommandInterface {

	public function execute(Input $input, StyledOutput $output): CommandResult;

	public function help(): string;
}