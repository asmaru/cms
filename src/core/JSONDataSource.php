<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use asmaru\io\FileSystem;
use asmaru\io\IOException;
use function array_pop;
use function explode;
use function trim;

/**
 * Class JSONDataSource
 *
 * @package asmaru\cms\core
 */
class JSONDataSource {

	private readonly FileSystem $fileSystem;

	public function __construct(FileSystem $fileSystem) {
		$this->fileSystem = $fileSystem;
	}

	/**
	 * @return mixed[]
	 */
	public function all(): array {
		$contents = $this->fileSystem->readAll(['json'], true);

		return array_map(function ($data) {
			return json_decode($data, true);
		}, $contents);
	}

	/**
	 * @throws IOException
	 */
	public function exists(string $name): bool {

		$parts = explode('/', trim($name, '/'));
		$lastPart = array_pop($parts);
		$fs = $this->fileSystem;
		foreach ($parts as $part) {
			if (!$fs->isDir($part)) {
				return false;
			}
			$fs = $fs->down($part);
		}
		$key = $this->getKey($lastPart);
		return $fs->isFile($key);

		/*
		$key = $this->getKey($name);
		return $this->fileSystem->isFile($key);
		*/
	}

	/**
	 * @param string $name
	 *
	 * @return string
	 */
	private function getKey(string $name): string {
		return $name . '.json';
	}

	/**
	 * @param callable|null $filter
	 * @param callable|null $sorting
	 * @param int $limit
	 * @param int $offset
	 *
	 * @return array
	 */
	public function find(callable $filter = null, callable $sorting = null, int $limit = 0, int $offset = 0): array {

		//find recursive @todo muss raus wenn man im admin tool die Ordner auswählen kann
		$contents = $this->fileSystem->readAllRecursive(['json'], true);

		//$contents = $this->fileSystem->readAll(['json'], true);

		$contents = array_map(function ($data) {
			return json_decode($data, true);
		}, $contents);

		if (is_callable($filter)) {
			$contents = array_filter($contents, $filter);
		}

		if (is_callable($sorting)) {
			uasort($contents, $sorting);
		}

		$size = count($contents);
		if ($limit > 0 && $size > $limit) {
			$offset = max(0, min($size - 1, $offset));
			$contents = array_slice($contents, $offset, $limit, true);
		}

		return $contents;
	}

	/**
	 * @throws IOException
	 */
	public function get(string $name) {

		$parts = explode('/', trim($name, '/'));
		$lastPart = array_pop($parts);
		$fs = $this->fileSystem;
		foreach ($parts as $part) {
			$fs = $fs->down($part);
		}
		$key = $this->getKey($lastPart);
		$data = $fs->read($key);
		return $data !== null ? json_decode($data, true) : null;

		/*
	$key = $this->getKey($name);
	$data = $this->fileSystem->read($key);
	$data = $data !== null ? json_decode($data, true) : null;
	return $data;
		*/
	}

	/**
	 * @throws IOException
	 */
	public function put(string $name, string $data): bool {

		$parts = explode('/', trim($name, '/'));
		$lastPart = array_pop($parts);
		$fs = $this->fileSystem;
		foreach ($parts as $part) {
			$fs = $fs->createDir($part);
		}
		$key = $this->getKey($lastPart);
		$data = json_encode($data, JSON_PRETTY_PRINT);
		return $fs->write($key, $data);

		/*
		$key = $this->getKey($name);
		$data = json_encode($data, JSON_PRETTY_PRINT);
		return $this->fileSystem->write($key, $data);
		*/
	}

	/**
	 * @throws IOException
	 */
	public function remove(string $name): void {

		$parts = explode('/', trim($name, '/'));
		$lastPart = array_pop($parts);
		$fs = $this->fileSystem;
		foreach ($parts as $part) {
			$fs = $fs->down($part);
		}
		$key = $this->getKey($lastPart);
		$fs->delete($key);
		/*
		$key = $this->getKey($name);
		return $this->fileSystem->delete($key);
		*/
	}
}