<?php

declare(strict_types=1);

namespace asmaru\cms;

use asmaru\cms\backend\api\AccessLogEndpoint;
use asmaru\cms\backend\api\BackupEndpoint;
use asmaru\cms\backend\api\ClearCacheEndpoint;
use asmaru\cms\backend\api\FileMetaDataEndpoint;
use asmaru\cms\backend\api\PreviewEndpoint;
use asmaru\cms\backend\api\ResourceEndpoint;
use asmaru\cms\backend\api\SystemStatusEndpoint;
use asmaru\cms\backend\api\TypeEndpoint;
use asmaru\cms\backend\filter\BasicAuthFilter;
use asmaru\cms\frontend\api\PingEndpoint;
use asmaru\cms\frontend\api\PublicPageEndpoint;
use asmaru\cms\frontend\api\PublicSearchEndpoint;
use asmaru\cms\frontend\api\SitemapEndpoint;
use asmaru\http\App;
use OpenApi\Attributes as OA;

#[OA\Info(version: '2.0', title: 'asmaru CMS API')]
#[OA\Server(url: 'http://127.0.0.1:8000')]
class Endpoints {

	public function register(App $app): void {

		// Authentication
		$app->filter('* api/:s', BasicAuthFilter::class);

		// Backend
		$app->route('GET|PUT|DELETE api/types', TypeEndpoint::class);
		$app->route('GET api/stats', AccessLogEndpoint::class);
		$app->route('GET api/status', SystemStatusEndpoint::class);
		$app->route('POST|GET|DELETE api/backup/#name', BackupEndpoint::class);
		$app->route('GET|PUT api/meta/#name', FileMetaDataEndpoint::class);
		$app->route('DELETE api/cache/#name AS cache', ClearCacheEndpoint::class);
		$app->route('PUT api/preview/#name AS preview_document', PreviewEndpoint::class);
		$app->route('GET|POST|PUT|DELETE api/#name AS doc', ResourceEndpoint::class);

		// Frontend
		$app->route('GET ping', PingEndpoint::class);
		$app->route('GET search', PublicSearchEndpoint::class);
		$app->route('GET sitemap.xml', SitemapEndpoint::class);
		$app->route('GET #name AS public_document', PublicPageEndpoint::class);
	}
}