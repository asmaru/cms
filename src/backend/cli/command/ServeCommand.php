<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\cms\core\Context;
use function system;

/**
 * Class ServeCommand
 *
 * @package asmaru\cms\backend\cli\command
 * @noinspection PhpUnused
 */
class ServeCommand implements CommandInterface {

	private readonly Context $context;

	public function __construct(Context $context) {
		$this->context = $context;
	}

	public function execute(Input $input, StyledOutput $output): CommandResult {
		$output->infoLine('Starting build-in server [127.0.0.1:8000]...');
		system('php -S 127.0.0.1:8000 -t ' . $this->context->getRootPath() . ' ' . $this->context->getPath('asmaru.phar'));
		return CommandResult::success();
	}

	public function help(): string {
		return 'Starts the build-in server';
	}
}