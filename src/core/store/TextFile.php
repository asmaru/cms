<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

use SplFileInfo;
use function in_array;

class TextFile extends Media {

	final const EXTENSIONS = ['txt', 'html'];

	public function getData(): ?string {
		return $this->parent->getFileSystem()->read($this->getName());
	}

	/**
	 * @param $data
	 */
	public function save($data): void {
		$this->parent->getFileSystem()->write($this->getName(), $data);
	}

	public static function matchType(SplFileInfo $file): bool {
		return in_array(mb_strtolower($file->getExtension()), self::EXTENSIONS);
	}

}