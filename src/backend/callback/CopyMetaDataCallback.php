<?php

declare(strict_types=1);

namespace asmaru\cms\backend\callback;

use asmaru\cms\core\event\Event;
use asmaru\cms\core\event\EventCallback;
use asmaru\cms\core\meta\MetaDataService;

/**
 * Class CopyMetaDataCallback
 *
 * @package asmaru\cms\backend\callback
 */
class CopyMetaDataCallback implements EventCallback {

	/**
	 * @var MetaDataService
	 */
	private readonly MetaDataService $metaDataService;

	/**
	 * DeleteMetaDataHandler constructor.
	 *
	 * @param MetaDataService $metaDataService
	 */
	public function __construct(MetaDataService $metaDataService) {
		$this->metaDataService = $metaDataService;
	}

	/**
	 * @param Event $event
	 */
	public function execute(Event $event): void {
		$this->metaDataService->set($event->getCopy(), $this->metaDataService->get($event->getSource()));
	}
}