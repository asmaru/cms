<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\bbcode;

use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\store\StoreElement;
use function array_reduce;

/**
 * Class ListShortCode
 *
 * @package asmaru\cms\frontend\generation\bbcode
 */
class ListShortCode extends AbstractListingShortCode {

	/**
	 * @return string
	 */
	protected function getPartialFileName(): string {
		return 'list.html';
	}

	/**
	 * @return string
	 */
	protected function getTagName(): string {
		return 'list';
	}

	/**
	 * @param SearchFilter $filter
	 */
	protected function prepareFilter(SearchFilter $filter): void {

	}

	/**
	 * @param array $items
	 *
	 * @return array
	 */
	protected function prepareItems(array $items): array {
		return array_reduce($items, function (array $carry, StoreElement $item): array {
			$carry[] = $item->jsonSerialize();
			return $carry;
		}, []);
	}
}