<?php

declare(strict_types=1);

namespace asmaru\cms\backend\service;

use asmaru\io\FileList;
use asmaru\io\FileSystem;
use asmaru\io\FileUtility;
use asmaru\io\IOException;
use SplFileInfo;
use function ignore_user_abort;
use function set_time_limit;

class BackupService {

	public function __construct(private readonly FileSystem $fileSystem) {
	}

	/**
	 * @return FileList
	 * @throws IOException
	 */
	public function all(): FileList {
		$filesystem = $this->fileSystem->down('backup');
		return $filesystem->files(['zip']);
	}

	/**
	 * @throws IOException
	 */
	public function create(): void {
		set_time_limit(0);
		ignore_user_abort(true);

		$fileUtility = new FileUtility();

		// Create a new backup dir
		$backupFileName = 'backup_' . date('Ymd', time()) . '_' . time();
		$backupTarget = $this->fileSystem->down('backup')->createDir($backupFileName);

		// Copy all files to the backup target
		$filesToCopy = ['data'];
		foreach ($filesToCopy as $name) {
			$fileUtility->copyRecursive($this->fileSystem->path($name), $backupTarget->path($name));
		}

		// Add all copied files to a new zip archive
		$zipFile = $this->fileSystem->down('backup')->create($backupFileName . '.zip');
		$fileUtility->createZIPFromDir($backupTarget->path(''), $zipFile->getRealPath());

		// Delete the backup target after the zip was created
		$fileUtility->deleteRecursive($backupTarget->path(''));
	}

	/**
	 * @param string $name
	 *
	 * @throws IOException
	 */
	public function delete(string $name): void {
		$this->fileSystem->down('backup')->delete($name);
	}

	/**
	 * @param string $name
	 *
	 * @return SplFileInfo
	 * @throws IOException
	 */
	public function get(string $name): SplFileInfo {
		$filesystem = $this->fileSystem->down('backup');
		return $filesystem->get($name);
	}
}