<?php

declare(strict_types=1);

namespace asmaru\cms\core;

/**
 * Class TypeDefinitionManager
 *
 * @package asmaru\cms\core
 */
class TypeDefinitionManager {

	/**
	 * @var TypeDefinition[]
	 */
	private array $typeDefinitions = [];

	public function add(TypeDefinition $typeDefinition): void {
		$this->typeDefinitions[$typeDefinition->getName()] = $typeDefinition;
	}

	/**
	 * @return TypeDefinition[]
	 */
	public function all(): array {
		return $this->typeDefinitions;
	}

	public function getTypeDefinitionByType(string $type): ?TypeDefinition {
		return $this->typeDefinitions[$type] ?? null;
	}
}