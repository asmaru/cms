<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\cms\core\image\ImageService;
use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\search\SearchService;
use asmaru\cms\core\store\Picture;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\io\IOException;
use Exception;
use function count;
use const IMAGETYPE_WEBP;

/**
 * Class WarmUpCommand
 *
 * @package asmaru\cms\backend\cli\command
 * @noinspection PhpUnused
 */
class WarmUpCommand implements CommandInterface {

	public function __construct(
		private readonly ImageService $imageService,
		private readonly StoreRootFolder $storeRootFolder,
		private readonly SearchService $searchService
	) {
	}

	/**
	 * @throws IOException
	 * @throws Exception
	 */
	public function execute(Input $input, StyledOutput $output): CommandResult {
		$output->writeLine('Warming up the cache ...');
		$filter = new SearchFilter();
		$filter->type = Picture::type();
		$pictures = $this->searchService->find($this->storeRootFolder, $filter);
		$resolutions = $this->imageService->getResolutions();
		$max = count($resolutions) * count($pictures);
		$processed = 0;
		/** @var Picture $picture */
		foreach ($pictures->toArray() as $picture) {
			foreach ($resolutions as $resolution) {
				$output->progress($max, $processed);
				$this->imageService->getImage($picture, $resolution);
				$this->imageService->getImage($picture, $resolution, IMAGETYPE_WEBP);
				$processed++;
			}
		}
		return CommandResult::success();
	}

	public function help(): string {
		return 'Resize all images for all available resolutions';
	}
}