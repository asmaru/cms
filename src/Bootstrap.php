<?php

declare(strict_types=1);

namespace asmaru\cms;

use asmaru\cli\StyledOutput;
use asmaru\cli\writer\StdOutWriter;
use asmaru\cms\backend\callback\ClearCacheCallback;
use asmaru\cms\backend\callback\CopyMetaDataCallback;
use asmaru\cms\backend\callback\DeleteMetaDataCallback;
use asmaru\cms\backend\cli\CommandDispatcher;
use asmaru\cms\backend\cli\ConsoleExceptionHandler;
use asmaru\cms\backend\event\CopyElementEvent;
use asmaru\cms\backend\event\OnBeforeDeleteElementEvent;
use asmaru\cms\backend\event\OnUpdateResourceEvent;
use asmaru\cms\backend\service\BackupService;
use asmaru\cms\backend\service\InstallService;
use asmaru\cms\core\Context;
use asmaru\cms\core\event\EventDispatcher;
use asmaru\cms\core\ExceptionHandler;
use asmaru\cms\core\SettingsManager;
use asmaru\cms\frontend\callback\LogAccessCallback;
use asmaru\cms\frontend\event\ViewPageEvent;
use asmaru\di\ObjectManager;
use asmaru\http\App;
use asmaru\http\Path;
use asmaru\http\Request;
use asmaru\io\FileSystem;
use asmaru\io\IOException;
use PHPUnit\Framework\Attributes\CodeCoverageIgnore;
use ReflectionException;
use function ob_flush;
use function ob_start;

#[CodeCoverageIgnore]
class Bootstrap {

	private readonly ObjectManager $objectManager;

	private readonly Context $context;

	/**
	 * @throws IOException
	 */
	public function __construct(string $rootPath, string $pharPath) {
		$this->context = new Context($rootPath, $pharPath, ASMARU_VERSION);
		(new InstallService($this->context))->install();
		$this->objectManager = ObjectManagerFactory::build($this->context);
	}

	/**
	 * @throws IOException
	 * @throws ReflectionException
	 */
	public function bootCLI(array $argv): void {
		$this->objectManager->service(StyledOutput::class, function () {
			return new StyledOutput(new StdOutWriter());
		});

		$this->objectManager->service(Request::class, function (): Request {
			return new Request('GET', new Path(''), '');
		});

		$this->objectManager->service(CommandDispatcher::class, function (): CommandDispatcher {
			return new CommandDispatcher($this->objectManager, $this->objectManager->make(StyledOutput::class));
		});

		$this->objectManager->register(BackupService::class, new BackupService(new FileSystem($this->context->getPath(''))));

		ConsoleExceptionHandler::init($this->objectManager);

		/** @var CommandDispatcher $cli */
		$cli = $this->objectManager->make(CommandDispatcher::class);
		$cli->run($argv);
	}

	public function bootWeb(Request $request): void {

		ob_start();

		/** @var SettingsManager $settings */
		$settings = $this->objectManager->make(SettingsManager::class);
		if (!$settings->isDebugMode()) {
			$errorHandler = $this->objectManager->make(ExceptionHandler::class);
			set_exception_handler([$errorHandler, 'handleException']);
			set_error_handler([$errorHandler, 'handleError']);
		}

		$this->objectManager->service(Request::class, function () use ($request): Request {
			return $request;
		});

		$this->initEvents();

		/** @var App $app */
		$app = $this->objectManager->make(App::class);

		(new Endpoints())->register($app);

		$app->run()->send();

		ob_flush();
	}

	private function initEvents(): void {
		/** @var EventDispatcher $eventDispatcher */
		$eventDispatcher = $this->objectManager->make(EventDispatcher::class);

		$eventDispatcher->on(ViewPageEvent::class, LogAccessCallback::class);

		$eventDispatcher->on(OnBeforeDeleteElementEvent::class, DeleteMetaDataCallback::class);
		$eventDispatcher->on(OnBeforeDeleteElementEvent::class, ClearCacheCallback::class);

		$eventDispatcher->on(CopyElementEvent::class, CopyMetaDataCallback::class);

		$eventDispatcher->on(OnUpdateResourceEvent::class, ClearCacheCallback::class);
	}
}