<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use asmaru\io\FileSystem;

/**
 * Class TemporaryFileSystem
 *
 * This is just a facade to inject the temp directory.
 *
 * @package asmaru\cms\core
 * @codeCoverageIgnore
 */
class TemporaryFileSystem extends FileSystem {

}