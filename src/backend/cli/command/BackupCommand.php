<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\cms\core\Context;
use asmaru\io\FileSystem;
use asmaru\io\FileUtility;
use asmaru\io\IOException;
use SplFileInfo;
use function date;
use function sprintf;
use const DIRECTORY_SEPARATOR;

/**
 * Class BackupCommand
 *
 * @package asmaru\cms\backend\cli\command
 * @noinspection PhpUnused
 */
class BackupCommand implements CommandInterface {

	public function __construct(private readonly Context $context) {
	}

	/**
	 * @param Input $input
	 * @param StyledOutput $output
	 *
	 * @return CommandResult
	 * @throws IOException
	 */
	public function execute(Input $input, StyledOutput $output): CommandResult {

		if ($input->hasOption('l')) {
			$output->infoLine('Available backups:');
			$fs = new FileSystem($this->context->getPath('backup'));
			$list = [];
			/** @var SplFileInfo $backup */
			foreach ($fs->files(['zip']) as $backup) {
				$list[] = [
					$backup->getBasename(),
					round($backup->getSize() / 1024 / 1024, 2) . ' MB',
					date('Y-m-d', $backup->getCTime())
				];
			}
			$output->table($list, ['Filename', 'Size', 'Date']);
			return CommandResult::success();
		}

		if ($input->hasOption('d')) {
			$output->writeLine('Delete old backups ...');
			$fs = new FileSystem($this->context->getPath('backup'));
			foreach ($fs->files(['zip']) as $backup) {
				if (time() - $backup->getCTime() > (60 * 60 * 24 * 90)) {
					$output->successLine(sprintf('Delete file %s ...', $backup->getBasename()));
					$fs->delete($backup->getBasename());
				}
			}
			return CommandResult::success();
		}

		$output->writeLine('Start backup ...');
		$steps = 4;
		$fileUtility = new FileUtility();
		$output->progress($steps, 0);

		$backupFileName = 'backup_' . date('Ymd', time()) . '_' . time();
		$backupName = 'backup' . DIRECTORY_SEPARATOR . $backupFileName;
		$backupTarget = $this->context->getRootPath() . DIRECTORY_SEPARATOR . $backupName;
		mkdir($backupTarget);
		$output->progress($steps, 1);

		$fileUtility->copyRecursive($this->context->getRootPath() . DIRECTORY_SEPARATOR . 'data', $backupTarget . DIRECTORY_SEPARATOR . 'data');
		$output->progress($steps, 2);

		$zipFilename = $this->context->getRootPath() . DIRECTORY_SEPARATOR . 'backup' . DIRECTORY_SEPARATOR . $backupFileName . '.zip';
		$fileUtility->createZIPFromDir($backupTarget, $zipFilename);
		$output->progress($steps, 3);

		$fileUtility->deleteRecursive($backupTarget);
		$output->progress($steps, 4);
		$output->newLine();
		$output->newLine();
		$output->successLine(sprintf('Backup was written to %s', $backupName));

		return CommandResult::success();
	}

	/**
	 * @return string
	 */
	public function help(): string {
		return 'Create a complete backup. Use parameter -l to list existing backups.';
	}
}