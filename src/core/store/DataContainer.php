<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

interface DataContainer {

	public function getData();

	public function getDataValue(string $name);

	public function save($data): void;

	public function setDataValue(string $name, $value): void;
}