<?php

declare(strict_types=1);

namespace asmaru\cms\core\search;

class Query {

	private ?string $path = null;

	private ?string $term = null;

	private ?string $type = null;

	private array $fields = [];

	private ?string $sortBy = null;

	private bool $sortAsc = false;

	private ?int $limit = null;

	private bool $includeContent = false;

	/**
	 * @return array
	 */
	public function getFields(): array {
		return $this->fields;
	}

	/**
	 * @param array $fields
	 */
	public function setFields(array $fields): void {
		$this->fields = $fields;
	}

	/**
	 * @return int|null
	 */
	public function getLimit(): ?int {
		return $this->limit;
	}

	/**
	 * @param int|null $limit
	 */
	public function setLimit(?int $limit): void {
		$this->limit = $limit;
	}

	/**
	 * @return string|null
	 */
	public function getPath(): ?string {
		return $this->path;
	}

	/**
	 * @param string|null $path
	 */
	public function setPath(?string $path): void {
		$this->path = $path;
	}

	/**
	 * @return string|null
	 */
	public function getSortBy(): ?string {
		return $this->sortBy;
	}

	/**
	 * @param string|null $sortBy
	 */
	public function setSortBy(?string $sortBy): void {
		$this->sortBy = $sortBy;
	}

	/**
	 * @return string|null
	 */
	public function getTerm(): ?string {
		return $this->term;
	}

	/**
	 * @param string|null $term
	 */
	public function setTerm(?string $term): void {
		$this->term = $term;
	}

	/**
	 * @return string|null
	 */
	public function getType(): ?string {
		return $this->type;
	}

	/**
	 * @param string|null $type
	 */
	public function setType(?string $type): void {
		$this->type = $type;
	}

	/**
	 * @return bool
	 */
	public function isIncludeContent(): bool {
		return $this->includeContent;
	}

	/**
	 * @param bool $includeContent
	 */
	public function setIncludeContent(bool $includeContent): void {
		$this->includeContent = $includeContent;
	}

	/**
	 * @return bool
	 */
	public function isSortAsc(): bool {
		return $this->sortAsc;
	}

	/**
	 * @param bool $sortAsc
	 */
	public function setSortAsc(bool $sortAsc): void {
		$this->sortAsc = $sortAsc;
	}

}