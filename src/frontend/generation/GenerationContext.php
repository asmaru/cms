<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation;

use asmaru\cms\core\store\StoreElement;

/**
 * Class GenerationContext
 *
 * @package asmaru\cms\frontend\generation
 */
class GenerationContext {

	private StoreElement $storeElement;

	private array $log = [];

	private readonly UrlFactory $urlFactory;

	public function __construct(UrlFactory $urlFactory) {
		$this->urlFactory = $urlFactory;
	}

	/**
	 * @return mixed[]
	 */
	public function getErrors(): array {
		return $this->log;
	}

	public function getUrlFactory(): UrlFactory {
		return $this->urlFactory;
	}

	public function hasErrors(): bool {
		return !empty($this->log);
	}

	public function logError(string $message): void {
		$this->log[] = $this->getStoreElement()->getPath() . ': ' . $message;
	}

	/**
	 * @return StoreElement
	 */
	public function getStoreElement(): StoreElement {
		return $this->storeElement;
	}

	/**
	 * @param StoreElement $storeElement
	 */
	public function setStoreElement(StoreElement $storeElement): void {
		$this->storeElement = $storeElement;
	}

	public function uri(string $path): string {
		return $this->urlFactory->forPath($path);
	}
}