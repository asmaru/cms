<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\mustache\Context;
use asmaru\mustache\Helper;

/**
 * Class DateHelper
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class DateHelper implements Helper {

	final const DEFAULT_FORMAT = 'Y-m-d';

	public function render($content, Context $context, array $params = []): string {
		if (empty($content)) return '';
		$format = $params['format'] ?? self::DEFAULT_FORMAT;
		return date($format, (int)$content);
	}

	public function renderBefore(): bool {
		return true;
	}
}