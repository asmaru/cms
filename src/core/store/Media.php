<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

use asmaru\cms\core\error\UploadSizeException;
use asmaru\cms\core\SettingsManager;
use asmaru\cms\core\TemporaryFileSystem;
use asmaru\io\IOException;
use Exception;
use SplFileInfo;

class Media extends StoreElement implements DataContainer {

	/**
	 * @var SettingsManager
	 */
	protected SettingsManager $settingsManager;

	/**
	 * @var TemporaryFileSystem
	 */
	protected TemporaryFileSystem $temporaryFileSystem;

	/**
	 * @var Base64UploadService
	 */
	protected Base64UploadService $uploadService;

	public function __construct(string $filename, Folder $parent, SettingsManager $settingsManager, TemporaryFileSystem $temporaryFileSystem, Base64UploadService $uploadService) {
		parent::__construct($filename, $parent);
		$this->settingsManager = $settingsManager;
		$this->temporaryFileSystem = $temporaryFileSystem;
		$this->uploadService = $uploadService;
	}

	public function getData() {
		return null;
	}

	public function getDataValue(string $name) {
		return null;
	}

	/**
	 * @param $data
	 *
	 * @throws IOException
	 * @throws UploadSizeException
	 */
	public function save($data): void {
		$tmpFile = $this->uploadService->convertDataUriToFile($data);
		$this->getParent()->getFileSystem()->import($tmpFile->getRealPath(), $this->getName());
		$this->temporaryFileSystem->delete($tmpFile->getRealPath());
	}

	/**
	 * @throws Exception
	 */
	public function setDataValue(string $name, $value): void {
		throw new Exception();
	}

	public static function matchType(SplFileInfo $file): bool {
		return true;
	}
}