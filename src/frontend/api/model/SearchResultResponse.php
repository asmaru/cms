<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\api\model;

use asmaru\cms\core\store\StoreElement;
use asmaru\cms\core\store\StoreElementList;
use asmaru\cms\core\utility\PaginatedList;
use asmaru\cms\core\utility\SortedList;
use JsonSerializable;
use OpenApi\Attributes as OA;

#[OA\Schema]
class SearchResultResponse implements JsonSerializable {

	#[OA\Property]
	private int $total;

	#[OA\Property(items: new OA\Items(type: 'string'))]
	private array $items;

	public function __construct(StoreElementList $items, int $limit) {
		$this->total = $items->count();
		$sortedList = new SortedList($items->toArray());
		$paginatedList = new PaginatedList($sortedList->getSorted('published'), $limit);
		/** @var StoreElement $item */
		foreach ($paginatedList->getPage(1) as $item) {
			$this->items[] = $item->getPublicPath();
		}
	}

	public function jsonSerialize(): array {
		return [
			'total' => $this->total,
			'items' => $this->items
		];
	}
}