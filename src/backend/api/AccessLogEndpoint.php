<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api;

use asmaru\cms\backend\api\model\RequestListResponse;
use asmaru\cms\core\logging\AccessLogService;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\http\rest\Resource;
use OpenApi\Attributes as OA;

class AccessLogEndpoint extends AbstractResource implements Resource {

	private readonly AccessLogService $accessLogService;

	public function __construct(Request $request, AccessLogService $accessLogService) {
		parent::__construct($request);
		$this->request = $request;
		$this->accessLogService = $accessLogService;
	}

	#[OA\Get(
		path: '/api/stats',
		operationId: 'getStats',
		parameters: [
			new OA\Parameter(name: 'onlyErrors', in: 'query', schema: new OA\Schema(type: 'boolean'))
		],
		responses: [
			new OA\Response(response: 200, description: '', content: new OA\JsonContent(type: RequestListResponse::class)),
			new OA\Response(response: 401, description: '')
		]
	)]
	public function getCollection(): Response {
		$data = new RequestListResponse($this->request->contains('onlyErrors') ? $this->accessLogService->getErrors(100) : $this->accessLogService->get(100));
		return (new Response())->json($data);
	}
}