<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation;

use asmaru\cms\core\store\PageType;
use asmaru\cms\core\TypeDefinitionManager;
use asmaru\io\FileSystem;
use asmaru\io\IOException;

/**
 * Class TemplateResolver
 *
 * @package asmaru\cms\frontend\generation
 */
class TemplateResolver {

	private readonly TypeDefinitionManager $typeDefinitionManager;

	private readonly FileSystem $fileSystem;

	/**
	 * TemplateResolver constructor.
	 *
	 * @param FileSystem $fileSystem
	 * @param TypeDefinitionManager $typeDefinitionManager
	 */
	public function __construct(FileSystem $fileSystem, TypeDefinitionManager $typeDefinitionManager) {
		$this->fileSystem = $fileSystem;
		$this->typeDefinitionManager = $typeDefinitionManager;
	}

	/**
	 * @param string $name
	 *
	 * @return string
	 * @throws IOException
	 */
	public function getPartial(string $name): string {
		return HtmlMinifier::minify($this->fileSystem->down('partials')->read($name));
	}

	/**
	 * @return array
	 * @throws IOException
	 */
	public function getPartials(): array {
		$partialsFileSystem = $this->fileSystem->down('partials');
		$partials = [];
		foreach ($partialsFileSystem->readAll(['html'], true) as $name => $partial) {
			$partials[$name] = HtmlMinifier::minify($partial);
		}
		return $partials;
	}

	/**
	 * @param PageType $type
	 *
	 * @return string
	 * @throws IOException
	 */
	public function getTemplate(PageType $type): string {
		$typeDefinition = $this->typeDefinitionManager->getTypeDefinitionByType($type->getName());
		$name = $typeDefinition->getName();
		$key = $name . '.html';
		if (!$this->fileSystem->isFile($key)) {
			throw new IOException(sprintf('template for type "%s" not found', $name));
		}
		return $this->fileSystem->read($key);
	}
}