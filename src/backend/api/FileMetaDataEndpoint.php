<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api;

use asmaru\cms\core\meta\FileMetaData;
use asmaru\cms\core\meta\MetaDataService;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\http\Path;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\http\rest\Resource;
use asmaru\io\IOException;

/**
 * Class FileMetaDataEndpoint
 *
 * @package asmaru\cms\backend\api
 */
class FileMetaDataEndpoint extends AbstractResource implements Resource {

	private readonly MetaDataService $metaDataService;

	private readonly StoreRootFolder $storeRootFolder;

	/**
	 * MetaEndpoint constructor.
	 *
	 * @param Request $request
	 * @param MetaDataService $metaDataService
	 * @param StoreRootFolder $storeRootFolder
	 */
	public function __construct(Request $request, MetaDataService $metaDataService, StoreRootFolder $storeRootFolder) {
		parent::__construct($request);
		$this->metaDataService = $metaDataService;
		$this->storeRootFolder = $storeRootFolder;
	}

	/**
	 * @param string $name
	 *
	 * @return Response
	 * @throws IOException
	 */
	public function get(string $name): Response {
		$element = $this->storeRootFolder->resolve(new Path($name));
		return (new Response())->json($this->metaDataService->get($element), Response::HTTP_OK);
	}

	/**
	 * @param string $name
	 * @param array $data
	 *
	 * @return Response
	 * @throws IOException
	 */
	public function put(string $name, array $data): Response {
		$element = $this->storeRootFolder->resolve(new Path($name));
		$this->metaDataService->set($element, FileMetaData::jsonUnserialize($data));
		return (new Response());
	}
}