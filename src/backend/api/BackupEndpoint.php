<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api;

use asmaru\cms\backend\api\model\FileDataResponse;
use asmaru\cms\backend\service\BackupService;
use asmaru\cms\core\TemporaryFileSystem;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\http\rest\Resource;
use asmaru\io\IOException;
use function sha1;
use function uniqid;

/**
 * Class BackupEndpoint
 *
 * @package asmaru\cms\backend\api
 */
class BackupEndpoint extends AbstractResource implements Resource {

	private readonly BackupService $backupService;

	private readonly TemporaryFileSystem $temporaryFileSystem;

	public function __construct(Request $request, BackupService $backupService, TemporaryFileSystem $temporaryFileSystem) {
		parent::__construct($request);
		$this->backupService = $backupService;
		$this->temporaryFileSystem = $temporaryFileSystem;
	}

	/**
	 * @throws IOException
	 */
	public function delete(string $name): Response {
		$this->backupService->delete($name);
		return Response::ok();
	}

	/**
	 * @throws IOException
	 */
	public function get(string $name): Response {
		$file = $this->backupService->get($name);
		$newName = sha1(uniqid('', true)) . '.zip';
		$this->temporaryFileSystem->import($file->getRealPath(), $newName);
		return (new Response())->json($this->request->uri('/temp/' . $newName));
	}

	/**
	 * @throws IOException
	 */
	public function getCollection(): Response {
		$backups = [];
		foreach ($this->backupService->all() as $fileInfo) {
			$backups[] = new FileDataResponse($fileInfo);
		}
		return (new Response())->json($backups);
	}

	/**
	 * @param array $data
	 *
	 * @return Response
	 * @throws IOException
	 */
	public function post(array $data): Response {
		$this->backupService->create();
		return Response::created();
	}
}