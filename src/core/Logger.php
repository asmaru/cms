<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use asmaru\io\FileSystem;
use function sprintf;
use const PHP_EOL;

/**
 * Class Logger
 *
 * @package asmaru\cms\backend\util
 */
class Logger {

	final const FILENAME = 'error.log';

	private readonly FileSystem $fileSystem;

	public function __construct(FileSystem $fileSystem) {
		$this->fileSystem = $fileSystem;
	}

	public function empty(): void {
		$this->fileSystem->delete(self::FILENAME);
	}

	public function error(string $message): void {
		$message = sprintf('[%s] [ERROR] %s', date('Y/m/d H:i:s', time()), $message) . PHP_EOL;
		$this->fileSystem->append(self::FILENAME, $message);
	}

	public function get(): string {
		return $this->fileSystem->read(self::FILENAME);
	}

	public function info(string $message): void {
		$message = sprintf('[%s] [INFO] %s', date('Y/m/d H:i:s', time()), $message) . PHP_EOL;
		$this->fileSystem->append(self::FILENAME, $message);
	}
}