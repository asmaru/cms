<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

use ArrayAccess;
use Countable;
use Iterator;

class StoreElementList implements ArrayAccess, Countable, Iterator {

	private array $elements;

	private int $position = 0;

	public function __construct(array $elements = []) {
		$this->elements = $elements;
	}

	public function add(StoreElement $child): void {
		$this->elements[] = $child;
	}

	public function count(): int {
		return count($this->elements);
	}

	public function current(): mixed {
		return $this->elements[$this->position];
	}

	public function key(): mixed {
		return $this->position;
	}

	public function next(): void {
		++$this->position;
	}

	public function offsetExists($offset): bool {
		return isset($this->elements[$offset]);
	}

	public function offsetGet($offset): mixed {
		return $this->elements[$offset] ?? null;
	}

	public function offsetSet($offset, $value): void {
		$this->elements[$offset] = $value;
	}

	public function offsetUnset($offset): void {
		unset($this->elements[$offset]);
	}

	public function rewind(): void {
		$this->position = 0;
	}

	/**
	 * @return mixed[]
	 */
	public function toArray(): array {
		return $this->elements;
	}

	/**
	 * @return mixed[]
	 */
	public function toJsonArray(): array {
		return array_reduce($this->elements, function (array $carry, StoreElement $item): array {
			$carry[] = $item->jsonSerialize();
			return $carry;
		}, []);
	}

	public function valid(): bool {
		return isset($this->elements[$this->position]);
	}
}