<?php

declare(strict_types=1);

namespace asmaru\cms\core\logging;

use DateTimeInterface;
use JsonSerializable;

/**
 * Class RequestData
 *
 * @package asmaru\cms\core\logging
 */
class RequestData implements JsonSerializable {

	final const STATUS_FOUND = 200;

	final const STATUS_NOT_FOUND = 404;

	/**
	 * @var int
	 */
	private int $id;

	/**
	 * @var string
	 */
	private string $protocol;

	/**
	 * @var string
	 */
	private string $path;

	/**
	 * @var string
	 */
	private string $userAgent;

	/**
	 * @var string
	 */
	private string $referer;

	/**
	 * @var bool
	 */
	private bool $dnt;

	/**
	 * @var int
	 */
	private int $time;

	/**
	 * @var int
	 */
	private int $status;

	/**
	 * @var string
	 */
	private string $acceptLanguage;

	/**
	 * @var string
	 */
	private string $accept;

	/**
	 * @var string
	 */
	private string $acceptEncoding;

	/**
	 * @return string
	 */
	public function getAccept(): string {
		return $this->accept;
	}

	/**
	 * @param string $accept
	 */
	public function setAccept(string $accept): void {
		$this->accept = $accept;
	}

	/**
	 * @return string
	 */
	public function getAcceptEncoding(): string {
		return $this->acceptEncoding;
	}

	/**
	 * @param string $acceptEncoding
	 */
	public function setAcceptEncoding(string $acceptEncoding): void {
		$this->acceptEncoding = $acceptEncoding;
	}

	/**
	 * @return string
	 */
	public function getAcceptLanguage(): string {
		return $this->acceptLanguage;
	}

	/**
	 * @param string $acceptLanguage
	 */
	public function setAcceptLanguage(string $acceptLanguage): void {
		$this->acceptLanguage = $acceptLanguage;
	}

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void {
		$this->id = $id;
	}

	/**
	 * Specify data which should be serialized to JSON
	 *
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return array{protocol: string|null, path: string|null, userAgent: string|null, referer: string|null, dnt:
	 *     string, time: string, status: int, acceptLanguage: string, accept: string, acceptEncoding: string} data
	 *     which can be serialized by <b>json_encode</b>, which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize(): array {
		return [
			'protocol' => $this->getProtocol(),
			'path' => $this->getPath(),
			'userAgent' => $this->getUserAgent(),
			'referer' => $this->getReferer(),
			'dnt' => $this->isDnt() ? '1' : '0',
			'time' => date(DateTimeInterface::W3C, $this->getTime()),
			'status' => $this->getStatus(),
			'acceptLanguage' => $this->acceptLanguage,
			'accept' => $this->accept,
			'acceptEncoding' => $this->acceptEncoding
		];
	}

	/**
	 * @return string|null
	 */
	public function getProtocol(): ?string {
		return $this->protocol;
	}

	/**
	 * @param string|null $protocol
	 */
	public function setProtocol(?string $protocol): void {
		$this->protocol = $protocol;
	}

	/**
	 * @return string|null
	 */
	public function getPath(): ?string {
		return $this->path;
	}

	/**
	 * @param string|null $path
	 */
	public function setPath(?string $path): void {
		$this->path = $path;
	}

	/**
	 * @return string|null
	 */
	public function getUserAgent(): ?string {
		return $this->userAgent;
	}

	/**
	 * @param string|null $userAgent
	 */
	public function setUserAgent(?string $userAgent): void {
		$this->userAgent = $userAgent;
	}

	/**
	 * @return string|null
	 */
	public function getReferer(): ?string {
		return $this->referer;
	}

	/**
	 * @param string|null $referer
	 */
	public function setReferer(?string $referer): void {
		$this->referer = $referer;
	}

	/**
	 * @return bool
	 */
	public function isDnt(): bool {
		return $this->dnt;
	}

	/**
	 * @param bool $dnt
	 */
	public function setDnt(bool $dnt): void {
		$this->dnt = $dnt;
	}

	/**
	 * @return int
	 */
	public function getTime(): int {
		return $this->time;
	}

	/**
	 * @param int $time
	 */
	public function setTime(int $time): void {
		$this->time = $time;
	}

	/**
	 * @return int
	 */
	public function getStatus(): int {
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus(int $status): void {
		$this->status = $status;
	}
}