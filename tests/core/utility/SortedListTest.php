<?php

declare(strict_types=1);

namespace asmaru\cms\core\utility;

use asmaru\cms\core\store\DefaultFields;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\TypeDefinition;
use asmaru\cms\core\TypeDefinitionField;
use asmaru\cms\core\TypeDefinitionManager;
use asmaru\di\ObjectManager;
use asmaru\io\FileUtility;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use function mkdir;
use function time;
use function touch;
use const DIRECTORY_SEPARATOR;

class SortedListTest extends TestCase {

	private string $dir = '';

	public function setUp(): void {
		parent::setUp();
		$this->dir = __DIR__ . DIRECTORY_SEPARATOR . time();
		mkdir($this->dir);
	}

	public function tearDown(): void {
		parent::tearDown();
		(new FileUtility())->deleteRecursive($this->dir);
	}

	#[Test]
	public function testSortedList() {

		// setup test environment

		$om = new ObjectManager();

		$typeDefinitionManager = new TypeDefinitionManager();
		$typeDefinitionManager->add(
			(new TypeDefinition('testtype'))->addField(
				(new TypeDefinitionField('published'))
					->setType(TypeDefinitionField::TYPE_INTEGER)
			)
		);
		$om->service(TypeDefinitionManager::class, $typeDefinitionManager);

		$storeRootFolder = new StoreRootFolder($this->dir, null, $om);
		$om->service(StoreRootFolder::class, $storeRootFolder);

		for ($i = 1; $i <= 3; $i++) {
			$elName = 'test' . $i . '.json';
			touch($this->dir . DIRECTORY_SEPARATOR . $elName);
			$el = $storeRootFolder->get($elName);
			$el->save([
				DefaultFields::TYPE => 'testtype',
				DefaultFields::DISABLED => false,
				DefaultFields::HIDDEN => false,
				'published' => $i
			]);
		}

		$elements = $storeRootFolder->getChildren();
		$sortedList = new SortedList($elements);

		// Sort ascending
		$sortedElements = $sortedList->getSorted('published', false);
		$this->assertEquals(1, $sortedElements[0]->getDataValue('published'));
		$this->assertEquals(2, $sortedElements[1]->getDataValue('published'));
		$this->assertEquals(3, $sortedElements[2]->getDataValue('published'));

		// Sort descending
		$sortedElements = $sortedList->getSorted('published', true);
		$this->assertEquals(3, $sortedElements[0]->getDataValue('published'));
		$this->assertEquals(2, $sortedElements[1]->getDataValue('published'));
		$this->assertEquals(1, $sortedElements[2]->getDataValue('published'));
	}
}