<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\di\ObjectManager;
use DirectoryIterator;
use ReflectionClass;
use ReflectionException;
use function strlen;

/**
 * Class HelpCommand
 *
 * @package asmaru\cms\backend\cli\command
 * @noinspection PhpUnused
 */
class HelpCommand implements CommandInterface {

	/**
	 * @var ObjectManager
	 */
	private readonly ObjectManager $objectManager;

	/**
	 * HelpCommand constructor.
	 *
	 * @param ObjectManager $objectManager
	 */
	public function __construct(ObjectManager $objectManager) {
		$this->objectManager = $objectManager;
	}

	/**
	 * @param StyledOutput $output
	 * @param array $arguments
	 *
	 * @throws ReflectionException
	 */
	public function execute(Input $input, StyledOutput $output): CommandResult {
		$output->writeLine('   ___    ____ __  ___ ___    ___   __  __  ');
		$output->writeLine('  / _ |  / __//  |/  // _ |  / _ \\ / / / /  ');
		$output->writeLine(' / __ | _\\ \\ / /|_/ // __ | / , _// /_/ /   ');
		$output->writeLine('/_/ |_|/___//_/  /_//_/ |_|/_/|_| \\____/    ');
		$output->newLine();

		$output->infoLine('Available commands');
		$output->newLine();
		$commands = [];

		// Get all classes which implements the CommandInterface
		foreach (new DirectoryIterator(__DIR__) as $fileInfo) {
			if ($fileInfo->isDir()) {
				continue;
			}
			if ($fileInfo->isDot()) {
				continue;
			}
			if (preg_match('/^([\w]+)Command\.php$/', $fileInfo->getBasename(), $matches)) {
				$commandClassName = '\\asmaru\\cms\\backend\\cli\\command\\' . ucfirst($matches[1]) . 'Command';
				$rf = new ReflectionClass($commandClassName);
				if ($rf->implementsInterface(CommandInterface::class)) {
					/** @var CommandInterface $commandInstance */
					$commandInstance = $this->objectManager->make($commandClassName);
					$commands[lcfirst($matches[1])] = $commandInstance->help();
				}
			}
		}

		// Get the longest command
		$commandMaxLength = 0;
		foreach ($commands as $command => $help) {
			$commandMaxLength = max($commandMaxLength, strlen($command));
		}

		// writeLine the command and help list
		foreach ($commands as $command => $help) {
			$output->info($command);
			$output->write(str_pad(' ', ($commandMaxLength + 4) - strlen($command)) . $help);
			$output->newLine();
		}

		return CommandResult::success();
	}

	/**
	 * @return string
	 */
	public function help(): string {
		return 'Show available commands';
	}
}