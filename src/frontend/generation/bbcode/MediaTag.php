<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\bbcode;

use asmaru\bbcode\Rule;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\TemporaryFileSystem;
use asmaru\cms\frontend\generation\GenerationContext;
use asmaru\http\Path;
use asmaru\io\IOException;
use function preg_replace_callback;
use function sprintf;

class MediaTag implements Rule {

	/**
	 * @var GenerationContext
	 */
	private readonly GenerationContext $generationContext;

	/**
	 * @var StoreRootFolder
	 */
	private readonly StoreRootFolder $storeRootFolder;

	/**
	 * @var TemporaryFileSystem
	 */
	private readonly TemporaryFileSystem $temporaryFileSystem;

	public function __construct(GenerationContext $generationContext, StoreRootFolder $storeRootFolder, TemporaryFileSystem $temporaryFileSystem) {
		$this->generationContext = $generationContext;
		$this->storeRootFolder = $storeRootFolder;
		$this->temporaryFileSystem = $temporaryFileSystem;
	}

	public function parse(string $input): string {
		$pattern = '/\[media(?:=(?P<value>[^\]]*))?\](?P<text>.*?)\[\/media\]/is';
		return preg_replace_callback(/**
		 * @throws IOException
		 */ $pattern, function ($matches): string {
			try {
				$text = $matches['text'];
				$value = $matches['value'];

				// Get the image by name
				$name = !empty($value) ? $value : $text;
				$path = new Path($name);
				$media = $this->storeRootFolder->resolve($path);

				$this->temporaryFileSystem->importDeep($media, $path->__toString());

				return sprintf('<a href="%s">%s</a>', $this->generationContext->uri('temp/' . $media->getPublicPath()), $text);

			} catch (GenerationException $e) {
				return $e->getMessage();
			}
		}, $input);
	}
}