<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\api;

use asmaru\cache\frontend\CacheFrontend;
use asmaru\cms\core\error\ValidationException;
use asmaru\cms\core\event\EventDispatcher;
use asmaru\cms\core\Logger;
use asmaru\cms\core\SettingsManager;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\frontend\event\ViewPageEvent;
use asmaru\cms\frontend\generation\bbcode\GenerationException;
use asmaru\cms\frontend\generation\PageGenerator;
use asmaru\http\ContentType;
use asmaru\http\Path;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\io\IOException;
use Exception;
use OpenApi\Attributes as OA;
use function intval;
use function max;
use function sprintf;

class PublicPageEndpoint extends AbstractResource {

	public function __construct(Request $request,
		private readonly SettingsManager $settingsManager,
		private readonly Logger $logger,
		private readonly CacheFrontend $cacheFrontend,
		private readonly PageGenerator $pageGenerator,
		private readonly StoreRootFolder $storeRootFolder,
		private readonly EventDispatcher $eventDispatcher) {
		parent::__construct($request);
	}

	/**
	 * Called when no page path was specified. Shows the default resource if set.
	 *
	 * @throws Exception
	 */
	public function getCollection(): Response {
		$defaultResourceName = $this->settingsManager->get(SettingsManager::PROPERTY_RESOURCE_DEFAULT);
		if (empty($defaultResourceName)) throw new \Exception(sprintf('Please define a default resource in "%s"!', SettingsManager::PROPERTY_RESOURCE_DEFAULT));
		return $this->get($defaultResourceName);
	}

	/**
	 * This is the entry point for all frontend page requests
	 *
	 * @throws IOException
	 * @throws ValidationException
	 */
	#[OA\Get(
		path: '/*',
		responses: [
			new OA\Response(response: 200, description: '', content: new OA\MediaType(mediaType: 'text/html'))
		]
	)]
	public function get(string $name): Response {
		try {
			$isCached = true;
			$content = $this->cacheFrontend->withCache($this->request->env('REQUEST_URI'), function () use ($name, &$isCached): string {
				$page = $this->getPage(Page::toInternalName($name));
				if ($page === null) {
					throw new Exception(sprintf('Page %s was not found, has an invalid type or is disabled!', $name));
				}
				$content = $this->render($page);
				if (empty($content)) {
					throw new GenerationException('Generated content is empty!');
				}
				$isCached = false;
				return $content;
			});
			$this->eventDispatcher->trigger(new ViewPageEvent(false));
			return (new Response())->withHeader('X-Asm-Cached', $isCached ? 'yes' : 'no')->setContent($content);
		} catch (Exception $e) {
			$this->logger->error($e->getMessage());
			$this->eventDispatcher->trigger(new ViewPageEvent(true));
			return $this->getErrorResponse($name);
		}
	}

	/**
	 * @throws IOException
	 */
	private function getPage(string $name): ?Page {
		$path = new Path($name);
		/** @var Page $page */
		$page = $this->storeRootFolder->resolve($path);
		if ($page === null || $page->getType() !== Page::type() || !$page->isVisible()) {
			$this->logger->error(sprintf('Page %s was not found or has invalid type!', $name));
			return null;
		}
		return $page;
	}

	/**
	 * @throws IOException
	 * @throws ValidationException
	 */
	private function render(Page $page): string {
		$currentPage = max(intval($this->request->get('page', '1')), 1);
		return $this->pageGenerator->render($page, $currentPage);
	}

	/**
	 * @throws IOException
	 * @throws ValidationException
	 * @throws Exception
	 */
	private function getErrorResponse(string $name): Response {
		$nameForErrorPage = Page::toInternalName($this->settingsManager->get(SettingsManager::PROPERTY_RESOURCE_NOT_FOUND));
		$resource = $this->getPage($nameForErrorPage);
		if ($resource === null) {
			throw new Exception(sprintf('Property "%s" is undefined or points to an invalid resource.', SettingsManager::PROPERTY_RESOURCE_NOT_FOUND));
		}
		$this->logger->info(sprintf('Resource "%s" was not found or is disabled, instead, resource "%s" is displayed.', $name, $nameForErrorPage));
		$content = $this->render($resource);
		$status = Response::HTTP_NOT_FOUND;
		return (new Response())->withHeader('x-robots-tag', 'noindex')->setContent($content, ContentType::HTML, $status);
	}
}