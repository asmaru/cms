CREATE TABLE IF NOT EXISTS request
(
    id
    INTEGER
    PRIMARY
    KEY
    AUTOINCREMENT,
    protocol
    TEXT
    NULL,
    path
    TEXT
    NULL,
    userAgent
    VARCHAR
(
    255
) NULL,
    referer VARCHAR
(
    255
) NULL,
    dnt INT NOT NULL,
    time INT NOT NULL,
    status INT NOT NULL,
    acceptLanguage TEXT NULL,
    accept TEXT NULL,
    acceptEncoding TEXT NULL
    );