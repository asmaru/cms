<?php

declare(strict_types=1);

namespace asmaru\cms\core\image;

class Resolution {

	public function __construct(
		private readonly string $name,
		private readonly ?int $width = null,
		private readonly ?int $height = null
	) {
	}

	public function getHeight(): ?int {
		return $this->height;
	}

	public function getName(): string {
		return $this->name;
	}

	public function getWidth(): ?int {
		return $this->width;
	}
}