<?php

declare(strict_types=1);

namespace asmaru\cms\backend\callback;

use asmaru\cms\core\event\Event;
use asmaru\cms\core\event\EventCallback;
use asmaru\cms\core\meta\MetaDataService;
use asmaru\cms\core\store\Folder;

/**
 * Class DeleteMetaDataHandler
 *
 * @package asmaru\cms\core\event
 */
class DeleteMetaDataCallback implements EventCallback {

	/**
	 * @var MetaDataService
	 */
	private readonly MetaDataService $metaDataService;

	/**
	 * DeleteMetaDataHandler constructor.
	 *
	 * @param MetaDataService $metaDataService
	 */
	public function __construct(MetaDataService $metaDataService) {
		$this->metaDataService = $metaDataService;
	}

	/**
	 * @param Event $event
	 */
	public function execute(Event $event): void {
		$element = $event->getStoreElement();
		if ($element instanceof Folder) {
			foreach ($element->getChildren(true) as $child) {
				$this->metaDataService->delete($child);
			}
		}
		$this->metaDataService->delete($element);
	}
}