<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\cms\core\image\ImageService;
use asmaru\gdlib\GDUtility;
use asmaru\io\IOException;
use asmaru\mustache\Context;
use asmaru\mustache\Helper;
use Exception;
use SplFileInfo;

/**
 * Class ImageColorGradientHelper
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class ImageColorGradientHelper implements Helper {

	/**
	 * @var ImageService
	 */
	private readonly ImageService $imageManager;

	/**
	 * ImageColorGradient constructor.
	 *
	 * @param ImageService $imageService
	 */
	public function __construct(ImageService $imageService) {
		$this->imageManager = $imageService;
	}

	/**
	 * @param $content
	 * @param Context $context
	 * @param array $params
	 *
	 * @return string
	 * @throws IOException
	 * @throws Exception
	 */
	public function render($content, Context $context, array $params = []): string {
		$image = $this->imageManager->getImageByName($content, $this->imageManager->getResolutionByName('gradient'));
		if ($image === null) {
			return '';
		}
		if ($image->isFile()) {
			return '';
		}
		$colors = $this->averageColorLeftToRight($image);
		$colorLeft = sprintf('rgb(%s,%s,%s)', $colors[0]['red'], $colors[0]['green'], $colors[0]['blue']);
		$colorRight = sprintf('rgb(%s,%s,%s)', $colors[1]['red'], $colors[1]['green'], $colors[1]['blue']);
		return 'background: linear-gradient(to right, ' . $colorLeft . ' 0%, ' . $colorRight . ' 100%);';
	}

	/**
	 * @return array<int, mixed[]>
	 */
	private function averageColorLeftToRight(SplFileInfo $image): array {
		$im = GDUtility::load($image->getRealPath());
		$sourceWidth = imagesx($im);
		$sourceHeight = imagesy($im);
		$im2 = imagecreate(20, 10);
		imagecopyresampled($im2, $im, 0, 0, 0, 0, 20, 10, $sourceWidth, $sourceHeight);
		$rgb = imagecolorat($im2, 0, 9);
		$colors = imagecolorsforindex($im2, $rgb);
		$rgb = imagecolorat($im2, 19, 9);
		$colors2 = imagecolorsforindex($im2, $rgb);
		return [$colors, $colors2];
	}

	public function renderBefore(): bool {
		return true;
	}
}