<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api\model;

use JsonSerializable;
use SplFileInfo;

class FileDataResponse implements JsonSerializable {

	/**
	 * @var SplFileInfo
	 */
	private readonly SplFileInfo $file;

	public function __construct(SplFileInfo $file) {
		$this->file = $file;
	}

	/**
	 * @return array{name: string, created: int, size: int|false}
	 */
	public function jsonSerialize(): array {
		return [
			'name' => $this->file->getBasename(),
			'created' => $this->file->getCTime(),
			'size' => $this->file->getSize()
		];
	}
}