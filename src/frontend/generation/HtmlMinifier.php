<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation;

/**
 * Class HtmlMinifier
 *
 * @package asmaru\cms\frontend\generation
 */
class HtmlMinifier {

	/**
	 * Removes linebreaks, whitespace and html comments.
	 *
	 * @param string $input
	 *
	 * @return string
	 */
	public static function minify(string $input): string {
		$input = implode(' ', array_map('trim', explode(PHP_EOL, $input)));
		$input = preg_replace('/>[\s]+</', '><', $input);
		return preg_replace('/<!--(.*?)-->/', '', $input);
	}
}