<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\cms\core\image\ImageService;
use asmaru\cms\frontend\generation\bbcode\GenerationException;
use asmaru\cms\frontend\generation\GenerationContext;
use asmaru\mustache\Context;
use asmaru\mustache\Helper;
use function sprintf;
use const IMG_JPG;
use const IMG_WEBP;

/**
 * Class ImgHelper
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class ImgHelper implements Helper {

	private readonly ImageService $imageManager;

	private readonly GenerationContext $generationContext;

	/**
	 * ImgHelper constructor.
	 *
	 * @param GenerationContext $generationContext
	 * @param ImageService $imageManager
	 */
	public function __construct(GenerationContext $generationContext, ImageService $imageManager) {
		$this->imageManager = $imageManager;
		$this->generationContext = $generationContext;
	}

	/**
	 * @param $content
	 * @param Context $context
	 * @param array $params
	 *
	 * @return string
	 */
	public function render($content, Context $context, array $params = []): string {
		try {
			// External
			if (str_starts_with((string)$content, 'http')) return $content;

			// Local file
			$res = $params['res'] ?? null;
			$format = $params['format'] ?? null;
			$resolution = $this->imageManager->getResolutionByName($res);
			$image = $this->imageManager->getImageByName($content, $resolution, $format === 'webp' ? IMG_WEBP : IMG_JPG);
			if ($image === null) {
				throw new GenerationException(sprintf('Image "%s:%s" not found!', $content, $resolution->getName()));
			}
			$path = $this->imageManager->getPublicPath($image);
			return $this->generationContext->uri($path);
		} catch (GenerationException $e) {
			$this->generationContext->logError($e->getMessage());
			return '';
		}
	}

	public function renderBefore(): bool {
		return true;
	}
}