<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\bbcode;

use asmaru\bbcode\Rule;
use asmaru\cms\core\search\QueryParser;
use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\search\SearchService;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\utility\SortedList;
use asmaru\cms\frontend\generation\TemplateResolver;
use asmaru\di\ObjectManager;
use asmaru\http\Path;
use asmaru\io\IOException;
use asmaru\mustache\Renderer;
use Exception;
use function html_entity_decode;

/**
 * Class AbstractListingShortCode
 *
 * Base class for listings
 *
 * @package asmaru\cms\frontend\generation\bbcode
 */
abstract class AbstractListingShortCode implements Rule {

	private readonly StoreRootFolder $storeRootFolder;

	private readonly SearchService $searchService;

	private readonly ObjectManager $objectManager;

	private readonly TemplateResolver $templateResolver;

	/**
	 * AbstractListingShortCode constructor.
	 *
	 * @param StoreRootFolder $storeRootFolder
	 * @param SearchService $searchService
	 * @param ObjectManager $objectManager
	 * @param TemplateResolver $templateResolver
	 */
	public function __construct(StoreRootFolder $storeRootFolder, SearchService $searchService, ObjectManager $objectManager, TemplateResolver $templateResolver) {
		$this->storeRootFolder = $storeRootFolder;
		$this->searchService = $searchService;
		$this->objectManager = $objectManager;
		$this->templateResolver = $templateResolver;
	}

	/**
	 * @param string $input
	 *
	 * @return string
	 * @throws IOException
	 * @throws Exception
	 */
	public function parse(string $input): string {
		return preg_replace_callback('/\[' . $this->getTagName() . '\(?([^)\]]*)\)?]/', function (array $matches): string {
			if (isset($matches[1])) {

				$query = QueryParser::fromString(html_entity_decode($matches[1]));

				$folder = $this->storeRootFolder;
				if (!empty($query->getPath())) {
					$folderFromParam = $this->storeRootFolder->resolve(new Path($query->getPath()));
					if ($folderFromParam !== null) $folder = $folderFromParam;
				}

				$filter = SearchFilter::fromQuery($query);
				$this->prepareFilter($filter);
				$elements = $this->searchService->find($folder, $filter)->toArray();

				// Sorting
				if (!empty($query->getSortBy())) {
					$sortedList = new SortedList($elements);
					$elements = $sortedList->getSorted($query->getSortBy(), !$query->isSortAsc());
				}

				// Limit
				if (!empty($query->getLimit())) {
					$elements = array_slice($elements, 0, $query->getLimit());
				}

				/** @var Renderer $renderer */
				$renderer = $this->objectManager->make(Renderer::class);
				$renderer->assign('elements', $this->prepareItems($elements));
				$partial = $this->templateResolver->getPartial($this->getPartialFileName());
				return $renderer->render($partial);
			}
			return '';
		}, $input);
	}

	abstract protected function getTagName(): string;

	abstract protected function prepareFilter(SearchFilter $filter): void;

	/**
	 * @return mixed[]
	 */
	abstract protected function prepareItems(array $items): array;

	abstract protected function getPartialFileName(): string;
}