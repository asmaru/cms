<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

use asmaru\cms\core\error\UploadSizeException;
use asmaru\cms\core\SettingsManager;
use asmaru\cms\core\TemporaryFileSystem;
use SplFileInfo;
use function base64_decode;

/**
 * Class Base64UploadService
 *
 * @package asmaru\cms\core\store
 */
class Base64UploadService {

	private readonly TemporaryFileSystem $temporaryFileSystem;

	private readonly SettingsManager $settingsManager;

	public function __construct(TemporaryFileSystem $temporaryFileSystem, SettingsManager $settingsManager) {
		$this->temporaryFileSystem = $temporaryFileSystem;
		$this->settingsManager = $settingsManager;
	}

	/**
	 * @param string $dataUri
	 *
	 * @return SplFileInfo
	 * @throws UploadSizeException
	 */
	public function convertDataUriToFile(string $dataUri): SplFileInfo {
		$base64String = mb_substr($dataUri, mb_strpos($dataUri, ',') + 1);
		$fileData = base64_decode($base64String);
		$tmpFile = $this->temporaryFileSystem->create();
		$this->temporaryFileSystem->write($tmpFile->getBasename(), $fileData);
		$maxUploadSize = $this->settingsManager->get(SettingsManager::MAX_UPLOAD_SIZE);
		if ($tmpFile->getSize() > $maxUploadSize) {
			$this->temporaryFileSystem->delete($tmpFile->getRealPath());
			throw new UploadSizeException($tmpFile->getSize(), $maxUploadSize);
		}
		return $tmpFile;
	}
}