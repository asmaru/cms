<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\api\model\StoreElementWriteRequest;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\cms\core\store\DataContainer;
use asmaru\cms\core\store\Folder;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\http\Path;

class CreateUserCommand implements CommandInterface {

	public function __construct(
		private readonly StoreRootFolder $storeRootFolder
	) {
	}

	public function execute(Input $input, StyledOutput $output): CommandResult {

		$name = $input->getArgument('name');
		$password = $input->getArgument('password');

		$writeRequest = new StoreElementWriteRequest($name, [
			'type' => Page::type(),
			'data' => [
				'type' => 'user',
				'password' => $password,
				'created' => time()
			]
		]);

		/** @var Folder $userFolder */
		$userFolder = $this->storeRootFolder->get('users');
		$path = new Path(Page::toInternalName($writeRequest->getName()));
		$user = $userFolder->resolve($path);
		if ($user !== null) {
			$output->errorLine(sprintf('User "%s" already exists', $name));
			return CommandResult::error();
		}

		$user = $userFolder->create($path, $writeRequest->getType());

		if ($user instanceof DataContainer) {
			$user->save($writeRequest->getData());
		}
		$output->successLine(sprintf('User "%s" created successfully', $name));
		return CommandResult::success();
	}

	public function help(): string {
		return 'Creates a new user';
	}
}