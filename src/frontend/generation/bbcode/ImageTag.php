<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\bbcode;

use asmaru\bbcode\Rule;
use asmaru\cms\core\image\ImageService;
use asmaru\cms\core\meta\MetaDataService;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\frontend\generation\GenerationContext;
use asmaru\http\Path;
use Exception;
use function preg_replace_callback;
use function sprintf;
use const IMAGETYPE_WEBP;

class ImageTag implements Rule {

	public function __construct(
		private readonly ImageService $imageService,
		private readonly GenerationContext $generationContext,
		private readonly MetaDataService $metaDataService,
		private readonly StoreRootFolder $storeRootFolder
	) {
	}

	/**
	 * @param string $input
	 *
	 * @return string
	 * @throws Exception
	 */
	public function parse(string $input): string {
		$pattern = '/\[img(?:=(?P<tagAttribute>[^\]]*))?\](?P<tagContent>.*?)\[\/img\]/is';
		return preg_replace_callback($pattern, function ($matches): string {
			try {
				$description = !empty($matches['tagAttribute']) ? $matches['tagContent'] : null;
				$path = !empty($matches['tagAttribute']) ? $matches['tagAttribute'] : $matches['tagContent'];

				// Get the image by name
				$storeElement = $this->storeRootFolder->resolve(new Path($path));
				if ($storeElement === null) {
					throw new GenerationException(sprintf('Element "%s" not found!', $path));
				}
				$originalImage = $this->imageService->getImageByName($storeElement->getPath(), null);

				// Scale image
				if ($originalImage === null) {
					throw new GenerationException(sprintf('Image "%s" not found!', $path));
				}
				$resolution = $this->imageService->getResolutionByName('content');
				$scaledImage = $this->imageService->getImageByName($path, $resolution);
				$scaledImageWebP = $this->imageService->getImageByName($path, $resolution, IMAGETYPE_WEBP);

				// Get URLs
				$originalImageUrl = $this->generationContext->uri($this->imageService->getPublicPath($originalImage));
				$scaledImageUrl = $this->generationContext->uri($this->imageService->getPublicPath($scaledImage));
				$scaledImageUrlWebP = $this->generationContext->uri($this->imageService->getPublicPath($scaledImageWebP));

				// Get meta data
				$fileMetaData = $this->metaDataService->get($storeElement);
				$alt = $fileMetaData->getAlt();
				$title = $fileMetaData->getTitle();
				$description = !empty($description) ? $description : $fileMetaData->getDescription();

				$imgTag = '<picture>';
				$imgTag .= '<source srcset="' . $scaledImageUrlWebP . '" type="image/webp">';
				$imgTag .= '<img' . (!empty($title) ? ' title="' . htmlentities($title, ENT_HTML5, 'UTF-8') . '"' : '') . ' alt="' . htmlentities($alt, ENT_HTML5, 'UTF-8') . '" src="' . $scaledImageUrl . '"/>';
				$imgTag .= '</picture>';

				if (!empty($description)) {
					$imgTag = '<figure>' . $imgTag . '<figcaption>' . $description . '</figcaption></figure>';
				}

				return '<a class="lightbox" target="_blank" href="' . $originalImageUrl . '">' . $imgTag . '</a>';

			} catch (GenerationException $e) {
				$this->generationContext->logError($e->getMessage());
				return $e->getMessage();
			}
		}, $input);
	}
}