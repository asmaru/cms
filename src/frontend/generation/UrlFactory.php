<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation;

use asmaru\cms\core\store\StoreElement;
use asmaru\http\Request;

/**
 * Class UrlFactory
 *
 * @package asmaru\cms\frontend\generation
 */
class UrlFactory {

	private readonly Request $request;

	/**
	 * UrlFactory constructor.
	 *
	 * @param Request $request
	 */
	public function __construct(Request $request) {
		$this->request = $request;
	}

	/**
	 * @param StoreElement $element
	 * @param int $index
	 *
	 * @return string
	 */
	public function forStoreElement(StoreElement $element, int $index): string {
		$path = $element->getPublicPath();
		if ($index > 1) {
			$path .= '?page=' . $index;
		}
		return $this->forPath($path);
	}

	/**
	 * @param string $path
	 *
	 * @return string
	 */
	public function forPath(string $path): string {
		return $this->request->uri($path);
	}
}