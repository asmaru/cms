<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\cms\core\image\ImageService;
use asmaru\cms\core\TemporaryFileSystem;
use asmaru\cms\frontend\generation\GenerationContext;
use asmaru\io\IOException;
use asmaru\mustache\Context;
use asmaru\mustache\Helper;
use function imagecopyresampled;
use function imagecreate;
use function imagecreatefrompng;
use function imagefilter;
use const IMG_FILTER_GAUSSIAN_BLUR;

/**
 * Class URIHelper
 *
 * @package asmaru\cms\frontend\generation
 */
class ImgBlurHelper implements Helper {

	/**
	 * @var ImageService
	 */
	private readonly ImageService $imageManager;

	/**
	 * @var TemporaryFileSystem
	 */
	private readonly TemporaryFileSystem $cacheFileSystem;

	/**
	 * @var GenerationContext
	 */
	private readonly GenerationContext $generationContext;

	/**
	 * ImgBlurHelper constructor.
	 *
	 * @param ImageService $imageManager
	 * @param TemporaryFileSystem $cacheFileSystem
	 * @param GenerationContext $generationContext
	 */
	public function __construct(ImageService $imageManager, TemporaryFileSystem $cacheFileSystem, GenerationContext $generationContext) {
		$this->imageManager = $imageManager;
		$this->cacheFileSystem = $cacheFileSystem;
		$this->generationContext = $generationContext;
	}

	/**
	 * @throws IOException
	 */
	public function render($content, Context $context, array $params = []): string {
		$image = $this->imageManager->get($content);
		$im = imagecreatefrompng($image->getRealPath());
		$sourceWidth = imagesx($im);
		$sourceHeight = imagesy($im);
		$im2 = imagecreate(20, 10);
		imagecopyresampled($im2, $im, 0, 0, 0, 0, 20, 10, $sourceWidth, $sourceHeight);
		for ($i = 0; $i < 5; $i++) {
			imagefilter($im2, IMG_FILTER_GAUSSIAN_BLUR);
		}
		$image = $this->cacheFileSystem->create('blur.jpg');
		imagejpeg($im2, $image->getRealPath());
		return $this->generationContext->uri('temp/' . $image->getBasename());
	}

	public function renderBefore(): bool {
		return true;
	}
}