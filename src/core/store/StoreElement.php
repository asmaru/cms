<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

use JsonSerializable;
use ReflectionClass;
use SplFileInfo;
use function array_reduce;
use function implode;

/**
 * Class StoreElement
 *
 * @package asmaru\cms\core\store
 */
abstract class StoreElement extends SplFileInfo implements JsonSerializable {

	protected ?Folder $parent;

	/**
	 * StoreElement constructor.
	 *
	 * @param string $filename
	 * @param Folder|null $parent
	 */
	public function __construct(string $filename, ?Folder $parent) {
		parent::__construct($filename);
		$this->parent = $parent;
	}

	public function getChanged(): int {
		return $this->getMTime();
	}

	public function getRoot(): StoreRootFolder {
		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return $this->isRoot() ? $this : $this->getParent()->getRoot();
	}

	public function isRoot(): bool {
		return $this->parent === null;
	}

	public function getParent(): ?Folder {
		return $this->parent;
	}

	/**
	 * @return array{name: string, path: string, publicPath: string, type: string|null}
	 */
	public function jsonSerialize(): array {
		return [
			'name' => $this->getName(),
			'path' => $this->getPath(),
			'publicPath' => $this->getPublicPath(),
			'type' => $this->getType()
		];
	}

	public function getName(): string {
		return $this->getBasename();
	}

	public function getPath(): string {
		return '/' . implode('/', array_reduce($this->getRootLine()->toArray(), function (array $carry, StoreElement $item): array {
				$carry[] = $item->getName();
				return $carry;
			}, []));
	}

	public function getPublicPath(): string {
		return $this->getPath();
	}

	public function getType(): string|false {
		return (new ReflectionClass($this))->getShortName();
	}

	public function getRootLine(): StoreElementList {
		if ($this->isRoot()) return new StoreElementList();
		$rootLine = $this->getParent()->getRootLine();
		$rootLine->add($this);
		return $rootLine;
	}

	abstract public static function matchType(SplFileInfo $file): bool;

	public static function type(): string {
		return (new ReflectionClass(static::class))->getShortName();
	}
}