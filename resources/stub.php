<?php

declare(strict_types=1);

use asmaru\cms\backend\cli\Server;
use asmaru\cms\Bootstrap;
use asmaru\cms\core\error\ClassNotFoundException;
use asmaru\http\RequestFactory;

define('ASMARU_REQUEST_TIME', microtime(true));

// Disable phar write access
ini_set('phar.readonly', '1');

// Create classloader
spl_autoload_register(function (string $name) {
	$path = __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $name) . '.php';
	if (!is_file($path)) {
		/** @noinspection PhpUnhandledExceptionInspection */
		throw new ClassNotFoundException($name);
	}
	include $path;
});

$rootPath = defined('WORK_DIR') ? WORK_DIR : dirname(Phar::running(false));
$pharPath = __DIR__;

/** @noinspection PhpUnhandledExceptionInspection */
$bootstrap = new Bootstrap($rootPath, $pharPath);
switch (PHP_SAPI) {
	case 'cli-server':
		$server = new Server($rootPath);
		$server->deliverStaticFiles();
		$bootstrap->bootWeb((new RequestFactory())->build());
		break;
	case 'cli':
		/** @noinspection PhpUnhandledExceptionInspection */
		$bootstrap->bootCLI($argv);
		break;
	default:
		$bootstrap->bootWeb((new RequestFactory())->build());
}