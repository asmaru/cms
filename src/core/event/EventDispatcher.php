<?php

declare(strict_types=1);

namespace asmaru\cms\core\event;

use asmaru\di\ObjectManager;
use function get_class;

/**
 * Class EventDispatcher
 *
 * @package asmaru\cms\core\event
 */
class EventDispatcher {

	/**
	 * @var array
	 */
	private array $events = [];

	/**
	 * @var ObjectManager
	 */
	private readonly ObjectManager $objectManager;

	/**
	 * EventDispatcher constructor.
	 *
	 * @param ObjectManager $objectManager
	 */
	public function __construct(ObjectManager $objectManager) {
		$this->objectManager = $objectManager;
	}

	/**
	 * @param string $event
	 * @param string $handler
	 */
	public function on(string $event, string $handler): void {
		if (!isset($this->events[$event])) {
			$this->events[$event] = [];
		}
		$this->events[$event][] = $handler;
	}

	/**
	 * @param Event $event
	 */
	public function trigger(Event $event): void {
		if (isset($this->events[get_class($event)])) {
			foreach ($this->events[get_class($event)] as $handler) {
				$this->objectManager->make($handler)->execute($event);
			}
		}
	}
}