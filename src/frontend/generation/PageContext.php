<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation;
/**
 * Class PageContext
 *
 * Contains the data which should be available in the template.
 *
 * @package asmaru\cms\frontend\generation
 */
class PageContext {

	private array $settings = [];

	private ?string $canonical = null;

	private array $resource = [];

	private array $documents = [];

	private bool $first = true;

	private array $pager = [];

	/**
	 * @param string $canonical
	 */
	public function setCanonical(string $canonical): void {
		$this->canonical = $canonical;
	}

	/**
	 * @param array $documents
	 */
	public function setDocuments(array $documents): void {
		$this->documents = $documents;
	}

	/**
	 * @param bool $first
	 */
	public function setFirst(bool $first): void {
		$this->first = $first;
	}

	/**
	 * @param array $pager
	 */
	public function setPager(array $pager): void {
		$this->pager = $pager;
	}

	/**
	 * @param array $resource
	 */
	public function setResource(array $resource): void {
		$this->resource = $resource;
	}

	/**
	 * @param array $settings
	 */
	public function setSettings(array $settings): void {
		$this->settings = $settings;
	}

	/**
	 * @return array{settings: mixed[], canonical: string|null, resource: mixed[], documents: mixed[], first: bool,
	 *     pager: mixed[]}
	 */
	public function toArray(): array {
		return [
			'settings' => $this->settings,
			'canonical' => $this->canonical,
			'resource' => $this->resource,
			'documents' => $this->documents,
			'first' => $this->first,
			'pager' => $this->pager
		];
	}
}