<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api\model;

use asmaru\cms\core\store\StoreElement;
use asmaru\di\ObjectManager;
use asmaru\http\App;
use asmaru\http\Request;
use JsonSerializable;
use function count;
use function time;

class SearchResult implements JsonSerializable {

	/**
	 * @var array|StoreElementOutputData[]
	 */
	private array $elements = [];

	/**
	 * @var ObjectManager
	 */
	private readonly ObjectManager $objectManager;

	/**
	 * @var Request
	 */
	private readonly Request $request;

	/**
	 * @var App
	 */
	private readonly App $app;

	public function __construct(ObjectManager $objectManager, Request $request, App $app) {
		$this->objectManager = $objectManager;
		$this->request = $request;
		$this->app = $app;
	}

	public function addElement(StoreElement $element): void {
		/** @var StoreElementOutputData $outputData */
		$this->elements[] = $this->objectManager->make(StoreElementOutputData::class, ['element' => $element]);
	}

	/**
	 * @return array{_self: string, _parent: string, _selfRelative: string, _parentRelative: string, _preview: null,
	 *     name: string, type: string, data: never[], children: StoreElementOutputData[],
	 *     size: int, created: int}
	 */
	public function jsonSerialize(): array {
		return [
			'_self' => $this->request->uri($this->app->getRoute('doc')->path(['name' => '/']), ['query' => $this->request->get('query')]),
			'_parent' => '',
			'_selfRelative' => '',
			'_parentRelative' => '',
			'_preview' => null,
			'name' => 'SearchResult',
			'type' => 'SearchResult',
			'data' => [],
			'children' => $this->elements,
			'size' => count($this->elements),
			'created' => time()
		];
	}

}