<?php

declare(strict_types=1);

namespace asmaru\cms;

use asmaru\bbcode\BBCode;
use asmaru\bbcode\Rule;
use asmaru\bbcode\rule\BlockquoteTag;
use asmaru\bbcode\rule\BoldTag;
use asmaru\bbcode\rule\CenterTag;
use asmaru\bbcode\rule\Heading1Tag;
use asmaru\bbcode\rule\Heading2Tag;
use asmaru\bbcode\rule\Heading3Tag;
use asmaru\bbcode\rule\HrTag;
use asmaru\bbcode\rule\ItalicTag;
use asmaru\bbcode\rule\ListTag;
use asmaru\bbcode\rule\MailTag;
use asmaru\bbcode\rule\PhoneTag;
use asmaru\bbcode\rule\SpoilerTag;
use asmaru\bbcode\rule\SpotifyTag;
use asmaru\bbcode\rule\SubTag;
use asmaru\bbcode\rule\SupTag;
use asmaru\bbcode\rule\UnderlineTag;
use asmaru\bbcode\rule\UrlTag;
use asmaru\bbcode\rule\YoutubeTag;
use asmaru\cache\backend\FileBackend;
use asmaru\cache\backend\NullBackend;
use asmaru\cache\frontend\CacheFrontend;
use asmaru\cms\backend\service\BackupService;
use asmaru\cms\core\Context;
use asmaru\cms\core\event\EventDispatcher;
use asmaru\cms\core\image\ImageService;
use asmaru\cms\core\JSONDataSource;
use asmaru\cms\core\Logger;
use asmaru\cms\core\SettingsManager;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\TemporaryFileSystem;
use asmaru\cms\core\TypeDefinitionManager;
use asmaru\cms\core\TypeDefinitionManagerFactory;
use asmaru\cms\frontend\generation\bbcode\ConTag;
use asmaru\cms\frontend\generation\bbcode\GalleryShortCode;
use asmaru\cms\frontend\generation\bbcode\ImageTag;
use asmaru\cms\frontend\generation\bbcode\IncludeTag;
use asmaru\cms\frontend\generation\bbcode\ListShortCode;
use asmaru\cms\frontend\generation\bbcode\MediaTag;
use asmaru\cms\frontend\generation\bbcode\PagesShortCode;
use asmaru\cms\frontend\generation\bbcode\ProTag;
use asmaru\cms\frontend\generation\GenerationContext;
use asmaru\cms\frontend\generation\helper\DateDiffHelper;
use asmaru\cms\frontend\generation\helper\DateHelper;
use asmaru\cms\frontend\generation\helper\FormatHtmlHelper;
use asmaru\cms\frontend\generation\helper\ImageColorGradientHelper;
use asmaru\cms\frontend\generation\helper\ImgHelper;
use asmaru\cms\frontend\generation\helper\IncludeHelper;
use asmaru\cms\frontend\generation\helper\NavigationHelper;
use asmaru\cms\frontend\generation\helper\NotEmptyHelper;
use asmaru\cms\frontend\generation\helper\PageListHelper;
use asmaru\cms\frontend\generation\helper\TrimHelper;
use asmaru\cms\frontend\generation\helper\UriHelper;
use asmaru\cms\frontend\generation\TemplateResolver;
use asmaru\cms\frontend\generation\UrlFactory;
use asmaru\di\ObjectManager;
use asmaru\http\App;
use asmaru\http\Request;
use asmaru\io\FileSystem;
use asmaru\io\IOException;
use asmaru\mustache\Parser;
use asmaru\mustache\Renderer;

class ObjectManagerFactory {

	/**
	 * @throws IOException
	 */
	public static function build(Context $context): ObjectManager {
		$objectManager = new ObjectManager();

		$objectManager->service(UrlFactory::class, function () use ($objectManager): UrlFactory {
			/** @var Request $request */
			$request = $objectManager->make(Request::class);
			return new UrlFactory($request);
		});

		$objectManager->service(GenerationContext::class);

		$objectManager->service(Context::class, $context);
		$objectManager->service(BBCode::class, function () use ($objectManager): BBCode {
			$bbCode = new BBCode();
			$bbCode->addRule(new BlockquoteTag());
			$bbCode->addRule(new BoldTag());
			$bbCode->addRule(new HrTag());
			$bbCode->addRule(new UnderlineTag());
			$bbCode->addRule(new Heading1Tag());
			$bbCode->addRule(new Heading2Tag());
			$bbCode->addRule(new Heading3Tag());
			$bbCode->addRule(new ItalicTag());
			$bbCode->addRule(new ListTag());
			$bbCode->addRule(new SpoilerTag());
			$bbCode->addRule(new SpotifyTag());
			$bbCode->addRule(new SubTag());
			$bbCode->addRule(new SupTag());
			$bbCode->addRule(new UrlTag());
			$bbCode->addRule(new YoutubeTag());
			$bbCode->addRule($objectManager->make(ImageTag::class));
			$bbCode->addRule(new ProTag());
			$bbCode->addRule(new ConTag());
			/** @var GenerationContext $generationContext */
			$generationContext = $objectManager->make(GenerationContext::class);
			$bbCode->addRule(new IncludeTag($objectManager, $generationContext));
			$bbCode->addRule(new MailTag());
			$bbCode->addRule(new PhoneTag());
			$bbCode->addRule(new CenterTag());
			/** @var Rule $shortCode */
			$shortCode = $objectManager->make(ListShortCode::class);
			$bbCode->addRule($shortCode);

			/** @var Rule $shortCode */
			$shortCode = $objectManager->make(PagesShortCode::class);
			$bbCode->addRule($shortCode);

			/** @var Rule $shortCode */
			$shortCode = $objectManager->make(GalleryShortCode::class);
			$bbCode->addRule($shortCode);

			/** @var Rule $shortCode */
			$shortCode = $objectManager->make(MediaTag::class);
			$bbCode->addRule($shortCode);

			return $bbCode;
		});

		$objectManager->service(TypeDefinitionManager::class, function () use ($context): TypeDefinitionManager {
			$dataSource = new JSONDataSource(new FileSystem($context->getDataPath('types')));
			$factory = new TypeDefinitionManagerFactory($dataSource);
			return $factory->create();
		});

		$objectManager->register(ImageService::class, function () use ($context, $objectManager): ImageService {
			$base = $context->getResourcePath();
			$fileSystem = new FileSystem($base);
			/** @var SettingsManager $settingsManager */
			$settingsManager = $objectManager->make(SettingsManager::class);
			/** @var TemporaryFileSystem $temporaryFileSystem */
			$temporaryFileSystem = $objectManager->make(TemporaryFileSystem::class);
			/** @var StoreRootFolder $storeRoot */
			$storeRoot = $objectManager->make(StoreRootFolder::class);
			return new ImageService($fileSystem, $settingsManager, $temporaryFileSystem, $storeRoot);
		});

		$objectManager->register(TemporaryFileSystem::class, function () use ($context, $objectManager): TemporaryFileSystem {
			$base = $context->getPath('temp');
			return new TemporaryFileSystem($base);
		});

		$objectManager->service(TemplateResolver::class, function () use ($objectManager, $context): TemplateResolver {
			$base = $context->getDataPath('templates');
			$fileSystem = new FileSystem($base);
			/** @var TypeDefinitionManager $typeDefinitionManager */
			$typeDefinitionManager = $objectManager->make(TypeDefinitionManager::class);
			return new TemplateResolver($fileSystem, $typeDefinitionManager);
		});

		$objectManager->register(Renderer::class, function () use ($objectManager): Renderer {
			/** @var Parser $mustacheParser */
			$mustacheParser = $objectManager->make(Parser::class);
			$renderer = new Renderer($mustacheParser);

			/** @var UriHelper $helper */
			$helper = $objectManager->make(UriHelper::class);
			$renderer->addHelper($helper);
			/** @var FormatHtmlHelper $helper */
			$helper = $objectManager->make(FormatHtmlHelper::class);
			$renderer->addHelper($helper);
			/** @var DateHelper $helper */
			$helper = $objectManager->make(DateHelper::class);
			$renderer->addHelper($helper);
			/** @var NavigationHelper $helper */
			$helper = $objectManager->make(NavigationHelper::class);
			$renderer->addHelper($helper);
			/** @var DateDiffHelper $helper */
			$helper = $objectManager->make(DateDiffHelper::class);
			$renderer->addHelper($helper);
			/** @var NotEmptyHelper $helper */
			$helper = $objectManager->make(NotEmptyHelper::class);
			$renderer->addHelper($helper);
			/** @var IncludeHelper $helper */
			$helper = $objectManager->make(IncludeHelper::class);
			$renderer->addHelper($helper);
			/** @var ImgHelper $helper */
			$helper = $objectManager->make(ImgHelper::class);
			$renderer->addHelper($helper);
			/** @var ImageColorGradientHelper $helper */
			$helper = $objectManager->make(ImageColorGradientHelper::class);
			$renderer->addHelper($helper);
			/** @var TrimHelper $helper */
			$helper = $objectManager->make(TrimHelper::class);
			$renderer->addHelper($helper);
			/** @var PageListHelper $helper */
			$helper = $objectManager->make(PageListHelper::class);
			$renderer->addHelper($helper);

			/** @var TemplateResolver $templateResolver */
			$templateResolver = $objectManager->make(TemplateResolver::class);
			foreach ($templateResolver->getPartials() as $name => $template) {
				$renderer->addPartial($name, $template);
			}

			return $renderer;
		});

		$objectManager->service(StoreRootFolder::class, function () use ($objectManager, $context): StoreRootFolder {
			$fs = new FileSystem($context->getResourcePath());
			return new StoreRootFolder($fs->getSelf()->getRealPath(), null, $objectManager);
		});

		$objectManager->service(BackupService::class, function () use ($context): BackupService {
			return new BackupService(new FileSystem($context->getRootPath()));
		});

		$objectManager->service(CacheFrontend::class, function () use ($objectManager, $context): CacheFrontend {
			/** @var SettingsManager $settings */
			$settings = $objectManager->make(SettingsManager::class);
			if ($settings->isCacheEnabled()) {
				/** @var TemporaryFileSystem $fileSystem */
				$fileSystem = $objectManager->make(TemporaryFileSystem::class);
				return new CacheFrontend(new FileBackend($fileSystem), $settings->getCacheLifetime());
			}
			return new CacheFrontend(new NullBackend(), 0);
		});

		$objectManager->service(EventDispatcher::class);
		$objectManager->service(App::class);
		$objectManager->service(Logger::class, new Logger(new FileSystem($context->getPath('log'))));

		return $objectManager;
	}
}