<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api\model;

use JsonSerializable;
use const PHP_MAJOR_VERSION;
use const PHP_MINOR_VERSION;

class SystemStatusResponse implements JsonSerializable {

	private int $usedSpace = 0;

	private int $resourceCount = 0;

	/**
	 * @return array{usedSpace: int, resourceCount: int, version: mixed, buildDate: mixed}
	 */
	public function jsonSerialize(): array {
		/** @noinspection PhpUndefinedConstantInspection */
		return [
			'usedSpace' => $this->usedSpace,
			'resourceCount' => $this->resourceCount,
			'version' => ASMARU_VERSION,
			'buildDate' => ASMARU_BUILD_DATE,
			'phpVersion' => PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION
		];
	}

	public function setResourceCount(int $resourceCount): void {
		$this->resourceCount = $resourceCount;
	}

	public function setUsedSpace(int $usedSpace): void {
		$this->usedSpace = $usedSpace;
	}
}