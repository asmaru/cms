<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\event;

use asmaru\cms\core\event\Event;

/**
 * Class ViewPageEvent
 *
 * @package asmaru\cms\frontend\event
 */
class ViewPageEvent implements Event {

	private readonly bool $error;

	public function __construct(bool $error) {
		$this->error = $error;
	}

	public function isError(): bool {
		return $this->error;
	}
}