<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use asmaru\cms\core\utility\PasswordUtility;
use Closure;
use JsonSerializable;
use OpenApi\Attributes as OA;

#[OA\Schema]
class TypeDefinition implements JsonSerializable {

	#[OA\Property]
	private readonly string $name;

	#[OA\Property]
	private string $label = '';

	#[OA\Property(items: new OA\Items(type: TypeDefinitionField::class))]
	private array $fields = [];

	#[OA\Property]
	private bool $hidden = false;

	#[OA\Property]
	private bool $public = false;

	/**
	 * @var callable[]
	 */
	private array $conversionFilters = [];

	/**
	 * TypeDefinition constructor.
	 *
	 * @param string $name
	 */
	public function __construct(string $name) {
		$this->name = $name;
		$this->fields = ['type' => new TypeDefinitionField('type')];
	}

	/**
	 * @param string $field
	 * @param callable $filter
	 */
	public function addConversionFilter(string $field, callable $filter): static {
		$this->conversionFilters[$field] = $filter;
		return $this;
	}

	/**
	 * @param TypeDefinitionField $typeDefField
	 */
	public function addField(TypeDefinitionField $typeDefField): static {
		$this->fields[$typeDefField->getName()] = $typeDefField;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param array $resource
	 *
	 * @return array
	 */
	public function convert(array $resource): array {
		$result = [];
		foreach ($this->getFields() as $field) {
			if (isset($resource[$field->getName()])) {
				$value = null;
				switch ($field->getType()) {
					case TypeDefinitionField::TYPE_BOOLEAN:
						$value = filter_var($resource[$field->getName()], FILTER_VALIDATE_BOOLEAN);
						break;
					case TypeDefinitionField::TYPE_INTEGER:
						$value = filter_var($resource[$field->getName()], FILTER_VALIDATE_INT);
						break;
					case TypeDefinitionField::TYPE_FLOAT:
						$value = filter_var($resource[$field->getName()], FILTER_VALIDATE_FLOAT);
						break;
					case TypeDefinitionField::TYPE_PASSWORD:
						$value = PasswordUtility::hash($resource[$field->getName()]);
						break;
					case TypeDefinitionField::TYPE_ARRAY:
					default:
						$value = $resource[$field->getName()];
						break;
				}
				if (isset($this->conversionFilters[$field->getName()])) {
					/** @var Closure $filter */
					$filter = $this->conversionFilters[$field->getName()];
					$value = $filter->__invoke($value);
				}
				$result[$field->getName()] = $value;
			}
		}
		return $result;
	}

	/**
	 * @return TypeDefinitionField[]
	 */
	public function getFields(): array {
		return $this->fields;
	}

	/**
	 * @return string
	 */
	public function getLabel(): string {
		return $this->label;
	}

	/**
	 * @param string $label
	 */
	public function setLabel(string $label): static {
		$this->label = $label;
		return $this;
	}

	/**
	 * @return array{name: string, label: string, public: bool, fields: mixed[], hidden: bool}
	 */
	function jsonSerialize(): array {
		return [
			'name' => $this->name,
			'label' => $this->label,
			'public' => $this->public,
			'fields' => $this->fields,
			'hidden' => $this->hidden
		];
	}

	/**
	 * @param boolean $hidden
	 *
	 * @codeCoverageIgnore
	 */
	public function setHidden(bool $hidden): static {
		$this->hidden = $hidden;
		return $this;
	}

	/**
	 * @param mixed $public
	 *
	 * @codeCoverageIgnore
	 */
	public function setPublic(bool $public): static {
		$this->public = $public;
		return $this;
	}

	/**
	 * @param array $resource
	 *
	 * @return array
	 */
	public function validate(array $resource): array {
		$errors = [];
		foreach ($this->getFields() as $field) {
			if ($field->isRequired() === true && empty($resource[$field->getName()])) {
				$errors[$field->getName()] = 'required';
			}
		}
		return $errors;
	}
}