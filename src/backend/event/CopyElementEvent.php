<?php

declare(strict_types=1);

namespace asmaru\cms\backend\event;

use asmaru\cms\core\event\Event;
use asmaru\cms\core\store\StoreElement;

/**
 * Class CopyElementEvent
 *
 * @package asmaru\cms\backend\event
 */
class CopyElementEvent implements Event {

	/**
	 * @var StoreElement
	 */
	private readonly StoreElement $source;

	/**
	 * @var StoreElement
	 */
	private readonly StoreElement $copy;

	public function __construct(StoreElement $source, StoreElement $copy) {
		$this->source = $source;
		$this->copy = $copy;
	}

	/**
	 * @return StoreElement
	 */
	public function getCopy(): StoreElement {
		return $this->copy;
	}

	/**
	 * @return StoreElement
	 */
	public function getSource(): StoreElement {
		return $this->source;
	}
}