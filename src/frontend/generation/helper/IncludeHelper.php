<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\mustache\Context;
use asmaru\mustache\Helper;
use function file_get_contents;

/**
 * Class NotEmptyHelper
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class IncludeHelper implements Helper {

	/**
	 * @var \asmaru\cms\core\Context
	 */
	private readonly \asmaru\cms\core\Context $context;

	public function __construct(\asmaru\cms\core\Context $context) {
		$this->context = $context;
	}

	public function render($content, Context $context, array $params = []): string|bool {
		return file_get_contents($this->context->getPath($params['file']));
	}

	public function renderBefore(): bool {
		return false;
	}
}