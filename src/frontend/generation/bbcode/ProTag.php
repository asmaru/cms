<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\bbcode;

use asmaru\bbcode\Tag;

class ProTag extends Tag {

	public function __construct() {
		parent::__construct('pro', '<div class="pro">%2$s</div>', function (string $content): string {
			return str_replace(["\r", "\n"], '', $content);
		});
	}
}