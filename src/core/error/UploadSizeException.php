<?php

declare(strict_types=1);

namespace asmaru\cms\core\error;

use function sprintf;

/**
 * Class UploadSizeException
 *
 * @package asmaru\cms\core
 * @codeCoverageIgnore
 */
class UploadSizeException extends ValidationException {

	/** @noinspection PhpMissingParentConstructorInspection */
	public function __construct(int $size, int $max) {
		$this->message = sprintf('File exceeds maximum size of %s bytes (%s)', $max, $size);
	}
}