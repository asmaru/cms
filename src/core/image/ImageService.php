<?php

declare(strict_types=1);

namespace asmaru\cms\core\image;

use asmaru\cms\core\error\InvalidResolutionException;
use asmaru\cms\core\SettingsManager;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\TemporaryFileSystem;
use asmaru\gdlib\GDUtility;
use asmaru\gdlib\Image;
use asmaru\http\Path;
use asmaru\io\FileSystem;
use asmaru\io\IOException;
use Exception;
use SplFileInfo;
use function intval;
use function preg_replace;
use function sprintf;
use function strtolower;
use const IMAGETYPE_JPEG;
use const IMG_JPG;
use const IMG_WEBP;
use const PATHINFO_FILENAME;

class ImageService {

	public function __construct(
		private readonly FileSystem $fileSystem,
		private readonly SettingsManager $settingsManager,
		private readonly TemporaryFileSystem $cacheFileSystem,
		private readonly StoreRootFolder $storeRootFolder
	) {
	}

	/**
	 * @param string $name
	 * @param Resolution|null $resolution
	 *
	 * @return Image|null
	 * @throws IOException
	 */
	public function getImageByName(string $name, ?Resolution $resolution, int $format = IMG_JPG): ?Image {
		$imageFile = $this->get($name);
		if ($imageFile === null) {
			return null;
		}
		return $this->getImage($imageFile, $resolution, $format);
	}

	/**
	 * @param string $name
	 *
	 * @return SplFileInfo|null
	 * @throws IOException
	 */
	public function get(string $name): ?SplFileInfo {
		return $this->storeRootFolder->resolve(new Path($name));
	}

	/**
	 * @param SplFileInfo $imageFile
	 * @param Resolution|null $resolution
	 *
	 * @return Image|null
	 */
	public function getImage(SplFileInfo $imageFile, ?Resolution $resolution, int $format = IMAGETYPE_JPEG): ?Image {

		// Use the width from the resolution or the original width if no resolution was given
		$resolution = $resolution ?? $this->getSourceResolution();
		$originalImageInfo = GDUtility::getImageInfo($imageFile->getRealPath());
		if ($originalImageInfo === null) return null;
		$targetWidth = $resolution->getWidth() ?? $originalImageInfo->getWidth();

		// Check if this image already exists in the required resolution
		$resizedFileName = $this->getImageFileName($imageFile->getBasename(), $resolution, $format);
		if ($this->cacheFileSystem->isFile($resizedFileName)) {
			return new Image($this->cacheFileSystem->get($resizedFileName)->getRealPath());
		}

		// Create a new image in the target resolution
		$targetPath = $this->cacheFileSystem->create($resizedFileName)->getRealPath();
		$sourcePath = $imageFile->getRealPath();
		if ($format === IMG_WEBP) {
			return GDUtility::toWEBP($sourcePath, $targetPath, $targetWidth, null, 90);
		} else {
			return GDUtility::toJPG($sourcePath, $targetPath, $targetWidth, null, 90);
		}
	}

	/**
	 * @return Resolution
	 */
	private function getSourceResolution(): Resolution {
		return new Resolution('source');
	}

	/**
	 * @param string $name
	 * @param Resolution $resolution
	 *
	 * @return string
	 */
	private function getImageFileName(string $name, Resolution $resolution, int $format = IMAGETYPE_JPEG): string {
		$filename = preg_replace('/[^a-z0-9_-]/', '', strtolower(pathinfo($name, PATHINFO_FILENAME)));
		return GDUtility::getFilenameWithExtension($filename . '_' . $resolution->getName(), $format);
	}

	/**
	 * @return Resolution
	 */
	public function getPreviewResolution(): Resolution {
		return new Resolution('preview', 400);
	}

	/**
	 * @param SplFileInfo $image
	 *
	 * @return string
	 */
	public function getPublicPath(SplFileInfo $image): string {
		return 'temp/' . $image->getBasename();
	}

	/**
	 * @param string|null $name
	 *
	 * @return Resolution|null
	 * @throws InvalidResolutionException
	 * @throws Exception
	 */
	public function getResolutionByName(?string $name): ?Resolution {
		$resolutions = $this->getResolutions();
		if (!empty($name)) {
			$resolution = mb_strtolower($name);
			if (isset($resolutions[$resolution])) {
				return $resolutions[$resolution];
			}
			throw new InvalidResolutionException(sprintf('Resolution "%s" not defined!', $name));
		}
		return null;
	}

	/**
	 * @return array|Resolution[]
	 * @throws Exception
	 */
	public function getResolutions(): array {
		$resolutionConfig = $this->settingsManager->get(SettingsManager::PROPERTY_RESOLUTIONS);
		$resolutions = ['source' => $this->getSourceResolution()];
		foreach ($resolutionConfig as $name => $width) {
			$resolutions[$name] = new Resolution($name, intval($width));
		}
		return $resolutions;
	}
}