<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

/**
 * Class PageType
 *
 * @package asmaru\cms\core\store
 */
class PageType {

	/**
	 * @var string
	 */
	private readonly string $name;

	/**
	 * PageType constructor.
	 *
	 * @param string $name
	 */
	public function __construct(string $name) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
}