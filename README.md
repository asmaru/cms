# Readme

## Build

```
php build.php
```

## Create a Project

### Installation

```
composer require asmaru/cms
composer require asmaru/adminpanel
composer exec asmaru install
```

### Development Server

```
composer exec asmaru serve
```