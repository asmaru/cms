<?php

declare(strict_types=1);

namespace asmaru\cms\core\search;

use asmaru\cms\core\store\DataContainer;
use asmaru\cms\core\store\Folder;
use asmaru\cms\core\store\StoreElement;
use asmaru\cms\core\store\StoreElementList;
use asmaru\cms\core\utility\SortedList;
use asmaru\http\Path;
use asmaru\io\IOException;
use function array_filter;
use function array_map;
use function array_merge;
use function array_reduce;
use function array_unique;
use function array_values;
use function explode;
use function in_array;
use function is_array;
use function mb_strlen;
use function mb_strtolower;
use function preg_replace;
use function preg_split;
use function strval;
use function trim;

/**
 * Class SearchService
 *
 * @package asmaru\cms\core\service
 */
class SearchService {

	/**
	 * @param Folder $folder
	 * @param Query $query
	 *
	 * @return StoreElementList
	 * @throws IOException
	 */
	public function findByQuery(Folder $folder, Query $query): StoreElementList {
		if (!empty($query->getPath())) {
			$folderFromParam = $folder->resolve(new Path($query->getPath()));
			if ($folderFromParam !== null) $folder = $folderFromParam;
		}

		$filter = SearchFilter::fromQuery($query);

		$elements = $this->find($folder, $filter)->toArray();

		// Sorting
		if (!empty($query->getSortBy())) {
			$sortedList = new SortedList($elements);
			$elements = $sortedList->getSorted($query->getSortBy(), !$query->isSortAsc());
		}

		// Limit
		if (!empty($query->getLimit())) {
			$elements = array_slice($elements, 0, $query->getLimit());
		}

		return new StoreElementList($elements);
	}

	/**
	 * Returns a filtered list of all child elements of this folder.
	 *
	 * @param Folder $folder
	 * @param SearchFilter $filter
	 *
	 * @return StoreElementList
	 */
	public function find(Folder $folder, SearchFilter $filter): StoreElementList {

		// Get all elements to be filtered
		$elements = $folder->getChildren(true);

		// Filter by type
		if (!empty($filter->getType())) {
			$allowedTypes = $this->expandFieldValues($filter->getType());
			$elements = array_filter($elements, function (StoreElement $element) use ($allowedTypes): bool {
				return in_array($element->getType(), $allowedTypes);
			});
		}

		// Filter by field value
		if (!empty($filter->getFields())) {
			$elements = array_filter($elements, function (StoreElement $element) use ($filter): bool {
				if ($element instanceof DataContainer) {
					foreach ($filter->getFields() as $name => $values) {
						$match = false;
						foreach ($this->expandFieldValues($values) as $value) {
							if ($element->getDataValue($name) == $value) {
								$match = true;
								break;
							}
						}
						if (!$match) return false;
					}
					return true; // All fields match
				}
				return false; // Element has no fields
			});
		}

		// Filter by term
		if (!empty($filter->getTerm())) {
			$expandedTerms = $this->expandTerms($filter->getTerm());

			if ($filter->includeContent) {

				// Fulltext search
				$elements = array_filter($elements, function (StoreElement $element) use ($expandedTerms): bool {
					$keywords = $this->createKeywords($element);
					foreach ($expandedTerms as $term) {
						$match = false;
						foreach ($keywords as $keyword) {
							if (mb_stripos($keyword, $term) !== false) {
								$match = true;
								break;
							}
						}
						if (!$match) return false;
					}
					return true;
				});

			} else {

				// Filter by path
				$elements = array_filter($elements, function (StoreElement $element) use ($expandedTerms): bool {
					$path = $this->normalizeKeyword($element->getPath());
					foreach ($expandedTerms as $term) {
						if (mb_stripos($this->normalizeKeyword($path), $term) === false) {
							return false;
						}
					}
					return true;
				});
			}

		}

		return new StoreElementList($elements);
	}

	/**
	 * @return string[]
	 */
	private function expandFieldValues(string $term): array {
		return explode('|', $term);
	}

	/**
	 * @return mixed[]
	 */
	private function expandTerms(string $term): array {
		return array_filter(array_map(function (string $term): string {
			return $this->normalizeKeyword($term);
		}, preg_split('/[ |]+/', $term)));
	}

	/**
	 * @return mixed[]
	 */
	public function createKeywords(StoreElement $element): array {
		$values = $this->getFlattenArrayValues($element->jsonSerialize());
		$values = array_reduce($values, function (array $carry, $value): array {
			return array_merge($carry, explode(' ', $this->normalizeKeyword(strval($value))));
		}, []);
		$values = array_filter($values);
		$values = array_unique($values);
		$values = array_filter($values, function ($v): bool {
			return mb_strlen($v) > 2;
		});
		return array_values($values);
	}

	private function normalizeKeyword(string $value): string {
		return preg_replace('/[^a-z0-9 ]/', '', mb_strtolower(trim($value)));
	}

	/**
	 * @return mixed[]
	 */
	private function getFlattenArrayValues(array $array): array {
		return array_reduce($array, function (array $carry, $value): array {
			return array_merge($carry, is_array($value) ? $this->getFlattenArrayValues($value) : [$value]);
		}, []);
	}
}