<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\mustache\Context;
use asmaru\mustache\Helper;
use function time;

/**
 * Class DateDiffHelper
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class DateDiffHelper implements Helper {

	public function render($content, Context $context, array $params = []): string {
		$time = (int)$content;
		$diff = time() - $time;

		if (isset($params['reverse'])) {
			$diff = $time - time();
		}

		if ($diff < 60) {
			return 'Jetzt';
		}

		if ($diff < 3600) {
			$value = round($diff / 60);
			return sprintf($value > 1 ? '%s Minuten' : '%s Minute', $value);
		}

		// day 86400
		if ($diff < 86400) {
			$value = round($diff / 3600);
			return sprintf($value > 1 ? '%s Stunden' : '%s Stunde', $value);
		}

		// week 604800
		if ($diff < 604800) {
			$value = round($diff / 86400);
			return sprintf($value > 1 ? '%s Tagen' : '%s Tag', $value);
		}

		// month 2592000
		if ($diff < 2592000) {
			$value = round($diff / 604800);
			return sprintf($value > 1 ? '%s Wochen' : '%s Woche', $value);
		}

		// year 31104000
		if ($diff < 31104000) {
			$value = round($diff / 2592000);
			return sprintf($value > 1 ? '%s Monaten' : '%s Monat', $value);
		}

		$value = round($diff / 31104000);
		return sprintf($value > 1 ? '%s Jahren' : '%s Jahr', $value);
	}

	public function renderBefore(): bool {
		return true;
	}
}