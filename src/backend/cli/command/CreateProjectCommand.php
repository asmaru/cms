<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\cms\backend\service\InstallService;
use asmaru\cms\core\Context;
use asmaru\io\FileSystem;
use function var_dump;

class CreateProjectCommand implements CommandInterface {

	private readonly InstallService $installService;

	private Context $context;

	public function __construct(InstallService $installService, Context $context) {
		$this->installService = $installService;
		$this->context = $context;
	}

	public function execute(Input $input, StyledOutput $output): CommandResult {

		var_dump($this->context->getRootPath());

		die();

		$target = new FileSystem($input->getArgument('target'));
		$this->installService->install($target);
		$output->successLine('Installation complete');
		return CommandResult::success();
	}

	public function help(): string {
		return '';
	}
}