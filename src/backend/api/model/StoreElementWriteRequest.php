<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api\model;

use InvalidArgumentException;

/**
 * Class StoreElementWriteRequest
 *
 * @package asmaru\cms\backend\api\model
 */
class StoreElementWriteRequest {

	/**
	 * @var string
	 */
	private readonly string $name;

	/**
	 * @var string
	 */
	private readonly mixed $type;

	/**
	 * @var mixed
	 */
	private readonly mixed $data;

	/**
	 * StoreElementWriteRequest constructor.
	 *
	 * @param string $name
	 * @param array $data
	 */
	public function __construct(string $name, array $data) {
		$this->name = $name;

		if (empty($data['type'])) throw new InvalidArgumentException('Type is missing');
		$this->type = $data['type'];

		if (!isset($data['data'])) throw new InvalidArgumentException('Data is missing');
		$this->data = $data['data'];
	}

	/**
	 * @return mixed
	 */
	public function getData(): mixed {
		return $this->data;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getType(): string {
		return $this->type;
	}
}