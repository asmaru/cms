<?php

declare(strict_types=1);

namespace asmaru\cms\core\logging;

use DateTime;
use JsonSerializable;
use OpenApi\Attributes as OA;

#[OA\Schema]
class LogEntry implements JsonSerializable {

	#[OA\Property]
	private int $id = 0;

	#[OA\Property]
	private string $protocol = '';

	#[OA\Property]
	private string $path = '';

	#[OA\Property]
	private string $userAgent = '';

	#[OA\Property]
	private string $referer = '';

	#[OA\Property]
	private bool $dnt = false;

	#[OA\Property]
	private ?DateTime $time = null;

	#[OA\Property]
	private int $status = 0;

	#[OA\Property]
	private string $acceptLanguage = '';

	#[OA\Property]
	private string $accept = '';

	#[OA\Property]
	private string $acceptEncoding = '';

	#[OA\Property]
	private bool $error = false;

	public function jsonSerialize(): array {
		return [
			'id' => $this->id,
			'protocol' => $this->protocol,
			'path' => $this->path,
			'userAgent' => $this->userAgent,
			'referer' => $this->referer,
			'dnt' => $this->dnt,
			'time' => $this->time->format('d.m.Y - H:i:s'),
			'status' => $this->status,
			'acceptLanguage' => $this->acceptLanguage,
			'accept' => $this->accept,
			'acceptEncoding' => $this->acceptEncoding,
			'error' => $this->error
		];
	}

	public function setAccept(string $accept): void {
		$this->accept = $accept;
	}

	public function setAcceptEncoding(string $acceptEncoding): void {
		$this->acceptEncoding = $acceptEncoding;
	}

	public function setAcceptLanguage(string $acceptLanguage): void {
		$this->acceptLanguage = $acceptLanguage;
	}

	public function setDnt(bool $dnt): void {
		$this->dnt = $dnt;
	}

	public function setError(bool $error): void {
		$this->error = $error;
	}

	public function setId(int $id): void {
		$this->id = $id;
	}

	public function setPath(string $path): void {
		$this->path = $path;
	}

	public function setProtocol(string $protocol): void {
		$this->protocol = $protocol;
	}

	public function setReferer(string $referer): void {
		$this->referer = $referer;
	}

	public function setStatus(int $status): void {
		$this->status = $status;
	}

	public function setTime(DateTime $time): void {
		$this->time = $time;
	}

	public function setUserAgent(string $userAgent): void {
		$this->userAgent = $userAgent;
	}
}