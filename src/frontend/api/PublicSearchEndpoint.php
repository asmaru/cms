<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\api;

use asmaru\cache\frontend\CacheFrontend;
use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\search\SearchService;
use asmaru\cms\core\store\DefaultFields;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\frontend\api\model\SearchResultResponse;
use asmaru\http\ContentType;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\json\Json;
use OpenApi\Attributes as OA;

/**
 * Class PublicSearchEndpoint
 *
 * @package asmaru\cms\frontend\api
 */
class PublicSearchEndpoint extends AbstractResource {

	private readonly StoreRootFolder $storeRootFolder;

	private readonly SearchService $searchService;

	private readonly CacheFrontend $cacheFrontend;

	/**
	 * PublicSearchEndpoint constructor.
	 *
	 * @param Request $request
	 * @param StoreRootFolder $storeRootFolder
	 * @param SearchService $searchService
	 * @param CacheFrontend $cacheFrontend
	 *
	 * @noinspection PhpMissingParentConstructorInspection
	 */
	public function __construct(Request $request, StoreRootFolder $storeRootFolder, SearchService $searchService, CacheFrontend $cacheFrontend) {
		$this->request = $request;
		$this->storeRootFolder = $storeRootFolder;
		$this->searchService = $searchService;
		$this->cacheFrontend = $cacheFrontend;
	}

	#[OA\Get(
		path: '/search',
		operationId: 'getSearch',
		parameters: [
			new OA\Parameter(name: 'term', required: true)
		],
		responses: [
			new OA\Response(response: 200, description: '', content: new OA\JsonContent(type: SearchResultResponse::class))
		]
	)]
	public function getCollection(): Response {
		$outputData = $this->cacheFrontend->withCache($this->request->env('REQUEST_URI'), function (): string {
			$filter = new SearchFilter();
			$filter->setTerm($this->request->get('term'));
			$filter->setType(Page::type());
			$filter->setIncludeContent(true);
			$filter->addField(DefaultFields::DISABLED, '0');
			$filter->addField(DefaultFields::HIDDEN, '0');
			$result = $this->searchService->find($this->storeRootFolder, $filter);
			$outputData = new SearchResultResponse($result, 50);
			return Json::toString($outputData);
		});
		return (new Response())->setContent($outputData, ContentType::JSON, Response::HTTP_OK);
	}
}