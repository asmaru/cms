<?php

declare(strict_types=1);

namespace asmaru\cms\core\logging;

use asmaru\cms\core\Context;
use asmaru\cms\core\Logger;
use asmaru\http\Request;
use asmaru\io\FileSystem;
use asmaru\io\IOException;
use DateTime;
use PDO;
use PDOException;
use function array_map;
use function file_get_contents;
use function intval;
use function time;
use const DIRECTORY_SEPARATOR;

class AccessLogService {

	final const FILE_NAME = 'requestLog.sqlite';

	private readonly PDO $pdo;

	private readonly Context $context;

	private readonly RequestDataFactory $requestDataFactory;

	private readonly Request $request;

	private readonly Logger $logger;

	public function __construct(Context $context, RequestDataFactory $requestDataFactory, Request $request, Logger $logger) {
		$this->context = $context;
		$path = $this->context->getDataPath('tracking') . DIRECTORY_SEPARATOR . self::FILE_NAME;
		$this->pdo = new PDO('sqlite:' . $path);
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->requestDataFactory = $requestDataFactory;
		$this->request = $request;
		$this->logger = $logger;
	}

	/**
	 * @throws IOException
	 */
	public function found(): void {
		$this->save($this->request, RequestData::STATUS_FOUND);
	}

	/**
	 * @param Request $request
	 * @param int $status
	 *
	 * @throws IOException
	 */
	private function save(Request $request, int $status): void {
		$this->createIfNotExist();
		$sql = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'insert.sql');
		$stmt = $this->pdo->prepare($sql);
		$data = $this->requestDataFactory->buildFromRequest($request, $status);
		try {
			$stmt->execute([
				'protocol' => $data->getProtocol(),
				'path' => $data->getPath(),
				'userAgent' => $data->getUserAgent(),
				'referer' => $data->getReferer(),
				'dnt' => $data->isDnt(),
				'time' => $data->getTime(),
				'status' => $data->getStatus(),
				'acceptLanguage' => $data->getAcceptLanguage(),
				'accept' => $data->getAccept(),
				'acceptEncoding' => $data->getAcceptEncoding()
			]);
			$this->cleanUp();
		} catch (PDOException $e) {
			$this->logger->error('SQL error: ' . $e->getMessage());
			if ($e->getCode() === 'HY000') {
				$this->logger->info('Try to repair access log ...');
				$fs = new FileSystem($this->context->getDataPath('tracking'));
				$this->logger->info(sprintf('Delete file "%s" ...', $fs->path(self::FILE_NAME)));
				$fs->delete(self::FILE_NAME);
				$this->logger->info('Recreate database ...');
				$this->createIfNotExist();
			} else throw $e;
		}
	}

	private function createIfNotExist(): void {
		$sql = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'create.sql');
		$this->pdo->exec($sql);
	}

	/**
	 * Remove old entries
	 */
	private function cleanUp(): void {
		$ttl = 90 * 24 * 60 * 60;
		$statement = $this->pdo->prepare('DELETE FROM `request` WHERE `time` < :expireDate');
		$statement->bindValue('expireDate', time() - $ttl, PDO::PARAM_INT);
		$statement->execute();
	}

	/**
	 * @return LogEntry[]
	 */
	public function get(int $limit): array {
		$statement = $this->pdo->prepare('SELECT * FROM `request` ORDER BY `time` DESC LIMIT :limit');
		$statement->bindValue('limit', $limit, PDO::PARAM_INT);
		$statement->execute();
		return array_map(function (array $row): LogEntry {
			return $this->arrayToObject($row);
		}, $statement->fetchAll(PDO::FETCH_ASSOC));
	}

	/**
	 * @return LogEntry[]
	 */
	public function getErrors(int $limit): array {
		$statement = $this->pdo->prepare('SELECT * FROM `request` WHERE NOT `status` = 200 ORDER BY `time` DESC LIMIT :limit');
		$statement->bindValue('limit', $limit, PDO::PARAM_INT);
		$statement->execute();
		return array_map(function (array $row): LogEntry {
			return $this->arrayToObject($row);
		}, $statement->fetchAll(PDO::FETCH_ASSOC));
	}

	/**
	 * @throws IOException
	 */
	public function notFound(): void {
		$this->save($this->request, RequestData::STATUS_NOT_FOUND);
	}

	private function arrayToObject(array $row): LogEntry {
		$entry = new LogEntry();
		$entry->setId($row['id']);
		$entry->setProtocol($row['protocol']);
		$entry->setPath($row['path']);
		$entry->setUserAgent($row['userAgent']);
		$entry->setReferer($row['referer']);
		$entry->setDnt($row['dnt'] === 1);
		$entry->setTime((new DateTime())->setTimestamp(intval($row['time'])));
		$entry->setStatus($row['status']);
		$entry->setAcceptLanguage($row['acceptLanguage']);
		$entry->setAccept($row['accept']);
		$entry->setAcceptEncoding($row['acceptEncoding']);
		$entry->setError($row['status'] != 200);
		return $entry;
	}
}