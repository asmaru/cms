<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api;

use asmaru\cache\frontend\CacheFrontend;
use asmaru\cms\backend\api\model\SearchResult;
use asmaru\cms\backend\api\model\StoreElementOutputData;
use asmaru\cms\backend\api\model\StoreElementWriteRequest;
use asmaru\cms\backend\event\CopyElementEvent;
use asmaru\cms\backend\event\OnBeforeDeleteElementEvent;
use asmaru\cms\backend\event\OnUpdateResourceEvent;
use asmaru\cms\core\event\EventDispatcher;
use asmaru\cms\core\Logger;
use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\search\SearchService;
use asmaru\cms\core\SettingsManager;
use asmaru\cms\core\store\DataContainer;
use asmaru\cms\core\store\Folder;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\utility\SortedList;
use asmaru\di\ObjectManager;
use asmaru\http\ContentType;
use asmaru\http\Path;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\http\rest\Resource;
use asmaru\io\IOException;
use asmaru\json\Json;
use Exception;
use function array_slice;

/**
 * Class ResourceEndpoint
 *
 * @package asmaru\cms\backend\api
 */
class ResourceEndpoint extends AbstractResource implements Resource {

	private readonly StoreRootFolder $storeRoot;

	private readonly Logger $logger;

	private readonly ObjectManager $objectManager;

	private readonly SearchService $searchService;

	private readonly CacheFrontend $cacheFrontend;

	private readonly EventDispatcher $eventDispatcher;

	public function __construct(Request $request, StoreRootFolder $storeRoot, Logger $logger, ObjectManager $objectManager, SearchService $searchService, CacheFrontend $cacheFrontend, EventDispatcher $eventDispatcher) {
		parent::__construct($request);
		$this->storeRoot = $storeRoot;
		$this->logger = $logger;
		$this->objectManager = $objectManager;
		$this->searchService = $searchService;
		$this->cacheFrontend = $cacheFrontend;
		$this->eventDispatcher = $eventDispatcher;
	}

	/**
	 * @param string $name
	 *
	 * @return Response
	 * @throws IOException
	 */
	public function delete(string $name): Response {
		// Get the element to delete and abort if the element doesn't exist
		$element = $this->storeRoot->resolve(new Path($name));
		if ($element === null) return (new Response())->status(Response::HTTP_NOT_FOUND);

		// Do not allow to delete the root element
		if ($element->getParent() === null) return (new Response())->status(Response::HTTP_BAD_REQUEST);

		// Trigger event
		$this->eventDispatcher->trigger(new OnBeforeDeleteElementEvent($element));

		// Delete the element and all its children
		$element->getParent()->delete($element);

		return (new Response())->status(Response::HTTP_OK);
	}

	/**
	 * @return Response
	 */
	public function getCollection(): Response {
		$output = $this->cacheFrontend->withCache($this->request->env('REQUEST_URI'), function (): string {
			return Json::toString($this->request->contains('query') ? $this->getSearchResult() : $this->getResource());
		}, SettingsManager::API_CACHE_LIFETIME);
		return (new Response())->setContent($output, ContentType::JSON, Response::HTTP_OK);
	}

	/**
	 * @return SearchResult
	 */
	private function getSearchResult(): SearchResult {
		// Build filter
		$filter = SearchFilter::fromRequest($this->request);
		$result = $this->searchService->find($this->storeRoot, $filter)->toArray();

		// Sorting
		if ($this->request->contains('sortBy')) {
			$sortedList = new SortedList($result);
			$result = $sortedList->getSorted($this->request->get('sortBy'), $this->request->contains('sortAsc') === false);
		}

		// Limit
		if ($this->request->contains('limit')) {
			$result = array_slice($result, 0, (int)$this->request->get('limit'));
		}

		/** @var SearchResult $searchResult */
		$searchResult = $this->objectManager->make(SearchResult::class);
		foreach ($result as $item) {
			$searchResult->addElement($item);
		}
		return $searchResult;
	}

	/**
	 * @return StoreElementOutputData
	 */
	private function getResource(): StoreElementOutputData {
		/** @var StoreElementOutputData $outputData */
		$outputData = $this->objectManager->make(StoreElementOutputData::class, ['element' => $this->storeRoot]);
		$outputData->includeChildren($this->request->get('recursive') !== null);
		return $outputData;
	}

	/**
	 * @param string $name
	 *
	 * @return Response
	 * @throws IOException
	 */
	public function get(string $name): Response {

		// Get the element to delete and abort if the element doesn't exist
		$element = $this->storeRoot->resolve(new Path($name));
		if ($element === null) return (new Response())->status(Response::HTTP_NOT_FOUND);

		// Search
		if ($element instanceof Folder) {
			if ($this->request->contains('query')) {
				$filter = SearchFilter::fromRequest($this->request);
				$result = $this->searchService->find($element, $filter)->toArray();
				/** @var SearchResult $searchResult */
				$searchResult = $this->objectManager->make(SearchResult::class);
				foreach ($result as $item) {
					$searchResult->addElement($item);
				}
				return (new Response())->json($searchResult, Response::HTTP_OK);
			}
		}

		// Return the element as JSON
		/** @var StoreElementOutputData $outputData */
		$outputData = $this->objectManager->make(StoreElementOutputData::class, ['element' => $element]);
		$outputData->includeData()->includeChildren();
		return (new Response())->json($outputData, Response::HTTP_OK);
	}

	public function put(string $name, array $data): Response {
		try {

			// Handle copy operation
			$source = $this->request->get('source');
			if (!empty($source)) {
				$sourceElement = $this->storeRoot->resolve(new Path($source));
				$copy = $this->storeRoot->copy($sourceElement, new Path($name));

				$this->eventDispatcher->trigger(new CopyElementEvent($sourceElement, $copy));

				return (new Response())->status(Response::HTTP_CREATED);
			}

			// Create transfer model
			$model = new StoreElementWriteRequest($name, $data);

			// Check if the element already exists
			$path = new Path($model->getName());
			$element = $this->storeRoot->resolve($path);
			$new = false;

			if ($element === null) {
				// Element doesn't exist, create a new one
				$new = true;
				$element = $this->storeRoot->create($path, $model->getType());
			}

			// Update data
			if ($element instanceof DataContainer) {
				$element->save($model->getData());
			}

			// Return the element as JSON
			/** @var StoreElementOutputData $outputData */
			$outputData = $this->objectManager->make(StoreElementOutputData::class, ['element' => $element]);
			$outputData->includeData();

			$this->eventDispatcher->trigger(new OnUpdateResourceEvent($element));

			return (new Response())->json($outputData, $new ? Response::HTTP_CREATED : Response::HTTP_OK);
		} catch (Exception $e) {
			$this->logger->error($e->getMessage());
			return (new Response())->status(Response::HTTP_UNPROCESSABLE_ENTITY);
		}
	}
}