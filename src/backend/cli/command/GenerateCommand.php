<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\search\SearchService;
use asmaru\cms\core\store\DefaultFields;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\TemporaryFileSystem;
use asmaru\cms\core\utility\PaginatedList;
use asmaru\cms\core\utility\SortedList;
use asmaru\cms\frontend\generation\PageGenerator;
use asmaru\cms\frontend\generation\Pager;
use asmaru\http\Path;
use asmaru\http\Request;
use asmaru\io\IOException;
use Exception;
use function sprintf;

class GenerateCommand implements CommandInterface {

	public function __construct(
		private readonly StoreRootFolder $storeRootFolder,
		private readonly SearchService $searchService,
		private readonly PageGenerator $pageGenerator,
		private readonly TemporaryFileSystem $temporaryFileSystem,
		private readonly Request $request) {
	}

	/**
	 * @param Input $input
	 * @param StyledOutput $output
	 *
	 * @return CommandResult
	 * @throws IOException
	 */
	public function execute(Input $input, StyledOutput $output): CommandResult {

		// Set the host which will be used to generate the urls
		$this->request->baseURL = $input->getArgument('host');

		// Create build dir
		$dist = $this->temporaryFileSystem->createDir('dist');

		// Get all resources which should be generated
		$filter = (new SearchFilter())
			->withType(Page::type())
			->withField(DefaultFields::DISABLED, '0');
		$pages = $this->searchService->find($this->storeRootFolder, $filter);
		$processed = 0;

		/** @var Page $page */
		foreach ($pages->toArray() as $page) {
			$output->writeLine(sprintf('Render: %s', $page->getPath()));

			try {
				$totalPageCount = 1;

				$datasourceConfig = $page->getDatasourceConfig();
				if ($datasourceConfig !== null) {

					// The page has a datasource

					$urlComponents = parse_url($datasourceConfig->getFilter());
					$path = $urlComponents['path'] ?? '';
					$query = $urlComponents['query'] ?? '';

					parse_str($query, $params);

					$folder = $this->storeRootFolder;
					if (!empty($path)) {
						$folderFromParam = $this->storeRootFolder->resolve(new Path($path));
						if ($folderFromParam !== null) $folder = $folderFromParam;
					}

					// Filter
					$filter = (new SearchFilter())->withType(Page::type());
					foreach ($params as $name => $value) {
						$filter = $filter->withField($name, $value);
					}
					$result = $this->searchService->find($folder, $filter)->toArray();

					// Sorting
					if (!empty($datasourceConfig->getSortBy())) {
						$sortedList = new SortedList($result);
						$result = $sortedList->getSorted($datasourceConfig->getSortBy(), $datasourceConfig->isSortAsc() === false);
					}

					// Pager
					$itemsPerPage = 0;
					if ($datasourceConfig->getItemsPerPage() !== null) {
						$itemsPerPage = max($datasourceConfig->getItemsPerPage(), 0);
					}
					$paginatedList = new PaginatedList($result, $itemsPerPage);

					$pager = new Pager(1, $paginatedList->size(), $itemsPerPage);
					$totalPageCount = $pager->getTotalPages();
				}

				for ($currentPage = 1; $currentPage <= $totalPageCount; $currentPage++) {
					$html = $this->pageGenerator->render($page, $currentPage);
					$filename = $page->getPublicPath() . ($currentPage > 1 ? $currentPage : '');
					$dist->write($filename, $html);
				}
			} catch (Exception $e) {
				$output->errorLine($e->getMessage());
			}
			$processed++;
		}

		$output->successLine(sprintf('%s pages generated', $processed));

		return CommandResult::success();
	}

	public function help(): string {
		return '';
	}
}