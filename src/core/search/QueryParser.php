<?php

declare(strict_types=1);

namespace asmaru\cms\core\search;

class QueryParser {

	public static function fromString(string $url): Query {
		$urlComponents = parse_url($url);
		$query = $urlComponents['query'] ?? '';
		parse_str(($query), $params);
		$params['path'] = $urlComponents['path'] ?? '';
		return static::fromArray($params);
	}

	/**
	 * @param array $params
	 *
	 * @return Query
	 */
	public static function fromArray(array $params): Query {
		$query = new Query();

		$path = $params['path'] ?? '';

		$query->setPath($path);
		unset($params['path']);

		if (isset($params['term'])) {
			$query->setTerm($params['term']);
			unset($params['term']);
		}

		if (isset($params['type'])) {
			$query->setType($params['type']);
			unset($params['type']);
		}

		if (isset($params['includeContent'])) {
			$query->setIncludeContent(true);
			unset($params['includeContent']);
		}

		if (isset($params['sortBy'])) {
			$query->setSortBy($params['sortBy']);
			unset($params['sortBy']);
		}

		if (isset($params['sortAsc'])) {
			$query->setSortAsc((int)$params['sortAsc'] === 1);
			unset($params['sortAsc']);
		}

		if (isset($params['limit'])) {
			$query->setLimit(intval($params['limit']));
			unset($params['limit']);
		}

		$query->setFields($params);

		return $query;
	}

}