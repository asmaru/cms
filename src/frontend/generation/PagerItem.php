<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation;

use JsonSerializable;

/**
 * Class PagerItem
 *
 * @package asmaru\cms\frontend\generation
 */
class PagerItem implements JsonSerializable {

	private readonly int $number;

	private readonly bool $active;

	private readonly string $link;

	/**
	 * PagerItem constructor.
	 *
	 * @param int $number
	 * @param bool $active
	 * @param string $link
	 */
	public function __construct(int $number, bool $active, string $link) {
		$this->number = $number;
		$this->active = $active;
		$this->link = $link;
	}

	/**
	 * @return array{number: int, active: bool, link: string}
	 */
	public function jsonSerialize(): array {
		return [
			'number' => $this->number,
			'active' => $this->active,
			'link' => $this->link
		];
	}
}