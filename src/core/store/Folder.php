<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

use asmaru\cms\core\error\ValidationException;
use asmaru\di\ObjectManager;
use asmaru\http\Path;
use asmaru\io\FileSystem;
use asmaru\io\IOException;
use SplFileInfo;
use function array_merge;
use function array_pop;
use function array_shift;
use function count;
use function implode;
use function sprintf;

/**
 * Class Folder
 *
 * @package asmaru\cms\core\store
 */
class Folder extends StoreElement {

	/**
	 * @var FileSystem
	 */
	protected FileSystem $fileSystem;

	/**
	 * @var ObjectManager
	 */
	protected ObjectManager $objectManager;

	/**
	 * Folder constructor.
	 *
	 * @param string $filename
	 * @param Folder|null $parent
	 * @param ObjectManager $objectManager
	 */
	public function __construct(string $filename, ?Folder $parent, ObjectManager $objectManager) {
		parent::__construct($filename, $parent);
		$this->fileSystem = FileSystem::fromFile($this);
		$this->objectManager = $objectManager;
	}

	/**
	 * @param StoreElement $source
	 * @param Path $path
	 *
	 * @return StoreElement
	 * @throws IOException
	 * @throws ValidationException
	 */
	public function copy(StoreElement $source, Path $path): StoreElement {
		$storeElementCopy = $this->create($path, $source->getType());
		copy($source->getRealPath(), $storeElementCopy->getRealPath());
		return $this->resolve(new Path($storeElementCopy->getPath()));
	}

	/**
	 * @param Path $path
	 * @param string $type
	 *
	 * @return StoreElement
	 * @throws IOException
	 * @throws ValidationException
	 */
	public function create(Path $path, string $type): StoreElement {
		$typeClass = $this->getTypeByName($type);
		if ($path->count() === 1) {
			$file = null;
			switch ($typeClass) {
				case Folder::class:
					$file = $this->fileSystem->createDir($path->firstSegment())->getSelf();
					break;
				case Page::class:
				case Media::class:
				case Picture::class:
				case TextFile::class:
					$file = $this->fileSystem->create($path->lastSegment());
					break;
			}
			if ($file === null) throw new ValidationException(sprintf('Invalid type: %s', $type));
			return $this->createElementFromFile($file);
		}

		$file = $this->fileSystem->createDir($path->firstSegment())->getSelf();
		/** @var Folder $folder */
		$folder = $this->createElementFromFile($file);
		$parts = $path->toArray();
		array_shift($parts);
		return $folder->create(Path::fromArray($parts), $type);
	}

	/**
	 * @param Path $path
	 *
	 * @return StoreElement|null
	 * @throws IOException
	 */
	public function resolve(Path $path): ?StoreElement {
		$pathParts = $path->toArray();

		if (empty($pathParts)) return $this;

		if (count($pathParts) === 1) {
			// If only one part is left, we are in the right folder.
			$name = array_pop($pathParts);
			return $this->get($name);
		}

		// If there is more than one part left, extract the first path segment and descend to the next level.
		$nextFolderName = array_shift($pathParts);

		// Create a new path for the subdirectory.
		$newPath = new Path(implode('/', $pathParts));
		$file = $this->fileSystem->down($nextFolderName)->getSelf();
		/** @var Folder $subFolder */
		$subFolder = $this->createElementFromFile($file);
		return $subFolder->resolve($newPath);
	}

	private function getTypeByName(string $type): ?string {
		switch ($type) {
			case 'Folder':
				return Folder::class;
			case 'Page':
				return Page::class;
			case 'Picture':
				return Picture::class;
			case 'Media':
				return Media::class;
			case 'TextFile':
				return TextFile::class;
		}
		return null;
	}

	private function createElementFromFile(SplFileInfo $file): StoreElement {
		/** @var StoreElement $element */
		$element = $this->objectManager->make($this->getTypeByFile($file), [
			'filename' => $file->getRealPath(),
			'parent' => $this
		]);
		return $element;
	}

	public function get(string $name): ?StoreElement {
		$file = $this->fileSystem->get($name);
		if ($file === null) return null;
		return $this->createElementFromFile($file);
	}

	private function getTypeByFile(SplFileInfo $file): string {
		if ($file->isDir()) return Folder::class;
		if (Page::matchType($file)) {
			return Page::class;
		}
		if (Picture::matchType($file)) {
			return Picture::class;
		}
		if (TextFile::matchType($file)) {
			return TextFile::class;
		}
		return Media::class;
	}

	public static function matchType(SplFileInfo $file): bool {
		return true;
	}

	public function delete(StoreElement $element): void {
		$this->fileSystem->delete($element->getBasename());
	}

	/**
	 * @param bool $recursive
	 *
	 * @return array|StoreElement[]
	 */
	public function getChildren(bool $recursive = false): array {
		$elements = [];
		foreach ($this->fileSystem->all() as $file) {
			$child = $this->createElementFromFile($file);
			$elements[] = $child;
			if ($recursive && $child instanceof Folder) {
				$elements = array_merge($elements, $child->getChildren(true));
			}
		}
		return $elements;
	}

	/**
	 * @return FileSystem
	 */
	public function getFileSystem(): FileSystem {
		return $this->fileSystem;
	}
}