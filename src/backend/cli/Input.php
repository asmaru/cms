<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli;

use function in_array;

class Input {

	private array $arguments = [];

	private array $options = [];

	public function getArgument(string $name): ?string {
		return $this->arguments[$name] ?? null;
	}

	public function hasArgument(string $name): bool {
		return isset($this->arguments[$name]);
	}

	public function hasOption(string $name): bool {
		return in_array($name, $this->options);
	}

	public function withArgument(string $name, string $value): static {
		$new = clone $this;
		$new->arguments[$name] = $value;
		return $new;
	}

	public function withOption(string $name): static {
		$new = clone $this;
		$new->options[] = $name;
		return $new;
	}
}