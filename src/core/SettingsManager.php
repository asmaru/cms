<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreRootFolder;
use function intval;

/**
 * Class SettingsManager
 *
 * @package asmaru\cms\core
 */
class SettingsManager {

	final const PROPERTY_RESOURCE_NOT_FOUND = 'pageNotFound';

	final const PROPERTY_RESOURCE_DEFAULT = 'homePage';

	final const FILENAME = 'settings.json';

	final const PROPERTY_RESOLUTIONS = 'resolutions';

	final const MAX_UPLOAD_SIZE = 'maxUploadSize';

	final const API_CACHE_LIFETIME = 1800;

	/**
	 * @var array
	 */
	private readonly array $data;

	/**
	 * SettingsManager constructor.
	 *
	 * @param StoreRootFolder $storeRootFolder
	 *
	 * @throws error\ValidationException
	 */
	public function __construct(StoreRootFolder $storeRootFolder) {
		/** @var Page $settings */
		$settings = $storeRootFolder->get(self::FILENAME);
		$this->data = $settings !== null ? $settings->getData() : [];
	}

	/**
	 * @param string $key
	 *
	 * @return mixed|null
	 */
	public function get(string $key): mixed {
		return $this->data[$key] ?? null;
	}

	/**
	 * @return array
	 */
	public function getAll(): array {
		return $this->data;
	}

	public function getCacheLifetime(): int {
		return intval($this->get('cacheLifetime'));
	}

	/**
	 * @return bool
	 */
	public function isCacheEnabled(): bool {
		return $this->get('cache') === true;
	}

	/**
	 * @return bool
	 */
	public function isDebugMode(): bool {
		return $this->get('debugMode') === true;
	}
}