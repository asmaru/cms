<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use function boolval;

/**
 * Class TypeDefinitionManagerFactory
 *
 * @package asmaru\cms\core
 */
class TypeDefinitionManagerFactory {

	/**
	 * @var JSONDataSource
	 */
	private readonly JSONDataSource $dataSource;

	/**
	 * TypeDefinitionManagerFactory constructor.
	 *
	 * @param JSONDataSource $dataSource
	 */
	public function __construct(JSONDataSource $dataSource) {
		$this->dataSource = $dataSource;
	}

	/**
	 * @return TypeDefinitionManager
	 */
	public function create(): TypeDefinitionManager {
		$typeDefinitionManager = new TypeDefinitionManager();
		foreach ($this->dataSource->find() as $typeName => $type) {
			$typeName = basename($typeName, '.json');
			$typeDefinition = new TypeDefinition($typeName);
			$typeDefinition->setLabel($type['label']);
			$typeDefinition->setHidden($type['hidden']);
			$typeDefinition->setPublic($type['public']);

			foreach ($type['fields'] as $fieldName => $field) {
				$typeDefinitionField = new TypeDefinitionField($fieldName);
				$typeDefinitionField->setType($field['type']);
				$typeDefinitionField->setRequired($field['required'] ?? false);
				$typeDefinitionField->setEditor($field['editor'] ?? null);
				$typeDefinitionField->setLabel($field['label'] ?? '');
				$typeDefinitionField->setHidden($field['hidden'] ?? false);
				$typeDefinitionField->setIndex(!isset($field['index']) || boolval($field['index']));
				$typeDefinition->addField($typeDefinitionField);
			}
			$typeDefinitionManager->add($typeDefinition);
		}
		return $typeDefinitionManager;
	}
}