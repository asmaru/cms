<?php

declare(strict_types=1);

namespace asmaru\cms\core\meta;

use asmaru\json\JsonUnserializable;
use JsonSerializable;

/**
 * Class FileMetaData
 *
 * @package asmaru\cms\core\meta
 */
class FileMetaData implements JsonSerializable, JsonUnserializable {

	private string $alt = '';

	private string $title = '';

	private string $description = '';

	public function getAlt(): string {
		return $this->alt;
	}

	public function setAlt(string $alt): void {
		$this->alt = $alt;
	}

	public function getDescription(): string {
		return $this->description;
	}

	public function setDescription(string $description): void {
		$this->description = $description;
	}

	public function getTitle(): string {
		return $this->title;
	}

	public function setTitle(string $title): void {
		$this->title = $title;
	}

	/**
	 * @return array{alt: string, title: string, description: string}
	 */
	public function jsonSerialize(): array {
		return [
			'alt' => $this->alt,
			'title' => $this->title,
			'description' => $this->description
		];
	}

	public static function jsonUnserialize($data): FileMetaData {
		$meta = new static();
		$meta->setAlt($data['alt']);
		$meta->setTitle($data['title']);
		$meta->setDescription($data['description']);
		return $meta;
	}
}