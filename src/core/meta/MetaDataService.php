<?php

declare(strict_types=1);

namespace asmaru\cms\core\meta;

use asmaru\cms\core\Context;
use asmaru\cms\core\store\StoreElement;
use asmaru\io\FileSystem;
use asmaru\io\IOException;
use asmaru\json\Json;
use function array_map;
use function trim;

/**
 * Class MetaDataService
 *
 * @package asmaru\cms\core\meta
 */
class MetaDataService {

	final const FILE_NAME = 'meta.json';

	/**
	 * @var FileMetaData[]
	 */
	private array $meta;

	private readonly FileSystem $fs;

	/**
	 * MetaDataService constructor.
	 *
	 * @param Context $context
	 *
	 * @throws IOException
	 */
	public function __construct(Context $context) {
		$this->fs = new FileSystem($context->getDataPath('meta'));
		if (!$this->fs->isFile(self::FILE_NAME)) $this->fs->write(self::FILE_NAME, Json::toString([]));
		$this->meta = array_map(function (array $data): FileMetaData {
			return FileMetaData::jsonUnserialize($data);
		}, Json::fromString($this->fs->read(self::FILE_NAME)));
	}

	public function delete(StoreElement $storeElement): void {
		$key = trim($storeElement->getPath(), '/');
		unset($this->meta[$key]);
		$this->fs->write(self::FILE_NAME, Json::toString($this->meta));
	}

	public function get(StoreElement $storeElement): FileMetaData {
		$key = trim($storeElement->getPath(), '/');
		return $this->meta[$key] ?? new FileMetaData();
	}

	public function set(StoreElement $storeElement, FileMetaData $fileMetaData): void {
		$key = trim($storeElement->getPath(), '/');
		$this->meta[$key] = $fileMetaData->jsonSerialize();
		$this->fs->write(self::FILE_NAME, Json::toString($this->meta));
	}
}