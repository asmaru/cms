<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api;

use asmaru\cms\core\TypeDefinition;
use asmaru\cms\core\TypeDefinitionManager;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\http\rest\Resource;
use OpenApi\Attributes as OA;
use function print_r;
use function strcasecmp;

class TypeEndpoint extends AbstractResource implements Resource {

	private readonly TypeDefinitionManager $typeDefinitionManager;

	public function __construct(Request $request, TypeDefinitionManager $typeDefinitionManager) {
		parent::__construct($request);
		$this->typeDefinitionManager = $typeDefinitionManager;
	}

	#[OA\Get(
		path: '/api/types',
		operationId: 'getTypes',
		responses: [
			new OA\Response(
				response: 200,
				description: '',
				content: new OA\JsonContent(type: 'array', items: new OA\Items(type: TypeDefinition::class))
			)
		]
	)]
	public function getCollection(): Response {
		$types = $this->typeDefinitionManager->all();
		uasort($types, function (TypeDefinition $a, TypeDefinition $b): int {
			return strcasecmp($a->getName(), $b->getLabel());
		});
		return (new Response())->json($types);
	}

	public function put(string $name, array $data): Response {
		die(print_r($data));
	}
}