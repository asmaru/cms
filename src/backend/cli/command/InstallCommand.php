<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\cms\backend\service\InstallService;
use asmaru\io\IOException;

class InstallCommand implements CommandInterface {

	private readonly InstallService $installService;

	public function __construct(InstallService $installService) {
		$this->installService = $installService;
	}

	/**
	 * @throws IOException
	 */
	public function execute(Input $input, StyledOutput $output): CommandResult {
		$output->writeLine('Initialize project ...');
		$this->installService->install(true);
		$output->successLine('Installation complete');
		return CommandResult::success();
	}

	public function help(): string {
		return 'Create all necessary directories';
	}
}