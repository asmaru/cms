<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api;

use asmaru\cache\frontend\CacheFrontend;
use asmaru\cms\core\Context;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\http\rest\Resource;
use asmaru\io\FileUtility;
use OpenApi\Attributes as OA;

class ClearCacheEndpoint extends AbstractResource implements Resource {

	private readonly Context $context;

	private readonly CacheFrontend $cacheFrontend;

	public function __construct(Request $request, Context $context, CacheFrontend $cacheFrontend) {
		parent::__construct($request);
		$this->context = $context;
		$this->cacheFrontend = $cacheFrontend;
	}

	#[OA\Delete(
		path: '/api/cache/{name}',
		operationId: 'deleteCache',
		parameters: [new OA\Parameter(name: 'name', in: 'path', required: true)],
		responses: [
			new OA\Response(
				response: 200,
				description: ''
			)
		]
	)]
	public function delete(string $name): Response {
		$this->cacheFrontend->flush();
		(new FileUtility())->deleteRecursive($this->context->getRootPath() . DIRECTORY_SEPARATOR . 'temp', false);
		return new Response();
	}
}