<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli;

class CommandResult {

	private const SUCCESS = 0;

	private const ERROR = 1;

	public function __construct(private readonly int $status) {
	}

	public function __toString(): string {
		return (string)$this->status;
	}

	public function getStatus(): int {
		return $this->status;
	}

	public static function error(): static {
		return new static(self::ERROR);
	}

	public static function success(): static {
		return new static(self::SUCCESS);
	}
}