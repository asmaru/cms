<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\cms\core\SettingsManager;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreElement;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\frontend\generation\bbcode\GenerationException;
use asmaru\cms\frontend\generation\GenerationContext;
use asmaru\http\Path;
use asmaru\io\IOException;
use asmaru\mustache\Context;
use asmaru\mustache\Helper;
use function array_reduce;
use function sprintf;

/**
 * Class ListHelper
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class NavigationHelper implements Helper {

	/**
	 * @var SettingsManager
	 */
	private readonly SettingsManager $settingsManager;

	/**
	 * @var StoreRootFolder
	 */
	private readonly StoreRootFolder $storeRootFolder;

	/**
	 * @var GenerationContext
	 */
	private readonly GenerationContext $generationContext;

	/**
	 * NavigationHelper constructor.
	 *
	 * @param SettingsManager $settingsManager
	 * @param StoreRootFolder $storeRootFolder
	 * @param GenerationContext $generationContext
	 */
	public function __construct(SettingsManager $settingsManager, StoreRootFolder $storeRootFolder, GenerationContext $generationContext) {
		$this->settingsManager = $settingsManager;
		$this->storeRootFolder = $storeRootFolder;
		$this->generationContext = $generationContext;
	}

	/**
	 * @param $content
	 * @param Context $context
	 * @param array $params
	 *
	 * @return mixed
	 * @throws GenerationException *@throws IOException
	 * @throws IOException
	 */
	public function render($content, Context $context, array $params = []): mixed {
		$names = $this->settingsManager->get('navigation');
		$documents = [];
		foreach ($names as $name) {
			/** @var Page $storeElement */
			$storeElement = $this->storeRootFolder->resolve(new Path(Page::toInternalName($name)));
			if ($storeElement === null) {
				throw new GenerationException(sprintf('Navigation %s not found!', $name));
			}
			$documents[] = $storeElement;
		}
		return array_reduce($documents, function (array $carry, StoreElement $item): array {
			$carry[] = [
				'active' => $this->generationContext->getStoreElement()->getPath() === $item->getPath(),
				'item' => $item->jsonSerialize()
			];
			return $carry;
		}, []);
	}

	/**
	 * @return bool
	 */
	public function renderBefore(): bool {
		return false;
	}
}