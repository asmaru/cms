<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use PHPUnit\Framework\Attributes\CodeCoverageIgnore;

#[CodeCoverageIgnore]
class SystemMessage {

	public static function createMessagePage(string $message, string $detail): string {
		return <<<TAG
<!DOCTYPE html>
<html>
	<head lang="de">
		<meta charset="UTF-8">
		<title>An error has occurred</title>
		<style>
			html {
				height: 100%;
				font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif;
				background-color:#293241;
				color: #E0FBFC;
				position: relative;
				font-size: 10px;
			}
			body {
				margin: 0;
				height: 100%;
				position: relative;
			}
			#container {
				display:block;
				position:absolute;
				left:50px;
				top:50px;
				right:50px;
				margin:auto;
				max-width: 1280px;
			}
			h1 {
				line-height:2rem;
				font-size: 2rem;
			}
			h2 {
				line-height:1.6rem;
				font-size: 1.6rem;
			}
			.title {
				background-color: #3D5A80;
				color: #E0FBFC;
				padding: 1rem;
				font-weight: bold;
				font-size: 1rem;
			}
			.line {
				padding: 1rem;
				margin: 0;
			}
			.error {
				background-color: #EE6C4D;
				color: #293241;
			}
			.code {
				font-family: monospace;
				background-color: #E0FBFC;
				color: #293241;
				font-size: 1.2rem;
				margin-bottom: 2em;
			}
</style>
	</head>
	<body>
		<div id="container">
			<h1>An error has occurred</h1>
			<div class="message">$message</div>
			<h2>Stacktrace</h2>
			<div class="message">$detail</div>
		</div>
	</body>
</html>
TAG;
	}
}