<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api;

use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\TemporaryFileSystem;
use asmaru\cms\core\TypeDefinitionManager;
use asmaru\cms\frontend\generation\bbcode\GenerationException;
use asmaru\cms\frontend\generation\PageGenerator;
use asmaru\di\ObjectManager;
use asmaru\http\Path;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use Exception;
use OpenApi\Attributes as OA;
use function intval;
use function max;
use function sprintf;

class PreviewEndpoint extends AbstractResource {

	public function __construct(Request $request,
		private readonly PageGenerator $pageGenerator,
		private readonly StoreRootFolder $storeRootFolder,
		private readonly TypeDefinitionManager $typeDefinitionManager,
		private readonly TemporaryFileSystem $temporaryFileSystem,
		private readonly ObjectManager $objectManager
	) {
		parent::__construct($request);
	}

	#[OA\Put(
		path: '/preview/*',
		responses: [
			new OA\Response(response: 200, description: '', content: new OA\MediaType(mediaType: 'text/html'))
		]
	)]
	public function put(string $name, array $data): Response {

		$dir = $this->temporaryFileSystem->createDir('preview');
		$folder = new StoreRootFolder($dir->path(), null, $this->objectManager);
		$page = new Page($dir->create()->getRealPath(), $folder, $this->typeDefinitionManager);
		$page->save($data['data']);

		//	$page = $this->getPage(Page::toInternalName($name));
		if ($page === null) {
			throw new Exception(sprintf('Page %s was not found, has an invalid type or is disabled!', $name));
		}
		$content = $this->render($page);
		if (empty($content)) {
			throw new GenerationException('Generated content is empty!');
		}
		return (new Response())->setContent($content);
	}

	private function render(Page $page): string {
		$currentPage = max(intval($this->request->get('page', '1')), 1);
		return $this->pageGenerator->render($page, $currentPage);
	}

	private function getPage(string $name): ?Page {
		$path = new Path($name);
		/** @var Page $page */
		$page = $this->storeRootFolder->resolve($path);
		if ($page === null) {
			return null;
		}
		return $page;
	}
}