<?php

declare(strict_types=1);

namespace asmaru\cms\core\event;

/**
 * Interface EventCallback
 *
 * @package asmaru\cms\core\event
 */
interface EventCallback {

	/**
	 * @param Event $event
	 */
	public function execute(Event $event): void;
}