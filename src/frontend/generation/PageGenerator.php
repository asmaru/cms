<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation;

use asmaru\cms\core\error\ValidationException;
use asmaru\cms\core\Logger;
use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\search\SearchService;
use asmaru\cms\core\SettingsManager;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\PageType;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\utility\PaginatedList;
use asmaru\cms\core\utility\SortedList;
use asmaru\http\Path;
use asmaru\io\IOException;
use asmaru\mustache\Renderer;
use Exception;
use function max;
use function parse_str;
use function parse_url;

/**
 * Class PageGenerator
 *
 * @package asmaru\cms\frontend\generation
 */
class PageGenerator {

	private readonly TemplateResolver $templateResolver;

	private readonly SettingsManager $settingsManager;

	private readonly Renderer $renderer;

	private readonly SearchService $searchService;

	private readonly StoreRootFolder $storeRootFolder;

	private readonly GenerationContext $generationContext;

	private readonly Logger $logger;

	/**
	 * PageGenerator constructor.
	 *
	 * @param TemplateResolver $templateResolver
	 * @param SettingsManager $settingsManager
	 * @param Renderer $renderer
	 * @param SearchService $searchService
	 * @param StoreRootFolder $storeRootFolder
	 * @param GenerationContext $generationContext
	 * @param Logger $logger
	 */
	public function __construct(TemplateResolver $templateResolver, SettingsManager $settingsManager, Renderer $renderer, SearchService $searchService, StoreRootFolder $storeRootFolder, GenerationContext $generationContext, Logger $logger) {
		$this->templateResolver = $templateResolver;
		$this->settingsManager = $settingsManager;
		$this->renderer = $renderer;
		$this->searchService = $searchService;
		$this->storeRootFolder = $storeRootFolder;
		$this->generationContext = $generationContext;
		$this->logger = $logger;
	}

	/**
	 * @param Page $page
	 * @param int $currentPage
	 *
	 * @return string
	 * @throws IOException
	 * @throws ValidationException
	 * @throws Exception
	 */
	public function render(Page $page, int $currentPage): string {
		$this->generationContext->setStoreElement($page);
		$resource = $page->getData();

		// Get template
		$template = $this->templateResolver->getTemplate(new PageType($page->getDataValue('type')));

		$pageContext = new PageContext();
		$pageContext->setSettings($this->settingsManager->getAll());
		$pageContext->setCanonical($this->generationContext->getUrlFactory()->forStoreElement($page, 0));
		$pageContext->setResource($page->jsonSerialize());

		// Assign resource fields
		foreach ($resource as $key => $value) {
			$this->renderer->assign($key, $value);
		}

		// Add data for data source pages
		$datasourceConfig = $page->getDatasourceConfig();
		if ($datasourceConfig !== null) {

			$urlComponents = parse_url($datasourceConfig->getFilter());
			$path = $urlComponents['path'] ?? '';
			$query = $urlComponents['query'] ?? '';

			parse_str($query, $params);

			$folder = $this->storeRootFolder;
			if (!empty($path)) {
				$folderFromParam = $this->storeRootFolder->resolve(new Path($path));
				if ($folderFromParam !== null) $folder = $folderFromParam;
			}

			// Filter
			$filter = new SearchFilter();
			$filter->type = Page::type();
			foreach ($params as $name => $value) {
				$filter->addField($name, $value);
			}
			$result = $this->searchService->find($folder, $filter)->toArray();

			// Sorting
			if (!empty($datasourceConfig->getSortBy())) {
				$sortedList = new SortedList($result);
				$result = $sortedList->getSorted($datasourceConfig->getSortBy(), $datasourceConfig->isSortAsc() === false);
			}

			// Pager
			$itemsPerPage = 0;
			if ($datasourceConfig->getItemsPerPage() !== null) {
				$itemsPerPage = max($datasourceConfig->getItemsPerPage(), 0);
			}
			$paginatedList = new PaginatedList($result, $itemsPerPage);

			$currentPage = max($currentPage, 1);
			$pager = new Pager($currentPage, $paginatedList->size(), $itemsPerPage);

			$pageDataList = [];
			/** @var Page $item */
			foreach ($paginatedList->getPage($currentPage) as $item) {
				$pageDataList[] = $item->jsonSerialize();
			}

			$firstPage = $currentPage === 1;

			$pageContext->setDocuments($pageDataList);
			$pageContext->setFirst($firstPage);
			$pageContext->setPager($pager->getPages($page, $this->generationContext->getUrlFactory()));

			$pageContext->setCanonical($this->generationContext->getUrlFactory()->forStoreElement($page, $currentPage));
		}

		$this->renderer->assignAll($pageContext->toArray());

		$content = $this->renderer->render($template);

		$content = HtmlMinifier::minify($content);

		if ($this->generationContext->hasErrors()) {
			foreach ($this->generationContext->getErrors() as $error) {
				$this->logger->error($error);
			}
		}

		return $content;
	}
}