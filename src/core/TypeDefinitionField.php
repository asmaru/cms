<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use JsonSerializable;
use OpenApi\Attributes as OA;

#[OA\Schema]
class TypeDefinitionField implements JsonSerializable {

	final const TYPE_STRING = 'string';

	final const TYPE_BOOLEAN = 'boolean';

	final const TYPE_INTEGER = 'integer';

	final const TYPE_FLOAT = 'float';

	final const TYPE_ARRAY = 'array';

	final const TYPE_MAP = 'map';

	final const TYPE_PASSWORD = 'hash';

	private readonly string $name;

	#[OA\Property]
	private string $type = self::TYPE_STRING;

	#[OA\Property]
	private bool $required = false;

	#[OA\Property]
	private string $label = '';

	#[OA\Property]
	private ?string $editor = '';

	#[OA\Property]
	private bool $hidden = false;

	#[OA\Property]
	private bool $index = true;

	/**
	 * TypeDefinitionField constructor.
	 *
	 * @param string $name
	 */
	public function __construct(string $name) {
		$this->name = $name;
	}

	public function getDefault(): bool|array|string|null {
		$value = null;
		switch ($this->getType()) {
			case TypeDefinitionField::TYPE_STRING:
				$value = '';
				break;
			case TypeDefinitionField::TYPE_BOOLEAN:
				$value = false;
				break;
			case TypeDefinitionField::TYPE_FLOAT:
			case TypeDefinitionField::TYPE_PASSWORD:
			case TypeDefinitionField::TYPE_INTEGER:
				$value = null;
				break;
			case TypeDefinitionField::TYPE_ARRAY:
			case TypeDefinitionField::TYPE_MAP:
				$value = [];
				break;
		}
		return $value;
	}

	/**
	 * @return string
	 * @codeCoverageIgnore
	 */
	public function getType(): string {
		return $this->type;
	}

	/**
	 * @param string $type
	 *
	 * @return TypeDefinitionField
	 * @codeCoverageIgnore
	 */
	public function setType(string $type): TypeDefinitionField {
		$this->type = $type;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @return bool
	 */
	public function isIndex(): bool {
		return $this->index;
	}

	/**
	 * @param bool $index
	 */
	public function setIndex(bool $index): void {
		$this->index = $index;
	}

	/**
	 * @return bool
	 * @codeCoverageIgnore
	 */
	public function isRequired(): bool {
		return $this->required;
	}

	/**
	 * @param bool $required
	 *
	 * @return TypeDefinitionField
	 * @codeCoverageIgnore
	 */
	public function setRequired(bool $required): TypeDefinitionField {
		$this->required = $required;
		return $this;
	}

	/**
	 * @return array{type: string, required: bool, editor: string|null, label: string, hidden: bool, index: bool}
	 */
	function jsonSerialize(): array {
		return [
			'type' => $this->type,
			'required' => $this->required,
			'editor' => $this->editor,
			'label' => $this->label,
			'hidden' => $this->hidden,
			'index' => $this->index
		];
	}

	/**
	 * @param string|null $editor
	 *
	 * @codeCoverageIgnore
	 */
	public function setEditor(?string $editor = null): void {
		$this->editor = $editor;
	}

	/**
	 * @param bool $hidden
	 *
	 * @codeCoverageIgnore
	 */
	public function setHidden(bool $hidden): void {
		$this->hidden = $hidden;
	}

	/**
	 * @param string $label
	 *
	 * @codeCoverageIgnore
	 */
	public function setLabel(string $label): void {
		$this->label = $label;
	}
}