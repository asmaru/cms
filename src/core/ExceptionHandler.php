<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use ReflectionClass;
use Throwable;
use function count;
use function explode;
use function file_get_contents;
use function get_class;
use function max;
use function min;
use function ob_clean;
use function sprintf;
use function str_pad;
use const STR_PAD_LEFT;

class ExceptionHandler {

	public function __construct(private readonly Logger $logger) {
	}

	public function handleError(int $errno, string $errstr, string $errfile, int $errline): never {
		ob_clean();
		$message = sprintf('<b>%s</b><br>%s:%s', $errstr, $errfile, $errline);
		$this->logger->error($message);
		echo SystemMessage::createMessagePage($message, '');
		die();
	}

	public function handleException(Throwable $e): void {
		ob_clean();
		$className = (new ReflectionClass($e))->getShortName();
		$message = '<h2>' . sprintf('%s: %s', $className, $e->getMessage()) . '</h2>';
		$message .= $this->getSource($e->getFile(), $e->getLine());
		$detail = '';
		foreach ($e->getTrace() as $value) {
			if (isset($value['file'])) {
				$detail .= $this->getSource($value['file'], $value['line']);
			}
		}
		echo SystemMessage::createMessagePage($message, $detail);
		$this->logger->error(sprintf('%s thrown in file %s:%s: %s', get_class($e), $e->getFile(), $e->getLine(), $e->getMessage()));
		die();
	}

	private function getSource(string $file, int $line): string {
		$html = '<div class="code">';
		$html .= '<div class="title">' . $file . ':' . $line . '</div>';
		$code = explode("\n", file_get_contents($file));
		if (count($code) > 0) {
			$offset = max($line - 3, 0);
			$end = min($offset + 4, count($code) - 1);
			for ($i = $offset; $i <= $end; $i++) {
				$html .= sprintf('<pre class="%s">%s: %s</pre>', $i === $line - 1 ? 'line error' : 'line', str_pad((string)($i + 1), 3, '0', STR_PAD_LEFT), $code[$i]);
			}
		}
		$html .= '</div>';
		return $html;
	}
}