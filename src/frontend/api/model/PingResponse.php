<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\api\model;

use JsonSerializable;
use OpenApi\Attributes as OA;

#[OA\Schema]
class PingResponse implements JsonSerializable {

	#[OA\Property(enum: ['ok'])]
	final const status = 'ok';

	public function jsonSerialize(): array {
		return [
			'status' => self::status
		];
	}
}