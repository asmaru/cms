<?php

declare(strict_types=1);

namespace asmaru\cms\core\error;

use Exception;

class InvalidResolutionException extends Exception {

}