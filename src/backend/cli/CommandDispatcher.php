<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli;

use asmaru\cli\StyledOutput;
use asmaru\di\ObjectManager;
use Exception;
use InvalidArgumentException;
use ReflectionClass;
use function array_shift;
use function microtime;
use function preg_match;
use function sprintf;
use function ucfirst;

class CommandDispatcher {

	public function __construct(
		private readonly ObjectManager $objectManager,
		private readonly StyledOutput $output) {
	}

	public function run(array $args): void {
		$startTime = microtime(true);

		// Ignore first argument
		array_shift($args);

		// Use last argument as command
		$command = !empty($args) ? array_shift($args) : 'help';

		// Parse argument options
		$input = new Input();
		if (!empty($args)) {
			foreach ($args as $optionValue) {
				// Parse short option
				if (preg_match('/\-(.+)/', (string)$optionValue, $matches)) {
					$input = $input->withOption($matches[1]);
					continue;
				}
				// Parse long option
				if (preg_match('/([^=]+)=(.+)/', (string)$optionValue, $matches)) {
					$input = $input->withArgument($matches[1], $matches[2]);
				}
			}
		}

		$commandName = array_reduce(explode('-', (string)$command), function ($carry, $item): string {
			$carry .= ucfirst($item);
			return $carry;
		}, '');
		$commandClassName = '\\asmaru\\cms\\backend\\cli\\command\\' . $commandName . 'Command';

		try {
			$rf = new ReflectionClass($commandClassName);

			if (!$rf->implementsInterface(CommandInterface::class)) {
				throw new InvalidArgumentException(sprintf('Command must implement interface %s', CommandInterface::class));
			}

			$this->output->newLine();
			/** @var CommandInterface $commandInstance */
			$commandInstance = $this->objectManager->make($commandClassName);
			$status = $commandInstance->execute($input, $this->output);

		} catch (Exception $e) {
			$this->output->error($e->getMessage());
			$status = CommandResult::error();
		}

		$this->output->newLine();
		$this->output->write('Memory usage: ');
		$this->output->info(sprintf('%s KB', round(memory_get_peak_usage() / 1024)));
		$this->output->write(' duration: ');
		$this->output->info(sprintf('%s seconds', round((microtime(true) - $startTime), 2)));
		$this->output->newLine();

		exit($status->getStatus());
	}
}