<?php

declare(strict_types=1);

namespace asmaru\cms\core\error;

use Exception;

/**
 * Class ValidationException
 *
 * @package asmaru\cms\core
 * @codeCoverageIgnore
 */
class ValidationException extends Exception {

}