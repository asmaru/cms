<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

use asmaru\cms\core\TypeDefinition;
use asmaru\cms\core\TypeDefinitionField;
use asmaru\cms\core\TypeDefinitionManager;
use asmaru\di\ObjectManager;
use asmaru\io\FileUtility;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use function time;
use function touch;
use const DIRECTORY_SEPARATOR;

class StoreElementTest extends TestCase {

	private string $dir = '';

	public function setUp(): void {
		parent::setUp();
		$this->dir = __DIR__ . DIRECTORY_SEPARATOR . time();
		mkdir($this->dir);
	}

	public function tearDown(): void {
		parent::tearDown();
		(new FileUtility())->deleteRecursive($this->dir);
	}

	#[Test]
	public function testPageElement() {
		$om = new ObjectManager();

		$typeDefinitionManager = new TypeDefinitionManager();
		$typeDefinitionManager->add((new TypeDefinition('testtype'))
			->addField((new TypeDefinitionField('value'))
				->setType(TypeDefinitionField::TYPE_STRING))
		);
		$om->service(TypeDefinitionManager::class, $typeDefinitionManager);

		$storeRootFolder = new StoreRootFolder($this->dir, null, $om);
		$om->service(StoreRootFolder::class, $storeRootFolder);

		$elName = 'test.json';
		touch($this->dir . DIRECTORY_SEPARATOR . $elName);

		$el = $storeRootFolder->get($elName);
		$el->save([
			DefaultFields::TYPE => 'testtype',
			DefaultFields::DISABLED => false,
			DefaultFields::HIDDEN => false,
			'value' => 123
		]);

		$this->assertInstanceOf(StoreElement::class, $el);
		$this->assertInstanceOf(Page::class, $el);
		$this->assertEquals(Page::type(), $el->getType());
		$this->assertEquals(123, $el->getDataValue('value'));
		$this->assertNull($el->getDatasourceConfig());
		$this->assertEquals($storeRootFolder, $el->getParent());
		$this->assertEquals('/test', $el->getPublicPath());
		$this->assertTrue($el->isVisible());

		$storeRootFolder->delete($el);
		$this->assertNull($storeRootFolder->get($elName));
	}
}