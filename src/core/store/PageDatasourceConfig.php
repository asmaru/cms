<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

class PageDatasourceConfig {

	private readonly string $filter;

	private readonly string $sortBy;

	private readonly bool $sortAsc;

	private readonly int $itemsPerPage;

	public function __construct(string $filter, string $sortBy, bool $sortAsc, int $itemsPerPage) {
		$this->filter = $filter;
		$this->sortBy = $sortBy;
		$this->sortAsc = $sortAsc;
		$this->itemsPerPage = $itemsPerPage;
	}

	/**
	 * @return string
	 */
	public function getFilter(): string {
		return $this->filter;
	}

	/**
	 * @return int
	 */
	public function getItemsPerPage(): int {
		return $this->itemsPerPage;
	}

	/**
	 * @return string
	 */
	public function getSortBy(): string {
		return $this->sortBy;
	}

	/**
	 * @return bool
	 */
	public function isSortAsc(): bool {
		return $this->sortAsc;
	}

}