<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\bbcode\BBCode;
use asmaru\mustache\Context;
use asmaru\mustache\Helper;
use function intval;
use function mb_strlen;

class TrimHelper implements Helper {

	/**
	 * @var BBCode
	 */
	private readonly BBCode $bbCode;

	public function __construct(BBCode $bbCodeParser) {
		$this->bbCode = $bbCodeParser;
	}

	public function render($content, Context $context, array $params = []) {
		$length = isset($params['length']) ? intval($params['length']) : 200;
		$content = strip_tags($this->bbCode->setEscapeHTML(false)->toHTML($content));
		if (mb_strlen($content) <= $length) return $content;
		return mb_substr($content, 0, mb_strpos(wordwrap($content, $length), "\n")) . ' ...';
	}

	/**
	 * @return boolean
	 */
	public function renderBefore(): bool {
		return true;
	}
}