<?php

declare(strict_types=1);

use asmaru\cli\StyledOutput;
use asmaru\cli\writer\StdOutWriter;
use asmaru\io\FileUtility;

include 'vendor/autoload.php';

gc_disable();
$start = microtime(true);
$out = new StyledOutput(new StdOutWriter());

array_map('call_user_func', [
	function () use ($out) {
		if (version_compare(PHP_VERSION, '8.1.0') > 0) {
			$out->successLine('✔ Compatible PHP version found.');
		} else {
			$out->errorLine(sprintf('Incompatible plattform! PHP >=8.1 required. Your version is %s', PHP_VERSION));
			exit();
		}
	},
	function () use ($out) {
		$out->writeLine('Clean build dir ...');
		$fileUtility = new FileUtility();
		$dir = __DIR__ . DIRECTORY_SEPARATOR . 'build';
		if (is_dir($dir)) {
			$fileUtility->deleteRecursive($dir);
		}
		mkdir($dir);
		$out->writeLine('Done.');
	},
	function () use ($out) {
		$out->writeLine('Compile PHAR ...');
		$major = 2;
		$minor = 3;
		$buildNumber = (int)file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'build.number');
		$buildNumber++;
		file_put_contents('build.number', $buildNumber);

		$pharPath = __DIR__ . '/build/asmaru.phar';
		$p = new Phar($pharPath, FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::KEY_AS_FILENAME, 'asmaru.phar');
		$p->startBuffering();
		$p->setMetadata(['version' => sprintf('%s.%s', $major, $buildNumber)]);

		// Constants
		$constants = [
			'ASMARU_VERSION' => sprintf('%s.%s.%s', $major, $minor, $buildNumber),
			'ASMARU_BUILD_DATE' => time()
		];
		$constantsString = '';
		foreach ($constants as $cname => $cvalue) {
			$constantsString .= sprintf('define(\'%s\',\'%s\');', $cname, $cvalue);
		}

		$p->setStub('<?php Phar::mapPhar(); ' . $constantsString . ' include "phar://asmaru.phar/stub.php"; __HALT_COMPILER(); ?>');

		// copy dependencies
		mkdir(__DIR__ . '/build/src/asmaru', 0777, true);
		$fileUtility = new FileUtility();
		$vendorBase = __DIR__ . '/vendor/asmaru/';
		$targetDir = __DIR__ . '/build/src/asmaru/';
		$packages = ['io', 'mustache', 'di', 'http', 'gdlib', 'bbcode', 'cli', 'json', 'sitemap', 'cache'];
		foreach ($packages as $package) {
			$out->writeLine(sprintf('Copy package %s -> %s', $vendorBase . $package . '/src', $targetDir . $package));
			$fileUtility->copyRecursive($vendorBase . $package . '/src', $targetDir . $package);
		}

		//mkdir(__DIR__ . '/build/src/asmaru/cms', 0777, true);
		$fileUtility->copyRecursive(__DIR__ . '/src', __DIR__ . '/build/src/asmaru/cms');

		/** @var RecursiveDirectoryIterator $iterator */
		$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__ . '/build/src', RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST);
		/** @var SplFileInfo $fileInfo */
		foreach ($iterator as $fileInfo) {
			if ($fileInfo->isFile()) {
				//$p->addFromString($iterator->getSubPathname(), php_strip_whitespace($fileInfo->getRealPath()));
				$p->addFromString($iterator->getSubPathname(), file_get_contents($fileInfo->getRealPath()));
			}
		}

		$p->addFromString('stub.php', file_get_contents(__DIR__ . '/resources/stub.php'));
		$p->addEmptyDir('resources');
		$p->addFile(__DIR__ . '/resources/.htaccess', 'resources/.htaccess');
		$p->addFile(__DIR__ . '/resources/robots.txt', 'resources/robots.txt');

		$p->addEmptyDir('resources/resources');
		$p->addEmptyDir('resources/resources/users');
		$p->addFile(__DIR__ . '/resources/resources/404.json', 'resources/resources/404.json');
		$p->addFile(__DIR__ . '/resources/resources/index.json', 'resources/resources/index.json');
		$p->addFile(__DIR__ . '/resources/resources/settings.json', 'resources/resources/settings.json');
		$p->addFile(__DIR__ . '/resources/resources/users/admin.json', 'resources/resources/users/admin.json');

		$p->addEmptyDir('resources/templates');
		$p->addEmptyDir('resources/templates/partials');
		$p->addFile(__DIR__ . '/resources/templates/page.html', 'resources/templates/page.html');
		$p->addFile(__DIR__ . '/resources/templates/partials/head.html', 'resources/templates/partials/head.html');

		$p->addEmptyDir('resources/types');
		$p->addFile(__DIR__ . '/resources/types/page.json', 'resources/types/page.json');
		$p->addFile(__DIR__ . '/resources/types/settings.json', 'resources/types/settings.json');
		$p->addFile(__DIR__ . '/resources/types/user.json', 'resources/types/user.json');

		$p->addFromString('resources/index.php', php_strip_whitespace(__DIR__ . '/resources/index.php'));
		$p->stopBuffering();
		unset($p);
		$size = round(filesize(__DIR__ . '/build/asmaru.phar') / 1024);
		$out->successLine(sprintf('✔ PHAR (%s KB) was written to %s', $size, $pharPath));
	},
	function () use ($out) {
		if (file_exists('build_targets.php')) {
			$dirs = include 'build_targets.php';
			$out->writeLine('Copy to target ...');
			foreach ($dirs as $dir) {
				copy(__DIR__ . '/build/asmaru.phar', $dir);
				$out->writeLine(sprintf('File copied to %s', $dir));
			}
			copy(__DIR__ . '/build/asmaru.phar', __DIR__ . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'asmaru.phar');
		}
	}
]);

$time = round(microtime(true) - $start, 2);
$memory = round(memory_get_peak_usage(true) / 1024);
$out->successLine(sprintf('✔ Build successful. Took %s seconds and required %s KB of memory.', $time, $memory));