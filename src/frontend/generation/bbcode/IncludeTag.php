<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\bbcode;

use asmaru\bbcode\BBCode;
use asmaru\bbcode\Rule;
use asmaru\cms\core\error\ValidationException;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\frontend\generation\GenerationContext;
use asmaru\di\ObjectManager;
use asmaru\http\Path;
use asmaru\io\IOException;
use function preg_replace_callback;
use function sprintf;

/**
 * Class IncludeTag
 *
 * @package asmaru\cms\frontend\generation\bbcode
 */
class IncludeTag implements Rule {

	/**
	 * @var ObjectManager
	 */
	private readonly ObjectManager $objectManager;

	/**
	 * @var GenerationContext
	 */
	private readonly GenerationContext $generationContext;

	/**
	 * IncludeTag constructor.
	 *
	 * @param ObjectManager $objectManager #+
	 * @param GenerationContext $generationContext
	 */
	public function __construct(ObjectManager $objectManager, GenerationContext $generationContext) {
		$this->objectManager = $objectManager;
		$this->generationContext = $generationContext;
	}

	/**
	 * @param string $input
	 *
	 * @return string
	 * @throws IOException
	 * @throws ValidationException
	 */
	public function parse(string $input): string {
		/** @var BBCode $bbCodeParser */
		$bbCodeParser = $this->objectManager->make(BBCode::class);
		/** @var StoreRootFolder $storeRootFolder */
		$storeRootFolder = $this->objectManager->make(StoreRootFolder::class);
		return preg_replace_callback('/\[include\]([^\[]*)\[\/include\]/is', function ($matches) use ($bbCodeParser, $storeRootFolder): string {
			try {
				$name = $matches[1];
				/** @var Page $resource */
				$resource = $storeRootFolder->resolve(new Path($name . '.json'));
				if ($resource === null) {
					throw new GenerationException(sprintf('Cannot include: Resource "%s" not found!', $name));
				}
				return '<div class="include">' . $bbCodeParser->setEscapeHTML(false)->toHTML((string)$resource->getData()['content']) . '</div>';
			} catch (GenerationException $e) {
				$this->generationContext->logError($e->getMessage());
				return $e->getMessage();
			}
		}, $input);
	}
}