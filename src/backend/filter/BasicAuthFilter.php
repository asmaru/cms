<?php

declare(strict_types=1);

namespace asmaru\cms\backend\filter;

use asmaru\cms\core\Logger;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\core\utility\PasswordUtility;
use asmaru\http\Path;
use asmaru\http\Request;
use asmaru\http\RequestHandler;
use asmaru\http\Response;
use asmaru\io\IOException;
use OpenApi\Attributes as OA;
use function sprintf;

#[OA\SecurityScheme(securityScheme: 'basicAuth', type: 'http', scheme: 'basic')]
class BasicAuthFilter implements RequestHandler {

	public function __construct(
		private readonly StoreRootFolder $storeRoot,
		private readonly Request $request,
		private readonly Logger $logger
	) {
	}

	/**
	 * @return Response|null
	 * @throws IOException
	 */
	public function execute(): ?Response {
		$name = $this->request->env('PHP_AUTH_USER');
		$password = $this->request->env('PHP_AUTH_PW');
		if (!empty($name) && !empty($password)) {
			$path = '/users/' . Page::toInternalName($name);
			/** @var Page $user */
			$user = $this->storeRoot->resolve(new Path($path));
			if (!empty($user)) {
				if (PasswordUtility::verify($password, (string)$user->getDataValue('password'))) {
					return null;
				}
			}
			$this->logger->error(sprintf('Login attempt from user %s, invalid username or password', $name));
			return (new Response())->status(Response::HTTP_FORBIDDEN);
		}
		return (new Response())->auth();
	}
}