<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

use asmaru\cms\core\error\ValidationException;
use asmaru\cms\core\TypeDefinitionField;
use asmaru\cms\core\TypeDefinitionManager;
use SplFileInfo;
use function array_merge;
use function is_array;
use function json_decode;
use function json_encode;
use function sprintf;
use function str_replace;
use const JSON_PRETTY_PRINT;

/**
 * Class Page
 *
 * @package asmaru\cms\core\store
 */
class Page extends StoreElement implements DataContainer {

	final const EXTENSION = 'json';

	private readonly TypeDefinitionManager $typeDefinitionManager;

	private ?array $data = null;

	/**
	 * Page constructor.
	 *
	 * @param string $filename
	 * @param Folder $parent
	 * @param TypeDefinitionManager $typeDefinitionManager
	 */
	public function __construct(string $filename, Folder $parent, TypeDefinitionManager $typeDefinitionManager) {
		parent::__construct($filename, $parent);
		$this->typeDefinitionManager = $typeDefinitionManager;
	}

	public function getDatasourceConfig(): ?PageDatasourceConfig {
		$filter = $this->getDataValue('filter');
		if (empty($filter)) {
			return null;
		}
		return new PageDatasourceConfig($filter, $this->getDataValue('sortBy'), $this->getDataValue('sortAsc'), $this->getDataValue('itemsPerPage'));
	}

	/**
	 * @param string $name
	 *
	 * @return mixed|null
	 */
	public function getDataValue(string $name): mixed {
		$this->load();
		return $this->data[$name] ?? null;
	}

	private function load(): void {
		if ($this->data === null) $this->data = json_decode($this->parent->getFileSystem()->read($this->getName()), true);
	}

	public function getPublicPath(): string {
		return str_replace('.' . self::EXTENSION, '', parent::getPublicPath());
	}

	/**
	 * @return bool
	 */
	public function isVisible(): bool {
		if ($this->getDataValue(DefaultFields::DISABLED) === null) {
			return true;
		}
		return $this->getDataValue(DefaultFields::DISABLED) === false;
	}

	/**
	 * @inheritDoc
	 * @throws ValidationException
	 */
	public function jsonSerialize(): array {
		return array_merge(parent::jsonSerialize(), [
			'data' => $this->getData()
		]);
	}

	/**
	 * @return array
	 * @throws ValidationException
	 */
	public function getData(): array {
		$this->load();

		$dataToOutput = $this->data;

		// Do not output password hashes
		$type = (string)$this->getDataValue(DefaultFields::TYPE);
		$typeDefinition = $this->typeDefinitionManager->getTypeDefinitionByType($type);
		if (empty($typeDefinition)) throw new ValidationException(sprintf('Cannot find definition for type "%s" in file "%s"!', $type, $this->getRealPath()));
		foreach ($typeDefinition->getFields() as $field) {
			if (isset($dataToOutput[$field->getName()]) && $field->getType() === TypeDefinitionField::TYPE_PASSWORD) {
				$dataToOutput[$field->getName()] = $field->getDefault();
			}
		}

		return $dataToOutput;
	}

	/**
	 * @param $data
	 *
	 * @throws ValidationException
	 */
	public function save($data = null): void {
		if ($data === null) $data = $this->data;

		if (!is_array($data)) throw new ValidationException('Data must be an array!');

		// Convert data
		$typeDefinition = $this->typeDefinitionManager->getTypeDefinitionByType($data[DefaultFields::TYPE]);
		if ($typeDefinition === null) throw new ValidationException('TypeDefinition not found');
		$convertedData = $typeDefinition->convert($data);
		$dataToSave = [];
		foreach ($typeDefinition->getFields() as $field) {
			$dataToSave[$field->getName()] = $convertedData[$field->getName()] ?? $field->getDefault();
		}

		// Validate values
		$errors = $typeDefinition->validate($dataToSave);
		//if (!empty($errors)) throw new ValidationException(print_r($errors, true));

		// Update file
		$this->parent->getFileSystem()->write($this->getName(), json_encode($dataToSave, JSON_PRETTY_PRINT));
		$this->data = $dataToSave;
	}

	/**
	 * @param string $name
	 * @param $value
	 */
	public function setDataValue(string $name, $value): void {
		$this->data[$name] = $value;
	}

	/**
	 * @param SplFileInfo $file
	 *
	 * @return bool
	 */
	public static function matchType(SplFileInfo $file): bool {
		return mb_strtolower($file->getExtension()) === self::EXTENSION;
	}

	/**
	 * @param string $name
	 *
	 * @return string
	 */
	public static function toInternalName(string $name): string {
		return $name . '.' . self::EXTENSION;
	}
}