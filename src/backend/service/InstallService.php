<?php

declare(strict_types=1);

namespace asmaru\cms\backend\service;

use asmaru\cms\core\Context;
use asmaru\io\FileSystem;
use asmaru\io\IOException;
use Phar;
use function is_dir;
use const DIRECTORY_SEPARATOR;

class InstallService {

	public function __construct(private readonly Context $context) {
	}

	/**
	 * Ensures that all necessary directories are existing.
	 *
	 * @throws IOException
	 */
	public function install(bool $first = false): void {

		if (!is_dir($this->context->getRootPath())) {
			mkdir($this->context->getRootPath());
		}

		$resourceDir = $this->context->getPharPath() . DIRECTORY_SEPARATOR . 'resources';

		$fileSystem = new FileSystem($this->context->getRootPath());
		$data = $fileSystem->createDir('data');

		$types = $data->createDir('types');
		if ($first) {
			$types->import($resourceDir . DIRECTORY_SEPARATOR . 'types' . DIRECTORY_SEPARATOR . 'page.json');
			$types->import($resourceDir . DIRECTORY_SEPARATOR . 'types' . DIRECTORY_SEPARATOR . 'settings.json');
			$types->import($resourceDir . DIRECTORY_SEPARATOR . 'types' . DIRECTORY_SEPARATOR . 'user.json');
		}

		$data->createDir('tracking');
		$data->createDir('meta');
		$templates = $data->createDir('templates');
		$partials = $templates->createDir('partials');
		if ($first) {
			$templates->import($resourceDir . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'page.html');
			$partials->import($resourceDir . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR . 'head.html');
		}

		$resources = $data->createDir('resources');
		if ($first) {
			$resources->import($resourceDir . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . '404.json');
			$resources->import($resourceDir . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'index.json');
			$resources->import($resourceDir . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'settings.json');
		}

		$users = $resources->createDir('users');
		if ($first) {
			$users->import($resourceDir . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'admin.json');
		}

		$fileSystem->createDir('temp');
		$fileSystem->createDir('backup');
		$fileSystem->createDir('log');

		if (!$fileSystem->isFile('.htaccess')) {
			$fileSystem->import($resourceDir . DIRECTORY_SEPARATOR . '.htaccess');
		}

		if (!$fileSystem->isFile('index.php')) {
			$fileSystem->import($resourceDir . DIRECTORY_SEPARATOR . 'index.php');
		}

		if (!$fileSystem->isFile('robots.txt')) {
			$fileSystem->import($resourceDir . DIRECTORY_SEPARATOR . 'robots.txt');
		}

		if (!$fileSystem->isFile('asmaru.phar')) {
			$fileSystem->import(Phar::running(false));
		}
	}
}