<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\cms\core\search\QueryParser;
use asmaru\cms\core\search\SearchService;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\io\IOException;
use asmaru\mustache\Context;
use asmaru\mustache\Helper;

/**
 * Class PageListHelper
 *
 * Usage: {{#pageList(path=/folder&sortBy=title&sortAsc=true&limit=5)}}{{data.title}}{{/pageList}}
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class PageListHelper implements Helper {

	/**
	 * @var StoreRootFolder
	 */
	private readonly StoreRootFolder $storeRootFolder;

	/**
	 * @var SearchService
	 */
	private readonly SearchService $searchService;

	/**
	 * PageListHelper constructor.
	 *
	 * @param StoreRootFolder $storeRootFolder
	 * @param SearchService $searchService
	 */
	public function __construct(StoreRootFolder $storeRootFolder, SearchService $searchService) {
		$this->storeRootFolder = $storeRootFolder;
		$this->searchService = $searchService;
	}

	/**
	 * @param $content
	 * @param Context $context
	 * @param array $params
	 *
	 * @return array
	 * @throws IOException
	 */
	public function render($content, Context $context, array $params = []): array {
		return $this->searchService->findByQuery($this->storeRootFolder, QueryParser::fromArray($params))->toJsonArray();
	}

	/**
	 * @return bool
	 */
	public function renderBefore(): bool {
		return false;
	}
}