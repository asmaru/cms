<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\api;

use asmaru\cms\frontend\api\model\PingResponse;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use OpenApi\Attributes as OA;

#[OA\Get(
	path: '/ping',
	operationId: 'getPing',
	responses: [
		new OA\Response(response: 200, description: '', content: new OA\JsonContent(type: PingResponse::class))
	]
)]
class PingEndpoint extends AbstractResource {

	public function getCollection(): Response {
		return Response::ok()->json(new PingResponse());
	}
}