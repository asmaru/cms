<?php /** @noinspection ALL */

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use function ini_get;
use const PHP_VERSION;

/**
 * Class VersionCommand
 *
 * @package asmaru\cms\backend\cli\command
 */
class CheckCommand implements CommandInterface {

	/**
	 * @param StyledOutput $output
	 * @param array $arguments
	 */
	public function execute(Input $input, StyledOutput $output): CommandResult {
		$output->writeLine('Check system requirements ...');
		$output->newLine();

		$requirements = [];

		$ok = version_compare(PHP_VERSION, '8.1.0') > 0;
		$requirements[] = ['PHP', $ok ? 'OK' : '< 8.1.0'];

		$ok = extension_loaded('gd');
		$requirements[] = ['GD', $ok ? 'OK' : 'not installed'];

		$ok = extension_loaded('curl');
		$requirements[] = ['CURL', $ok ? 'OK' : 'not installed'];

		$size = $this->getBytes(ini_get('memory_limit'));
		$ok = $size >= 8388608;
		$requirements[] = ['Memory limit', $ok ? 'OK (' . $this->formatBytes($size) . ')' : 'too low (' . $this->formatBytes($size) . ')'];

		$size = $this->getBytes(ini_get('post_max_size'));
		$ok = $size >= 8388608;
		$requirements[] = ['Post size', $ok ? 'OK (' . $this->formatBytes($size) . ')' : 'too low (' . $this->formatBytes($size) . ')'];

		$size = $this->getBytes(ini_get('upload_max_filesize'));
		$ok = $size >= 8388608;
		$requirements[] = ['Upload size', $ok ? 'OK (' . $this->formatBytes($size) . ')' : 'too low (' . $this->formatBytes($size) . ')'];

		$output->table($requirements, ['Requirement', 'Status']);

		return CommandResult::success();
	}

	/**
	 * @param $value
	 *
	 * @return int|string
	 */
	private function getBytes(string|bool $value) {
		preg_match('/(?<value>\d+)(?<option>.?)/i', trim((string)$value), $matches);
		$inc = [
			'g' => 1073741824, // (1024 * 1024 * 1024)
			'm' => 1048576, // (1024 * 1024)
			'k' => 1024
		];
		$value = (int)$matches['value'];
		$key = strtolower(trim($matches['option']));
		if (isset($inc[$key])) {
			$value *= $inc[$key];
		}
		return $value;
	}

	private function formatBytes(int|string $bytes) {
		return round($bytes / 1024 / 1024) . ' MB';
	}

	/**
	 * @return string
	 */
	public function help(): string {
		return 'Show the installed application version';
	}
}