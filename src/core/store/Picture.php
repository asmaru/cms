<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

use asmaru\cms\core\error\ValidationException;
use asmaru\gdlib\GDUtility;
use asmaru\io\IOException;
use SplFileInfo;
use function in_array;
use function sprintf;
use const IMAGETYPE_PNG;

class Picture extends Media {

	final const EXTENSIONS = ['png', 'jpg', 'jpeg', 'gif', 'webp'];

	/**
	 * @param $data
	 *
	 * @throws IOException
	 * @throws ValidationException
	 */
	public function save($data): void {
		$tmpFile = $this->uploadService->convertDataUriToFile($data);

		// Validate and resize image
		if (!GDUtility::isImage($tmpFile->getRealPath())) {
			throw new ValidationException(sprintf('File %s is not a valid image', $this->getName()));
		}
		$maxImageWidth = $this->settingsManager->get('maxImageSize');
		$resizedFileName = GDUtility::getFilenameWithExtension($tmpFile->getRealPath(), IMAGETYPE_PNG);
		GDUtility::toPNG($tmpFile->getRealPath(), $resizedFileName, $maxImageWidth);
		$this->temporaryFileSystem->delete($tmpFile->getRealPath());

		// Copy the file to the correct folder
		$this->parent->getFileSystem()->delete($this->getName());
		$this->parent->getFileSystem()->import($resizedFileName, $this->getName());
		$this->temporaryFileSystem->delete($resizedFileName);
	}

	public static function matchType(SplFileInfo $file): bool {
		return in_array(mb_strtolower($file->getExtension()), self::EXTENSIONS);
	}
}