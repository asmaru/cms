<?php

declare(strict_types=1);

namespace asmaru\cms\backend\callback;

use asmaru\cache\frontend\CacheFrontend;
use asmaru\cms\core\event\Event;
use asmaru\cms\core\event\EventCallback;

/**
 * Class ClearCacheHandler
 *
 * @package asmaru\cms\core\event
 */
class ClearCacheCallback implements EventCallback {

	/**
	 * @var CacheFrontend
	 */
	private readonly CacheFrontend $cacheFrontend;

	/**
	 * ClearCacheHandler constructor.
	 *
	 * @param CacheFrontend $cacheFrontend
	 */
	public function __construct(CacheFrontend $cacheFrontend) {
		$this->cacheFrontend = $cacheFrontend;
	}

	/**
	 * @param Event $event
	 */
	public function execute(Event $event): void {
		$this->cacheFrontend->flush();
	}
}