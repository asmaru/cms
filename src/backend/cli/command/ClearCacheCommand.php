<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\cms\core\Context;
use asmaru\io\FileUtility;
use const DIRECTORY_SEPARATOR;
use const false;

class ClearCacheCommand implements CommandInterface {

	public function __construct(private readonly Context $context) {
	}

	public function execute(Input $input, StyledOutput $output): CommandResult {
		(new FileUtility())->deleteRecursive($this->context->getRootPath() . DIRECTORY_SEPARATOR . 'temp', false);
		(new FileUtility())->deleteRecursive($this->context->getRootPath() . DIRECTORY_SEPARATOR . 'log', false);
		$output->successLine('✔ All temporary files were removed');
		return CommandResult::success();
	}

	public function help(): string {
		return 'Delete temporary files';
	}
}