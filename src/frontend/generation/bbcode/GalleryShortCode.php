<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\bbcode;

use asmaru\cms\core\meta\MetaDataService;
use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\search\SearchService;
use asmaru\cms\core\store\Picture;
use asmaru\cms\core\store\StoreElement;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\cms\frontend\generation\TemplateResolver;
use asmaru\di\ObjectManager;
use function array_reduce;

/**
 * Class GalleryShortCode
 *
 * @package asmaru\cms\frontend\generation\bbcode
 */
class GalleryShortCode extends AbstractListingShortCode {

	private readonly MetaDataService $metaDataService;

	/**
	 * GalleryShortCode constructor.
	 *
	 * @param StoreRootFolder $storeRootFolder
	 * @param SearchService $searchService
	 * @param ObjectManager $objectManager
	 * @param TemplateResolver $templateResolver
	 * @param MetaDataService $metaDataService
	 */
	public function __construct(StoreRootFolder $storeRootFolder, SearchService $searchService, ObjectManager $objectManager, TemplateResolver $templateResolver, MetaDataService $metaDataService) {
		parent::__construct($storeRootFolder, $searchService, $objectManager, $templateResolver);
		$this->metaDataService = $metaDataService;
	}

	/**
	 * @return string
	 */
	protected function getPartialFileName(): string {
		return 'gallery.html';
	}

	/**
	 * @return string
	 */
	protected function getTagName(): string {
		return 'gallery';
	}

	/**
	 * @param SearchFilter $filter
	 */
	protected function prepareFilter(SearchFilter $filter): void {
		$filter->type = Picture::type();
	}

	/**
	 * @param array $items
	 *
	 * @return array
	 */
	protected function prepareItems(array $items): array {
		return array_reduce($items, function (array $carry, StoreElement $item): array {
			$meta = $this->metaDataService->get($item);
			$carry[] = [
				'element' => $item->jsonSerialize(),
				'meta' => $meta->jsonSerialize()
			];
			return $carry;
		}, []);
	}
}