<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api\model;

use asmaru\cms\core\image\ImageService;
use asmaru\cms\core\Logger;
use asmaru\cms\core\store\DataContainer;
use asmaru\cms\core\store\Folder;
use asmaru\cms\core\store\Picture;
use asmaru\cms\core\store\StoreElement;
use asmaru\gdlib\ImageDimension;
use asmaru\http\App;
use asmaru\http\Request;
use asmaru\io\IOException;
use JsonSerializable;
use function array_reduce;
use function strcasecmp;
use function usort;

/**
 * Class StoreElementOutputData
 *
 * @package asmaru\cms\backend\api\model
 */
class StoreElementOutputData implements JsonSerializable {

	/**
	 * @var Request
	 */
	private readonly Request $request;

	/**
	 * @var App
	 */
	private readonly App $app;

	/**
	 * @var bool
	 */
	private bool $includeChildren = false;

	/**
	 * @var bool
	 */
	private bool $includeData = false;

	/**
	 * @var StoreElement
	 */
	private readonly StoreElement $element;

	/**
	 * @var bool
	 */
	private bool $includeChildrenRecursive = false;

	/**
	 * @var ImageService
	 */
	private readonly ImageService $imageService;

	/**
	 * @var Logger
	 */
	private readonly Logger $logger;

	public function __construct(StoreElement $element, Request $request, App $app, ImageService $imageService, Logger $logger) {
		$this->element = $element;
		$this->request = $request;
		$this->app = $app;
		$this->imageService = $imageService;
		$this->logger = $logger;
	}

	public function includeData(): StoreElementOutputData {
		$this->includeData = true;
		return $this;
	}

	/**
	 * @return array{_self: string, _parent: string|null, _selfRelative: string, _parentRelative: string, _preview:
	 *     string|null, _public: string, name: string, type: string|null, data: mixed, children: mixed[], size:
	 *     int|false, changed: int, dimensions: ImageDimension|never[]}
	 */
	public function jsonSerialize(): array {
		$self = $this->request->uri($this->app->getRoute('doc')->path(['name' => $this->element->getPath()]));

		$parent = null;
		if (!$this->element->isRoot()) {
			$parent = $this->request->uri($this->app->getRoute('doc')->path(['name' => $this->element->getParent()->getPath()]));
		}

		$data = $this->includeData && $this->element instanceof DataContainer ? $this->element->getData() : null;

		$children = [];
		if ($this->includeChildren && $this->element instanceof Folder) {
			$children = $this->element->getChildren();

			usort($children, function (StoreElement $a, StoreElement $b) {
				// Folders first
				if ($a instanceof Folder && !($b instanceof Folder)) {
					return -1;
				}
				if ($b instanceof Folder && !($a instanceof Folder)) {
					return 1;
				}
				// Order by name for all other types
				return strcasecmp($a->getName(), $b->getName());
			});

			$children = array_reduce($children, function (array $elements, StoreElement $element): array {
				$childOutput = new static($element, $this->request, $this->app, $this->imageService, $this->logger);
				if ($this->includeChildrenRecursive) $childOutput->includeChildren($this->includeChildrenRecursive);
				$elements[] = $childOutput;
				return $elements;
			}, []);
		}

		$previewImageUrl = null;
		$dimensions = null;
		if ($this->element->getType() === Picture::type()) {
			try {
				$scaledImage = $this->imageService->getImage($this->element, $this->imageService->getPreviewResolution());
				if ($scaledImage !== null) $previewImageUrl = $this->request->uri($this->imageService->getPublicPath($scaledImage));
			} catch (IOException $e) {
				$this->logger->error($e->getMessage());
			}
			try {
				$image = $this->imageService->getImage($this->element, null);
				if ($image !== null) $dimensions = $image->getDimensions();
			} catch (IOException $e) {
				$this->logger->error($e->getMessage());
			}
		}

		$publicPath = $this->element->getPublicPath();
		if ($this->element->getType() === Picture::type()) {
			$scaledImage = $this->imageService->getImage($this->element, null);
			$publicPath = $this->request->uri($this->imageService->getPublicPath($scaledImage));
		}

		return [
			'_self' => $self,
			'_parent' => $parent,
			'_selfRelative' => $this->element->getPath(),
			'_parentRelative' => $this->element->isRoot() ? '' : $this->element->getParent()->getPath(),
			'_preview' => $previewImageUrl,
			'_public' => $publicPath,
			'name' => $this->element->getName(),
			'type' => $this->element->getType(),
			'data' => $data,
			'children' => $children,
			'size' => $this->element->getSize(),
			'changed' => $this->element->getChanged(),
			'dimensions' => $dimensions ? new ImageDimensionResponse($dimensions) : null
		];
	}

	public function includeChildren(bool $recursive = false): StoreElementOutputData {
		$this->includeChildren = true;
		$this->includeChildrenRecursive = $recursive;
		return $this;
	}
}