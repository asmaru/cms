<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\api;

use asmaru\cache\frontend\CacheFrontend;
use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\search\SearchService;
use asmaru\cms\core\store\DefaultFields;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreRootFolder;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\sitemap\Frequency;
use asmaru\sitemap\SiteMapBuilder;

/**
 * Class PublicPageEndpoint
 *
 * @package asmaru\cms\frontend\api
 */
class SitemapEndpoint extends AbstractResource {

	private readonly CacheFrontend $cacheFrontend;

	private readonly StoreRootFolder $storeRootFolder;

	/**
	 * @var SearchService
	 */
	private readonly SearchService $searchService;

	/**
	 * SitemapEndpoint constructor.
	 *
	 * @param Request $request
	 * @param CacheFrontend $cacheFrontend
	 * @param StoreRootFolder $storeRootFolder
	 * @param SearchService $searchService
	 *
	 * @noinspection PhpMissingParentConstructorInspection
	 */
	public function __construct(Request $request, CacheFrontend $cacheFrontend, StoreRootFolder $storeRootFolder, SearchService $searchService) {
		$this->request = $request;
		$this->cacheFrontend = $cacheFrontend;
		$this->storeRootFolder = $storeRootFolder;
		$this->searchService = $searchService;
	}

	public function getCollection(): Response {
		$content = $this->cacheFrontend->withCache($this->request->env('REQUEST_URI'), function (): string {
			$sitemap = new SiteMapBuilder();
			$filter = new SearchFilter();
			$filter->setType(Page::type());
			$filter->addField(DefaultFields::DISABLED, '0');
			$filter->addField(DefaultFields::HIDDEN, '0');
			$result = $this->searchService->find($this->storeRootFolder, $filter);
			/** @var Page $page */
			foreach ($result->toArray() as $page) {
				$sitemap->add($this->request->uri($page->getPublicPath()), strval($page->getChanged()), Frequency::daily());
			}
			return $sitemap->__toString();
		});
		return (new Response())->setContent($content, 'application/xml; charset=utf-8');
	}
}