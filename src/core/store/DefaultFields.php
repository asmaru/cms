<?php

declare(strict_types=1);

namespace asmaru\cms\core\store;

interface DefaultFields {

	const TYPE = 'type';

	const DISABLED = 'disabled';

	const CHANGED = 'changed';

	const HIDDEN = 'hidden';
}