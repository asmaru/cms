<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\SetList;

return static function (RectorConfig $rectorConfig): void {
	$rectorConfig->paths([
		__DIR__ . '/resources',
		__DIR__ . '/src',
	]);

	$rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);
	$rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);

	$rectorConfig->sets([
		SetList::PHP_81,
		SetList::TYPE_DECLARATION
	]);
};