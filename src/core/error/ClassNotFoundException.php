<?php

declare(strict_types=1);

namespace asmaru\cms\core\error;

use Exception;
use Throwable;

/**
 * Class ClassNotFoundException
 *
 * @package asmaru\cms\core
 * @codeCoverageIgnore
 */
class ClassNotFoundException extends Exception {

	public function __construct(string $message = '', int $code = 0, Throwable $previous = null) {
		parent::__construct(sprintf('Class "%s" not found', $message), $code, $previous);
	}
}