<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation;

use asmaru\cms\core\store\StoreElement;

/**
 * Class Pager
 *
 * @package asmaru\cms\frontend\generation
 */
class Pager {

	private readonly int $size;

	private readonly int $currentPage;

	private readonly int $totalItems;

	private readonly int $totalPages;

	/**
	 * Pager constructor.
	 *
	 * @param int $currentPage
	 * @param int $totalItems
	 * @param int $size
	 */
	public function __construct(int $currentPage, int $totalItems, int $size = 10) {
		$this->totalItems = $totalItems;
		$this->size = $size;
		$this->totalPages = $this->totalItems > 0 && $size > 0 ? intval(ceil($this->totalItems / $this->size)) : 0;
		$this->currentPage = min(max($currentPage, 1), $this->totalPages);
	}

	/**
	 * @param StoreElement $element
	 * @param UrlFactory $urlFactory
	 *
	 * @return array|PagerItem[]
	 */
	public function getPages(StoreElement $element, UrlFactory $urlFactory): array {
		$pages = [];
		for ($i = 1; $i <= $this->totalPages; $i++) {
			$pages[] = (new PagerItem($i, $i == $this->currentPage, $urlFactory->forStoreElement($element, $i)))->jsonSerialize();
		}
		return $pages;
	}

	public function getTotalPages(): int {
		return $this->totalPages;
	}
}