<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli;

use function filesize;
use function header;
use function is_file;
use function mime_content_type;
use function preg_match;
use function readfile;
use function strtoupper;

class Server {

	public function __construct(private readonly string $rootPath) {
	}

	public function deliverStaticFiles(): void {

		$_SERVER['BASEPATH'] = '/';

		// Handle CORS
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header('Access-Control-Allow-Origin: *');
		}
		if (strtoupper((string)$_SERVER['REQUEST_METHOD']) === 'OPTIONS') {
			header('Access-Control-Allow-Methods: *');
			header('Access-Control-Allow-Headers: *');
			header('Access-Control-Allow-Credentials: true');
			die();
		}

		// Send file
		$path = $this->rootPath . $_SERVER['REQUEST_URI'];
		if (is_file($path)) {
			if (preg_match('/\.css$/', $path)) {
				header('Content-Type: text/css');
			} else if (preg_match('/\.svg$/', $path)) {
				header('Content-Type: image/svg+xml');
			} else if (preg_match('/\.js$/', $path)) {
				header('Content-Type: text/javascript');
			} else {
				header('Content-Type: ' . mime_content_type($path));
			}
			header('Content-Length: ' . filesize($path));
			readfile($path);
			exit();
		}
	}
}