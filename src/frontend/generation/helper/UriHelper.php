<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\cms\frontend\generation\GenerationContext;
use asmaru\mustache\Context;
use asmaru\mustache\Helper;

/**
 * Class UriHelper
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class UriHelper implements Helper {

	/**
	 * @var GenerationContext
	 */
	private readonly GenerationContext $generationContext;

	public function __construct(GenerationContext $generationContext) {
		$this->generationContext = $generationContext;
	}

	public function render($content, Context $context, array $params = []): string {
		return $this->generationContext->uri($content);
	}

	public function renderBefore(): bool {
		return true;
	}
}