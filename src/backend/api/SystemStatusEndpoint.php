<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api;

use asmaru\cache\frontend\CacheFrontend;
use asmaru\cms\backend\api\model\SystemStatusResponse;
use asmaru\cms\core\Context;
use asmaru\cms\core\SettingsManager;
use asmaru\http\ContentType;
use asmaru\http\Request;
use asmaru\http\Response;
use asmaru\http\rest\AbstractResource;
use asmaru\http\rest\Resource;
use asmaru\io\FileSystem;
use asmaru\json\Json;

class SystemStatusEndpoint extends AbstractResource implements Resource {

	private readonly Context $context;

	private readonly CacheFrontend $cacheFrontend;

	public function __construct(Request $request, Context $context, CacheFrontend $cacheFrontend) {
		parent::__construct($request);
		$this->context = $context;
		$this->cacheFrontend = $cacheFrontend;
	}

	public function getCollection(): Response {
		$data = $this->cacheFrontend->withCache($this->request->env('REQUEST_URI'), function (): string {

			$data = new SystemStatusResponse();
			$data->setUsedSpace((new FileSystem($this->context->getRootPath()))->size(true));
			$data->setResourceCount((new FileSystem($this->context->getResourcePath()))->count(true));

			return Json::toString($data);
		}, SettingsManager::API_CACHE_LIFETIME);
		return (new Response())->setContent($data, ContentType::JSON);
	}
}