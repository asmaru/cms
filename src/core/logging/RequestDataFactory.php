<?php

declare(strict_types=1);

namespace asmaru\cms\core\logging;

use asmaru\http\Request;

/**
 * Class RequestDataFactory
 *
 * @package asmaru\cms\core\logging
 */
class RequestDataFactory {

	/**
	 * @param Request $request
	 * @param int $status
	 *
	 * @return RequestData
	 */
	public function buildFromRequest(Request $request, int $status): RequestData {
		$requestData = new RequestData();
		$requestData->setProtocol($request->env('SERVER_PROTOCOL', ''));
		$requestData->setPath($request->env('REQUEST_URI', ''));
		$requestData->setUserAgent($request->env('HTTP_USER_AGENT', ''));
		$requestData->setReferer($request->env('HTTP_REFERER', ''));
		$requestData->setTime((int)$request->env('REQUEST_TIME', time()));
		$requestData->setStatus($status);
		$requestData->setDnt($request->env('HTTP_DNT') === '1');
		$requestData->setAccept($request->env('HTTP_ACCEPT', ''));
		$requestData->setAcceptLanguage($request->env('HTTP_ACCEPT_LANGUAGE', ''));
		$requestData->setAcceptEncoding($request->env('HTTP_ACCEPT_ENCODING', ''));
		return $requestData;
	}
}