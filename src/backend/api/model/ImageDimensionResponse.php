<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api\model;

use asmaru\gdlib\ImageDimension;
use JsonSerializable;

class ImageDimensionResponse implements JsonSerializable {

	private ImageDimension $imageDimension;

	public function __construct(ImageDimension $imageDimension) {
		$this->imageDimension = $imageDimension;
	}

	public function jsonSerialize(): array {
		return [
			'height' => $this->imageDimension->getHeight(),
			'width' => $this->imageDimension->getWidth()
		];
	}
}