<?php

declare(strict_types=1);

namespace asmaru\cms\core;

use PHPUnit\Framework\Attributes\CodeCoverageIgnore;
use const DIRECTORY_SEPARATOR;

class Context {

	private readonly string $rootPath;

	private readonly string $pharPath;

	private readonly string $version;

	public function __construct(string $rootPath, string $pharPath, string $version) {
		$this->rootPath = $rootPath;
		$this->pharPath = $pharPath;
		$this->version = $version;
	}

	public function getPath(string $name): string {
		return $this->getRootPath() . DIRECTORY_SEPARATOR . $name;
	}

	#[CodeCoverageIgnore]
	public function getRootPath(): string {
		return $this->rootPath;
	}

	#[CodeCoverageIgnore]
	public function getPharPath(): string {
		return $this->pharPath;
	}

	public function getResourcePath(): string {
		return $this->getDataPath('resources');
	}

	public function getDataPath(string $name): string {
		return $this->rootPath . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . $name;
	}

	#[CodeCoverageIgnore]
	public function getVersion(): string {
		return $this->version;
	}
}