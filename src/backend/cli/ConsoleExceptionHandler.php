<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli;

use asmaru\cli\StyledOutput;
use asmaru\di\ObjectManager;
use ReflectionClass;
use ReflectionException;
use Throwable;
use function array_map;
use function implode;
use function is_array;
use function set_exception_handler;
use function sprintf;

/**
 * Class CLIExceptionHandler
 *
 * @package asmaru\cms\backend\cli
 */
class ConsoleExceptionHandler {

	/**
	 * @param ObjectManager $objectManager
	 *
	 * @throws ReflectionException
	 */
	public static function init(ObjectManager $objectManager): void {
		set_exception_handler(function (Throwable $e) use ($objectManager): void {
			/** @var StyledOutput $out */
			$out = $objectManager->make(StyledOutput::class);
			$className = (new ReflectionClass($e))->getShortName();
			$message = sprintf('%s thrown in file %s:%s: %s', $className, $e->getFile(), $e->getLine(), $e->getMessage());
			$out->newLine();
			$out->errorLine($message);
			$out->newLine();
			foreach ($e->getTrace() as $value) {

				if (isset($value['file'])) {
					$out->write($value['file'] . ':' . $value['line'] . ' ');
				}

				if (isset($value['class'])) $out->write($value['class'] . $value['type']);

				$out->write($value['function'] . '(' . implode(',', array_map(function ($v): string {
						return is_array($v) ? 'array' : '\'' . $v . '\'';
					}, $value['args'] ?? 'null')) . ')');

				$out->newLine();
			}
		});
	}
}