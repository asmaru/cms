<?php

declare(strict_types=1);

namespace asmaru\cms\core\utility;

use function password_verify;

/**
 * Class PasswordUtility
 *
 * @package asmaru\cms\core
 */
class PasswordUtility {

	public static function hash(string $password): string {
		return password_hash($password, PASSWORD_DEFAULT, ['cost' => 8]);
	}

	public static function verify(string $password, string $hash): bool {
		return password_verify($password, $hash);
	}
}