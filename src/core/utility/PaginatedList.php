<?php

declare(strict_types=1);

namespace asmaru\cms\core\utility;

use function array_slice;

class PaginatedList {

	/**
	 * @var array
	 */
	private readonly array $data;

	/**
	 * @var int
	 */
	private readonly int $pageSize;

	public function __construct(array $data, int $pageSize) {
		$this->data = $data;
		$this->pageSize = $pageSize;
	}

	/**
	 * @return mixed[]
	 */
	public function getPage(int $page): array {
		$offset = ($page - 1) * $this->pageSize;
		return array_slice($this->data, $offset, $this->pageSize);
	}

	public function size(): int {
		return count($this->data);
	}
}