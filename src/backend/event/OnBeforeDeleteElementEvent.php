<?php

declare(strict_types=1);

namespace asmaru\cms\backend\event;

use asmaru\cms\core\event\Event;
use asmaru\cms\core\store\StoreElement;

/**
 * Class OnBeforeDeleteElementEvent
 *
 * @package asmaru\cms\core\event
 */
class OnBeforeDeleteElementEvent implements Event {

	/**
	 * @var StoreElement
	 */
	private readonly StoreElement $storeElement;

	/**
	 * OnBeforeDeleteElementEvent constructor.
	 *
	 * @param StoreElement $element
	 */
	public function __construct(StoreElement $element) {
		$this->storeElement = $element;
	}

	/**
	 * @return StoreElement
	 */
	public function getStoreElement(): StoreElement {
		return $this->storeElement;
	}
}