<?php

declare(strict_types=1);

namespace asmaru\cms\backend\cli\command;

use asmaru\cli\StyledOutput;
use asmaru\cms\backend\cli\CommandInterface;
use asmaru\cms\backend\cli\CommandResult;
use asmaru\cms\backend\cli\Input;
use asmaru\cms\core\Context;

/**
 * Class VersionCommand
 *
 * @package asmaru\cms\backend\cli\command
 * @noinspection PhpUnused
 */
class VersionCommand implements CommandInterface {

	/**
	 * @var Context
	 */
	private readonly Context $context;

	/**
	 * VersionCommand constructor.
	 *
	 * @param Context $context
	 */
	public function __construct(Context $context) {
		$this->context = $context;
	}

	/**
	 * @param StyledOutput $output
	 * @param array $arguments
	 */
	public function execute(Input $input, StyledOutput $output): CommandResult {
		$output->writeLine('   ___    ____ __  ___ ___    ___   __  __  ');
		$output->writeLine('  / _ |  / __//  |/  // _ |  / _ \\ / / / /  ');
		$output->writeLine(' / __ | _\\ \\ / /|_/ // __ | / , _// /_/ /   ');
		$output->writeLine('/_/ |_|/___//_/  /_//_/ |_|/_/|_| \\____/    ');
		$output->newLine();
		$output->successLine(sprintf('asmaru CMS version %s', $this->context->getVersion()));
		return CommandResult::success();
	}

	/**
	 * @return string
	 */
	public function help(): string {
		return 'Show the installed application version';
	}
}