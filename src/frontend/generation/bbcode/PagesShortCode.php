<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\bbcode;

use asmaru\cms\core\search\SearchFilter;
use asmaru\cms\core\store\DefaultFields;
use asmaru\cms\core\store\Page;

/**
 * Class PagesShortCode
 *
 * @package asmaru\cms\frontend\generation\bbcode
 */
class PagesShortCode extends ListShortCode {

	/**
	 * @return string
	 */
	protected function getTagName(): string {
		return 'pages';
	}

	/**
	 * @param SearchFilter $filter
	 */
	protected function prepareFilter(SearchFilter $filter): void {
		$filter->type = Page::type();
		$filter->addField(DefaultFields::DISABLED, '0');
		$filter->addField(DefaultFields::HIDDEN, '0');
	}
}