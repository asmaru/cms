<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\bbcode\BBCode;
use asmaru\mustache\Context;
use asmaru\mustache\Helper;

/**
 * Class BBCodeHelper
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class FormatHtmlHelper implements Helper {

	private readonly BBCode $bbCode;

	/**
	 * FormatHtmlHelper constructor.
	 *
	 * @param BBCode $bbCodeParser
	 */
	public function __construct(BBCode $bbCodeParser) {
		$this->bbCode = $bbCodeParser;
	}

	/**
	 * @param $content
	 * @param Context $context
	 * @param array $params
	 *
	 * @return string
	 */
	public function render($content, Context $context, array $params = []): string {
		return $this->bbCode->setEscapeHTML(true)->toHTML(html_entity_decode((string)$content));
	}

	/**
	 * @return boolean
	 */
	public function renderBefore(): bool {
		return true;
	}
}