<?php

declare(strict_types=1);

namespace asmaru\cms\backend\api\model;

use asmaru\cms\core\logging\LogEntry;
use JsonSerializable;
use OpenApi\Attributes as OA;

#[OA\Schema]
class RequestListResponse implements JsonSerializable {

	#[OA\Property(items: new OA\Items(type: LogEntry::class))]
	private array $requests;

	public function __construct(array $requests) {
		$this->requests = $requests;
	}

	public function jsonSerialize(): array {
		return [
			'requests' => $this->requests,
		];
	}
}