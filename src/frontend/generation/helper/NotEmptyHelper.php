<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\generation\helper;

use asmaru\mustache\Context;
use asmaru\mustache\Helper;

/**
 * Class NotEmptyHelper
 *
 * @package asmaru\cms\frontend\generation\helper
 */
class NotEmptyHelper implements Helper {

	public function render($content, Context $context, array $params = []): ?array {
		return !empty($context->get($params['name'])) ? [true] : null;
	}

	public function renderBefore(): bool {
		return false;
	}
}