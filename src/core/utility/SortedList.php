<?php

declare(strict_types=1);

namespace asmaru\cms\core\utility;

use asmaru\cms\core\store\DefaultFields;
use asmaru\cms\core\store\Page;
use asmaru\cms\core\store\StoreElement;
use function usort;

class SortedList {

	private array $data;

	public function __construct(array $data) {
		$this->data = $data;
	}

	/**
	 * @return mixed[]
	 */
	public function getSorted(string $property, bool $descending = true): array {
		usort($this->data, function (StoreElement $elementA, StoreElement $elementB) use ($property, $descending): int {
			$valueA = null;
			$valueB = null;
			if ($property === DefaultFields::CHANGED) {
				$valueA = $elementA->getChanged();
				$valueB = $elementB->getChanged();
			} else {
				if ($elementA instanceof Page) {
					$valueA = $elementA->getDataValue($property);
				}
				if ($elementB instanceof Page) {
					$valueB = $elementB->getDataValue($property);
				}
			}
			return ($valueA < $valueB) ? ($descending ? 1 : -1) : (($valueA > $valueB) ? ($descending ? -1 : 1) : 0);
		});
		return $this->data;
	}
}