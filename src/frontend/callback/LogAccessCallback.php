<?php

declare(strict_types=1);

namespace asmaru\cms\frontend\callback;

use asmaru\cms\core\event\Event;
use asmaru\cms\core\event\EventCallback;
use asmaru\cms\core\logging\AccessLogService;
use asmaru\io\IOException;

/**
 * Class LogAccessCallback
 *
 * @package asmaru\cms\frontend\callback
 */
class LogAccessCallback implements EventCallback {

	/**
	 * @var AccessLogService
	 */
	private readonly AccessLogService $accessLogService;

	/**
	 * LogPageAccessCallback constructor.
	 *
	 * @param AccessLogService $accessLogService
	 */
	public function __construct(AccessLogService $accessLogService) {
		$this->accessLogService = $accessLogService;
	}

	/**
	 * @param Event $event
	 *
	 * @throws IOException
	 */
	public function execute(Event $event): void {
		if ($event->isError()) {
			$this->accessLogService->notFound();
		} else {
			$this->accessLogService->found();
		}
	}
}