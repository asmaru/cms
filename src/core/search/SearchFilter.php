<?php

declare(strict_types=1);

namespace asmaru\cms\core\search;

use asmaru\http\Request;

class SearchFilter {

	public string $path;

	public string $term = '';

	public string $type = '';

	public bool $includeContent = false;

	private array $fields = [];

	public static function fromQuery(Query $query): SearchFilter {
		$filter = new static();
		if (!empty($query->getPath())) $filter->setPath($query->getPath());
		if (!empty($query->getTerm())) $filter->setTerm($query->getTerm());
		if (!empty($query->getType())) $filter->setType($query->getType());
		$filter->setIncludeContent($query->isIncludeContent());
		foreach ($query->getFields() as $name => $value) {
			$filter = $filter->withField($name, $value);
		}
		return $filter;
	}

	/**
	 * @return string
	 */
	public function getPath(): string {
		return $this->path;
	}

	public function setPath(string $path): void {
		$this->path = $path;
	}

	public function getTerm(): ?string {
		return $this->term;
	}

	public function setTerm(string $term): void {
		$this->term = $term;
	}

	public function getType(): ?string {
		return $this->type;
	}

	public function setType(string $type): void {
		$this->type = $type;
	}

	public function setIncludeContent(bool $includeContent): void {
		$this->includeContent = $includeContent;
	}

	/**
	 * @return mixed[]
	 */
	public function getFields(): array {
		return $this->fields;
	}

	public function addField(string $name, string $value): void {
		$this->fields[$name] = $value;
	}

	public static function fromRequest(Request $request): SearchFilter {
		$filter = new static();
		$filter->term = $request->get('query');
		$filter->type = $request->get('type');
		$filter->includeContent = $request->contains('includeContent');
		return $filter;
	}

	public function withType(string $type): static {
		$instance = clone $this;
		$instance->type = $type;
		return $instance;
	}

	public function withField(string $name, string $value): static {
		$instance = clone $this;
		$instance->fields[$name] = $value;
		return $instance;
	}

}